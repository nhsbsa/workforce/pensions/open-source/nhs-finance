package com.nhsbsa.controllers;

import static org.hamcrest.object.IsCompatibleType.typeCompatibleWith;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.nhsbsa.controllers.finance.SummaryController;
import com.nhsbsa.exceptions.AlreadySubmittedException;
import com.nhsbsa.exceptions.GlobalExceptionHandler;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.service.RequestForTransferService;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import com.nhsbsa.view.RequestForTransferView;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.view.InternalResourceView;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class SummaryControllerTest {

  private MockMvc mockMvc;
  private RequestForTransferService mockRftService;

  private static RequestForTransferView EXPECTED_RFT;
  private static RequestForTransferView EXPECTED_SUBMITTED_RFT;
  private static List<EmployingAuthorityView> EXPECTED_EA;
  private static List<OrganisationView> EXPECTED_ORG;

  private static final String EXPECTED_UUID = "6a7957d8-d0bc-4a9b-a5c4-65836fe809b6";
  private static final String INVALID_UUID = "MISSING-UUID";
  private static final String MISSING_UUID = "7a8068d9-d1bc-5a0b-a6c5-76947fe910b7";

  @BeforeClass
  public static void classSetUp() throws Exception {
    EXPECTED_RFT = new RequestForTransferView();
    EXPECTED_RFT.setRftUuid(EXPECTED_UUID);

    EmployingAuthorityView employingAuthority = EmployingAuthorityView.builder().eaCode("EA1234").employerType("").name("").build();
    EXPECTED_EA = Collections.singletonList(employingAuthority);
    OrganisationView organisation = OrganisationView.builder().identifier(UUID.randomUUID()).name("TEST").eas(EXPECTED_EA).build();
    EXPECTED_ORG = Collections.singletonList(organisation);
  
    EXPECTED_SUBMITTED_RFT = new RequestForTransferView();
    EXPECTED_SUBMITTED_RFT.setRftUuid(EXPECTED_UUID);
    EXPECTED_SUBMITTED_RFT.setSubmitDate(new Date());
  }

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    FinanceUser user = FinanceUser.builder()
        .id(0L)
        .firstName("test")
        .lastName("test")
        .username("test_user")
        .uuid(UUID.randomUUID().toString())
        .contactEmail("email@example.com")
        .organisations(EXPECTED_ORG)
        .build();

    Authentication authentication = mock(Authentication.class);
    SecurityContext securityContext = mock(SecurityContext.class);
    when(securityContext.getAuthentication()).thenReturn(authentication);
    SecurityContextHolder.setContext(securityContext);
    when(SecurityContextHolder.getContext().getAuthentication().getDetails()).thenReturn(user);

    mockRftService = mock(RequestForTransferService.class);
    SummaryController controller = new SummaryController(mockRftService);

    mockMvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/summary.html"))
        .setControllerAdvice(new GlobalExceptionHandler())
        .build();
  }

  @Test
  public void given_endpoint_requested_when_no_rft_uuid_provided_then_a_404_status_is_returned()
      throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/summary"))
        .andExpect(status().is(404));
  }

  // Note: Spring helpfully wraps the IllegalArgumentException in a NestedServletException
  @Test(expected = Exception.class)
  public void given_endpoint_requested_when_invalid_rft_uuid_provided_then_an_exception_is_raised()
      throws Exception {
    when(mockRftService.getRequestForTransferByRftUuidValidating(INVALID_UUID))
        .thenThrow(new IllegalArgumentException());
    mockMvc.perform(MockMvcRequestBuilders.get("/summary/{rftUuid}", INVALID_UUID));

  }

  @Test
  public void given_endpoint_requested_when_not_existing_rft_uuid_provided_then_a_404_response_is_returned()
      throws Exception {
    when(mockRftService.getOptionalRequestForTransferByRftUuid(MISSING_UUID))
        .thenReturn(Optional.empty());
    mockMvc.perform(MockMvcRequestBuilders.get("/summary/{rftUuid}", MISSING_UUID))
        .andExpect(status().is(404));
  }

  @Test
  public void given_endpoint_requested_when_valid_rft_uuid_provided_then_existing_rft_instance_is_used()
      throws Exception {
    when(mockRftService.getOptionalRequestForTransferByRftUuid(EXPECTED_UUID))
        .thenReturn(Optional.of(EXPECTED_RFT));
    mockMvc.perform(MockMvcRequestBuilders.get("/summary/{rftUuid}", EXPECTED_UUID))
        .andExpect(status().isOk())
        .andExpect(view().name("summary"))
        .andExpect(model().attributeExists("rft"))
        .andExpect(model().attribute("orgName", "TEST"))
        .andExpect(model().attribute("eaCode", EXPECTED_RFT.getEaCode()))
        .andExpect(model().attribute("rft", EXPECTED_RFT));
  }

  @Test
  @WithMockUser
  public void given_endpoint_requested_when_valid_rft_uuid_provided_but_already_submitted_then_login_redirection_response_is_returned()
      throws Exception {
    when(mockRftService.getOptionalRequestForTransferByRftUuid(EXPECTED_UUID))
        .thenReturn(Optional.of(EXPECTED_SUBMITTED_RFT));

    MvcResult mvcResult = mockMvc
        .perform(MockMvcRequestBuilders.get("/summary/{rftUuid}", EXPECTED_UUID))
        .andExpect(MockMvcResultMatchers.redirectedUrl("/login"))
        .andReturn();

    assertThat(mvcResult.getResolvedException().getClass(),
        typeCompatibleWith(AlreadySubmittedException.class));
  }

  @Test
  public void given_endpoint_requested_when_valid_rft_uuid_provided_with_invalid_eacode_then_500_response_is_returned()
      throws Exception {
    when(mockRftService.getOptionalRequestForTransferByRftUuid(EXPECTED_UUID))
        .thenReturn(Optional.of(EXPECTED_RFT));

    List<OrganisationView> organisations = Collections.emptyList();
    FinanceUser user = FinanceUser.builder()
        .id(0L)
        .firstName("test")
        .lastName("test")
        .username("test_user")
        .uuid(UUID.randomUUID().toString())
        .contactEmail("email@example.com")
        .organisations(organisations)
        .build();

    when(SecurityContextHolder.getContext().getAuthentication().getDetails()).thenReturn(user);

    mockMvc.perform(MockMvcRequestBuilders.get("/summary/{rftUuid}", EXPECTED_UUID))
        .andExpect(status().is(500));
  }

  @Test
  public void given_existing_rft_when_submit_post_done_then_url_is_thankyou_page()
      throws Exception {

    mockMvc.perform(MockMvcRequestBuilders.post("/summary/" + EXPECTED_UUID))
        .andExpect(status().isOk())
        .andExpect(view().name("redirect:/thankyou/" + EXPECTED_UUID));

  }


}
