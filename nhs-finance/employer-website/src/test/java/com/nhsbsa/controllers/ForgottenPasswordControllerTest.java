package com.nhsbsa.controllers;

import com.nhsbsa.controllers.finance.ForgottenPasswordController;
import com.nhsbsa.model.ForgottenPassword;
import com.nhsbsa.service.authentication.AuthorizationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by nataliehulse on 24/11/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ForgottenPasswordControllerTest {

    public static final String SET_FORGOTTEN_PASS_VIEW_NAME = "forgottenpassword";
    public static final String SET_FORGOTTEN_PASS_RESET_VIEW_NAME = "forgottenpasswordreset";
    public static final String EMAIL = "test.test@test.com";

    private MockMvc mockMvc;

    @Mock
    private AuthorizationService authorizationService;


    @Before
    public void setUp() throws Exception {

        mockMvc = MockMvcBuilders
                .standaloneSetup(new ForgottenPasswordController(authorizationService))
                .build();
    }

    @Test
    public void setForgottenPasswordPageIsReturned() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/forgotten-password"))
                .andExpect(handler().methodName("showForgottenPasswordPage"))
                .andExpect(status().isOk())
                .andExpect(view().name(SET_FORGOTTEN_PASS_VIEW_NAME))
                .andExpect(model().attributeExists("forgottenPassword"));
    }

    @Test
    public void successfulSaveRedirectsUserToPasswordResetPage() throws Exception {

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.post("/forgotten-password")
                .param("email", EMAIL))
                .andExpect(handler().methodName("postForgottenPasswordPage"))
                .andExpect(status().isOk())
                .andExpect(model().attributeErrorCount("forgottenPassword", 0))
                .andExpect(view().name(SET_FORGOTTEN_PASS_RESET_VIEW_NAME));

        verify(authorizationService, times(1)).checkUsername(Mockito.any(ForgottenPassword.class));

    }

    @Test
    public void postForgottenPasswordPageWithErrors() throws Exception {

        final MockHttpSession session = new MockHttpSession();

        mockMvc.perform(MockMvcRequestBuilders.post("/forgotten-password")
                .param("email", "tt123")
                .session(session))
                .andExpect(handler().handlerType(ForgottenPasswordController.class))
                .andExpect(handler().methodName("postForgottenPasswordPage"))
                .andExpect(status().isOk())
                .andExpect(view().name(is(equalTo("forgottenpassword"))))
                .andExpect(model().attributeErrorCount("forgottenPassword", 1))
                .andExpect(model().attributeHasFieldErrorCode("forgottenPassword", "email", "EmailFormat"))
                .andExpect(model().attribute("forgottenPassword", is(instanceOf(ForgottenPassword.class))));
    }

    @Test
    public void setForgottenPasswordResetPageIsReturned() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/forgotten-password-reset"))
                .andExpect(handler().methodName("showForgottenPasswordResetPage"))
                .andExpect(status().isOk())
                .andExpect(view().name(SET_FORGOTTEN_PASS_RESET_VIEW_NAME));
    }

}
