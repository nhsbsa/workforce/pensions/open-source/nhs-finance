package com.nhsbsa.controllers.finance;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.nhsbsa.model.AccountUser;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.UserRoles;
import com.nhsbsa.service.FinanceService;
import com.nhsbsa.service.authentication.AuthorizationService;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.view.InternalResourceView;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class CurrentUsersControllerTest {

  @MockBean
  private AuthorizationService authorizationService;

  @MockBean
  private FinanceService financeService;

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  private static EmployingAuthorityView EA = EmployingAuthorityView.builder().eaCode("EA1234").name("").employerType("").build();
  private static List<EmployingAuthorityView> EA_LIST = Collections.singletonList(EA);
  private static OrganisationView ORG = OrganisationView.builder().identifier(UUID.randomUUID()).name("").eas(EA_LIST).build();
  private static List<OrganisationView> ORG_LIST = Collections.singletonList(ORG);

  @Before
  public void setup() {

    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders
        .webAppContextSetup(wac)
        .apply(springSecurity())
        .build();

  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_endpoint_requested_with_standard_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/current-users"))
        .andExpect(status().isForbidden())
      .andReturn();
}

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_requested_with_master_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/current-users"))
        .andExpect(status().isForbidden())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_requested_with_master_role_then_requested_accounts_page_is_returned()
      throws Exception {

    FinanceUser user = FinanceUser.builder()
        .firstName("Sam")
        .lastName("Jones")
        .contactEmail("sam.jones@email.com")
        .uuid("user-uuid")
        .role(UserRoles.ROLE_ADMIN.name())
        .organisations(ORG_LIST)
        .build();

    AccountUser expectedUsersRow0 = AccountUser.builder().name("Sam Jones").email("sam.jones@email.com").userRole(UserRoles.ROLE_ADMIN.name()).uuid("user-uuid").isAdmin(true).build();
    AccountUser expectedUsersRow1 = AccountUser.builder().name("Test User").userRole(UserRoles.ROLE_STANDARD.name()).build();

    CurrentUsersController controller = Mockito
        .spy(new CurrentUsersController(authorizationService, financeService));
    doReturn(Optional.of(user)).when(controller).getCurrentUser();

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/currentusers.html"))
        .build();

    List<FinanceUser> financeUsers = Collections.singletonList(FinanceUser.builder()
        .firstName("Sam")
        .lastName("Jones")
        .contactEmail("sam.jones@email.com")
        .uuid("user-uuid")
        .role(UserRoles.ROLE_ADMIN.name())
        .build());
    doReturn(financeUsers).when(financeService).getFinanceUserByOrgUuid(user.getOrganisations().get(0).getIdentifier().toString());

    List<AccountUser> accountUsers = Collections.singletonList(AccountUser.builder()
        .name("Test User")
        .userRole(UserRoles.ROLE_STANDARD.name())
        .build());
    when(authorizationService.getStandardUsers(financeUsers)).thenReturn(accountUsers);

    MvcResult result = mvc
        .perform(MockMvcRequestBuilders.get("/current-users"))
        .andExpect(handler().methodName("showCurrentUsersPage"))
        .andExpect(model().attributeExists("users"))
        .andExpect(model().attribute("users", hasSize(2)))
        .andReturn();

    List aboutYouList = (List) result.getModelAndView().getModel().get("users");

    assertEquals(expectedUsersRow0, aboutYouList.get(0));
    assertEquals(expectedUsersRow1, aboutYouList.get(1));
  }


}
