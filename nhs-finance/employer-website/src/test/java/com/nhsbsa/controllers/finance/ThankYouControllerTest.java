package com.nhsbsa.controllers.finance;

import static com.nhsbsa.controllers.finance.ThankYouController.SNAP_SURVEY_URL;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.service.RequestForTransferService;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import com.nhsbsa.view.RequestForTransferView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.servlet.view.InternalResourceView;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ThankYouControllerTest {

    private MockMvc mockMvc;
    private RequestForTransferService mockRftService;
    private SecurityContextLogoutHandler mockSecurityContextLogoutHandler;

    private static RequestForTransferView EXPECTED_RFT;
    private static List<EmployingAuthorityView> EXPECTED_EA;
    private static List<OrganisationView> EXPECTED_ORG;

    private static final String EXPECTED_UUID = "9bae9b12-698f-457e-a22c-693ac186d952";
    private static final String MISSING_UUID = "7a8068d9-d1bc-5a0b-a6c5-76947fe910b7";
    private static final String INVALID_UUID = "INVALID_UUID";

    private ThankYouController controller;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    @BeforeClass
    public static void classSetUp() throws Exception {
        EXPECTED_RFT = new RequestForTransferView();
        EXPECTED_RFT.setRftUuid(EXPECTED_UUID);
        EmployingAuthorityView employingAuthority = EmployingAuthorityView.builder().eaCode("EA12345").name("").employerType("").build();
        EXPECTED_EA = Collections.singletonList(employingAuthority);
        OrganisationView organisation = OrganisationView.builder().identifier(UUID.randomUUID()).name("TEST").eas(EXPECTED_EA).build();
        EXPECTED_ORG = Collections.singletonList(organisation);
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        FinanceUser user = FinanceUser.builder()
            .id(0L)
            .firstName("test")
            .lastName("test")
            .username("test_user")
            .uuid(UUID.randomUUID().toString())
            .contactEmail("email@example.com")
            .organisations(EXPECTED_ORG)
            .build();
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(SecurityContextHolder.getContext().getAuthentication().getDetails()).thenReturn(user);

        
        mockRftService = mock(RequestForTransferService.class);

        mockSecurityContextLogoutHandler = mock(SecurityContextLogoutHandler.class);

        controller = new ThankYouController(mockRftService,mockSecurityContextLogoutHandler);

        mockMvc = standaloneSetup(controller)
                    .setSingleView(new InternalResourceView("/WEB-INF/templates/thankyou.html"))
                    .build();
    }

    @Test
    public void given_endpoint_requested_when_no_rft_uuid_provided_then_a_404_status_is_returned() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/thankyou"))
                .andExpect(status().is(404));
    }

    // Note: Spring helpfully wraps the IllegalArgumentException in a NestedServletException
    @Test(expected = Exception.class)
    public void given_endpoint_requested_when_invalid_rft_uuid_provided_then_an_exception_is_raised() throws Exception {
        when(mockRftService.getRequestForTransferByRftUuidValidating(INVALID_UUID))
                .thenThrow(new IllegalArgumentException());
        mockMvc.perform(MockMvcRequestBuilders.get("/thankyou/{rftUuid}", INVALID_UUID))
                .andExpect(status().isOk())
                .andExpect(view().name("thankyou"))
                .andExpect(model().attributeExists("rft"))
                .andExpect(model().attribute("rft", hasProperty("rftUuid", nullValue())));
    }

    @Test
    public void given_endpoint_requested_when_not_existing_rft_uuid_provided_then_a_404_response_is_returned() throws Exception {
        when(mockRftService.getOptionalRequestForTransferByRftUuid(MISSING_UUID))
                .thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.get("/thankyou/{rftUuid}", MISSING_UUID))
                .andExpect(status().is(404));
    }

    @Test
    public void given_endpoint_requested_when_valid_rft_uuid_provided_with_invalid_eacode_then_500_response_is_returned()
        throws Exception {
        when(mockRftService.getOptionalRequestForTransferByRftUuid(EXPECTED_UUID))
            .thenReturn(Optional.of(EXPECTED_RFT));

        List<OrganisationView> organisations = Collections.emptyList();
        FinanceUser user = FinanceUser.builder()
            .id(0L)
            .firstName("test")
            .lastName("test")
            .username("test_user")
            .uuid(UUID.randomUUID().toString())
            .contactEmail("email@example.com")
            .organisations(organisations)
            .build();

        when(SecurityContextHolder.getContext().getAuthentication().getDetails()).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders.get("/thankyou/{rftUuid}", EXPECTED_UUID))
            .andExpect(status().is(500));
    }

    @Test
    public void given_endpoint_requested_when_valid_rft_uuid_provided_then_existing_rft_instance_is_used() throws Exception {

        EXPECTED_RFT.setSubmitDate(dateFormat.parse("02/10/2018 10:50"));

        when(mockRftService.getOptionalRequestForTransferByRftUuid(EXPECTED_UUID))
                .thenReturn(Optional.of(EXPECTED_RFT));
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/thankyou/{rftUuid}", EXPECTED_UUID))
                .andExpect(status().isOk())
                .andExpect(view().name("thankyou"))
                .andExpect(model().attributeExists("rft"))
                .andExpect(model().attribute("orgName", "TEST"))
                .andExpect(model().attribute("eaCode", EXPECTED_RFT.getEaCode()))
                .andExpect(model().attribute("rft", EXPECTED_RFT))
                .andExpect(model().attributeExists("multiEa"))
                .andExpect(model().attributeExists("formattedSubmittedDate")).andReturn()
        ;

        assertEquals("2 October 2018 at 10:50", result.getModelAndView().getModel().get("formattedSubmittedDate"));
    }

    @Test
    public void given_endpoint_requested_then_user_logged_out_and_redirected() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/thankyou/finish"))
            .andExpect(handler().methodName("logOutAndRedirect"))
            .andExpect(status().is2xxSuccessful())
            .andExpect((view().name("redirect:"+SNAP_SURVEY_URL)));


        verify(mockSecurityContextLogoutHandler, times(1))
            .logout(any(), any(), any());
    }

    @Test
    public void given_endpoint_requested_with_no_user_then_redirected_only() throws Exception {
        when(SecurityContextHolder.getContext().getAuthentication()).thenReturn(null);
        mockMvc.perform(MockMvcRequestBuilders.post("/thankyou/finish"))
            .andExpect(handler().methodName("logOutAndRedirect"))
            .andExpect(status().is2xxSuccessful())
            .andExpect((view().name("redirect:"+SNAP_SURVEY_URL)));

        verify(mockSecurityContextLogoutHandler, times(0))
            .logout(any(), any(), any());
    }

    @Test
    public void submitted_date_formatter_test() throws ParseException {

        assertEquals("1 October 2018 at 10:50", controller.formatSubmittedDate(dateFormat.parse("01/10/2018 10:50")) );
        assertEquals("20 November 2018 at 19:50", controller.formatSubmittedDate(dateFormat.parse("20/11/2018 19:50")) );
        assertEquals("30 January 2018 at 12:00", controller.formatSubmittedDate(dateFormat.parse("30/1/2018 12:00")) );


    }

}
