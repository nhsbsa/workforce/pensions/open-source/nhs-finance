package com.nhsbsa.controllers.finance;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class EmployerCreatedControllerTest {

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  @Before
  public void setup() {

    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders
        .webAppContextSetup(wac)
        .apply(springSecurity())
        .build();

  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_endpoint_post_requested_with_standard_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.post("/employer-created").with(csrf()))
        .andExpect(status().isForbidden())
        .andDo(print())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_post_requested_with_admin_role_then_employer_created_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.post("/employer-created").with(csrf()))
        .andExpect(handler().methodName("showEmployerCreatedPage"))
        .andExpect(view().name("employerCreated"))
        .andExpect(status().isOk())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_post_requested_with_master_role_then_employer_created_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.post("/employer-created").with(csrf()))
        .andExpect(handler().methodName("showEmployerCreatedPage"))
        .andExpect(view().name("employerCreated"))
        .andExpect(status().isOk())
        .andReturn();
  }

}
