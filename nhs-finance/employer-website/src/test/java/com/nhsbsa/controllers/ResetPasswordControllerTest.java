package com.nhsbsa.controllers;

import com.nhsbsa.controllers.finance.ResetPasswordController;
import com.nhsbsa.security.ResetEmailTokenResponse;
import com.nhsbsa.security.SetPassword;
import com.nhsbsa.service.authentication.AuthorizationService;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.view.InternalResourceView;

import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by nataliehulse on 24/11/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ResetPasswordControllerTest {

    public static final String SET_RESET_PASS_VIEW_NAME = "resetpassword";

    private MockMvc mockMvc;

    private AuthorizationService mockService;

    private static ResetEmailTokenResponse EXPECTED_RESPONSE;

    private static final String TOKEN = "6a7957d8-token";
    private static final String INVALID_TOKEN = "invalid";
    private static final String EXPECTED_UUID = "6a7957d8-d0bc-4a9b-a5c4-65836fe809b6";

    @BeforeClass
    public static void classSetUp() throws Exception {
        EXPECTED_RESPONSE = new ResetEmailTokenResponse();
        EXPECTED_RESPONSE.setUserUuid(EXPECTED_UUID);
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockService = mock(AuthorizationService.class);

        mockMvc = MockMvcBuilders
                .standaloneSetup(new ResetPasswordController(mockService))
                .setSingleView(new InternalResourceView("/WEB-INF/templates/resetpassword.html"))
                .build();
    }

    @WithMockUser
    @Test
    public void verifiedTokenReturnsResetPasswordPage()
            throws Exception {
        when(mockService.getOptionalUuidByEmailToken(TOKEN))
                .thenReturn(Optional.of(EXPECTED_RESPONSE));

        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders.get("/reset-password/{emailToken}", TOKEN))
                .andExpect(handler().methodName("showResetPasswordPage"))
                .andExpect(status().isOk())
                .andExpect(view().name(SET_RESET_PASS_VIEW_NAME))
                .andExpect(model().attributeExists("setPassword"))
                .andReturn();

    }

    @WithMockUser
    @Test
    public void Reponse404ReturnedWhenInvalidToken()
            throws Exception {
        when(mockService.getOptionalUuidByEmailToken(INVALID_TOKEN))
                .thenReturn(Optional.empty());

        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders.get("/reset-password/{emailToken}", INVALID_TOKEN))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andReturn();
    }


    @Test
    public void successfulSaveRedirectsUserToPasswordUpdatePage() throws Exception {

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.post("/reset-password")
                .sessionAttr("uuid", "uuid-1234")
                .sessionAttr("token", "token-1234")
                .param("newPassword", "Password12345")
                .param("passwordConfirm", "Password12345"))
                .andExpect(handler().methodName("postResetPasswordPage"))
                .andExpect(status().isOk())
                .andExpect(model().attributeErrorCount("setPassword", 0))
                .andExpect(view().name("redirect:/passwordUpdated"));

        verify(mockService, times(1)).savePassword(Mockito.any(SetPassword.class));
    }

    @Test
    public void postDataWithErrors() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/reset-password")
                .sessionAttr("uuid", "uuid-1234")
                .sessionAttr("token", "token-1234")
                .param("newPassword", "123456")
                .param("passwordConfirm", "123456"))
                .andExpect(handler().methodName("postResetPasswordPage"))
                .andExpect(status().isOk())
                .andExpect(view().name(is(equalTo("resetpassword"))))
                .andExpect(model().attributeErrorCount("setPassword", 1))
                .andExpect(model().attributeHasFieldErrorCode("setPassword", "newPassword", "ValidatePassword"))
                .andExpect(model().attribute("setPassword", is(instanceOf(SetPassword.class))));
    }


}
