package com.nhsbsa.service.authentication;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.security.AuthenticationResponse.AuthenticationStatus;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserRoleLoginTest {

    public static final String SET_PASS_URL = "/set-password";
    public static final String SCHEDULE_YOUR_PAYMENT_URL = "/scheduleyourpayment";
    public static final String DOWNLOAD_FILES_URL = "/download-files";
    public static final String MULTI_EA = "/multi-ea";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @MockBean
    private UserLoginService userLoginService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        FinanceUser user = mock(FinanceUser.class);
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(SecurityContextHolder.getContext().getAuthentication().getDetails()).thenReturn(user);


        mockMvc = MockMvcBuilders
            .webAppContextSetup(wac)
            .apply(springSecurity())
            .build();
    }

    @Test
    public void firstLoginUserThenSetPasswordPageIsReturned() throws Exception {

        FinanceUser user = FinanceUser.builder().firstLogin(true).role("ROLE_ADMIN").build();

        FinanceUserStatusWrapper wrapper = FinanceUserStatusWrapper.builder()
            .financeUser(user)
            .status(AuthenticationStatus.AUTH_SUCCESS)
            .build();

        when(userLoginService.financeLogin(any(String.class),any(String.class))).thenReturn(wrapper);

        mockMvc.perform(formLogin())
            .andExpect(redirectedUrl(SET_PASS_URL));

    }

    @Test
    public void NotFirstLoginAdminUserAndNotMultiEaUserThenScheduleYourPaymentIsReturned() throws Exception {

        EmployingAuthorityView eaView = EmployingAuthorityView.builder().eaCode("EA1234").build();
        OrganisationView orgView = OrganisationView.builder().eas(Collections.singletonList(eaView)).build();
        FinanceUser user = FinanceUser.builder().firstLogin(false).role("ROLE_ADMIN").organisations(Collections.singletonList(orgView)).build();

        FinanceUserStatusWrapper wrapper = FinanceUserStatusWrapper.builder()
            .financeUser(user)
            .status(AuthenticationStatus.AUTH_SUCCESS)
            .build();

        when(userLoginService.financeLogin(any(String.class),any(String.class))).thenReturn(wrapper);

        mockMvc.perform(formLogin())
            .andExpect(redirectedUrl(SCHEDULE_YOUR_PAYMENT_URL));

    }

    @Test
    public void NotFirstLoginMasterUserThenDownloadFilesIsReturned() throws Exception {

        FinanceUser user = FinanceUser.builder().firstLogin(false).role("ROLE_MASTER").build();

        FinanceUserStatusWrapper wrapper = FinanceUserStatusWrapper.builder()
            .financeUser(user)
            .status(AuthenticationStatus.AUTH_SUCCESS)
            .build();

        when(userLoginService.financeLogin(any(String.class),any(String.class))).thenReturn(wrapper);

        mockMvc.perform(formLogin())
            .andExpect(redirectedUrl(DOWNLOAD_FILES_URL));

    }

    @Test
    public void NotFirstLoginStandardUserNotMulitEaThenScheduleYourPaymentIsReturned() throws Exception {

        EmployingAuthorityView eaView = EmployingAuthorityView.builder().eaCode("EA1234").build();
        OrganisationView orgView = OrganisationView.builder().eas(Collections.singletonList(eaView)).build();
        FinanceUser user = FinanceUser.builder().firstLogin(false).role("ROLE_STANDARD").organisations(Collections.singletonList(orgView)).build();

        FinanceUserStatusWrapper wrapper = FinanceUserStatusWrapper.builder()
            .financeUser(user)
            .status(AuthenticationStatus.AUTH_SUCCESS)
            .build();

        when(userLoginService.financeLogin(any(String.class),any(String.class))).thenReturn(wrapper);

        mockMvc.perform(formLogin())
            .andExpect(redirectedUrl(SCHEDULE_YOUR_PAYMENT_URL));

    }

    @Test
    public void NotFirstLoginAdminUserAndMultiEaUserThenMultiEaIsReturned() throws Exception {

        EmployingAuthorityView eaView1 = EmployingAuthorityView.builder().eaCode("EA111").build();
        EmployingAuthorityView eaView2 = EmployingAuthorityView.builder().eaCode("EA2222").build();
        List<EmployingAuthorityView> eaList = Arrays.asList(eaView1, eaView2);
        OrganisationView orgView = OrganisationView.builder().eas(eaList).build();
        FinanceUser user = FinanceUser.builder().firstLogin(false).role("ROLE_ADMIN").organisations(Collections.singletonList(orgView)).build();

        FinanceUserStatusWrapper wrapper = FinanceUserStatusWrapper.builder()
            .financeUser(user)
            .status(AuthenticationStatus.AUTH_SUCCESS)
            .build();

        when(userLoginService.financeLogin(any(String.class),any(String.class))).thenReturn(wrapper);

        mockMvc.perform(formLogin())
            .andExpect(redirectedUrl(MULTI_EA));

    }

    @Test
    public void NotFirstLoginStandardUserAndMultiEaUserThenMultiEaIsReturned() throws Exception {

        EmployingAuthorityView eaView1 = EmployingAuthorityView.builder().eaCode("EA111").build();
        EmployingAuthorityView eaView2 = EmployingAuthorityView.builder().eaCode("EA2222").build();
        List<EmployingAuthorityView> eaList = Arrays.asList(eaView1, eaView2);
        OrganisationView orgView = OrganisationView.builder().eas(eaList).build();
        FinanceUser user = FinanceUser.builder().firstLogin(false).role("ROLE_STANDARD").organisations(Collections.singletonList(orgView)).build();

        FinanceUserStatusWrapper wrapper = FinanceUserStatusWrapper.builder()
            .financeUser(user)
            .status(AuthenticationStatus.AUTH_SUCCESS)
            .build();

        when(userLoginService.financeLogin(any(String.class),any(String.class))).thenReturn(wrapper);

        mockMvc.perform(formLogin())
            .andExpect(redirectedUrl(MULTI_EA));

    }
}
