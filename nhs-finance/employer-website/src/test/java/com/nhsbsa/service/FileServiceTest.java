package com.nhsbsa.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.mockito.BDDMockito.given;

import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RequestForTransferServiceTest.class)
@WithMockUser
public class FileServiceTest {

  private FileService fileService;

  private BackendApiUriService backendApiUriService;

  private RestTemplate restTemplate;

  private static final String FILE_DATE = "2018-10-10";

  @Before
  public void before() {
    restTemplate = Mockito.mock(RestTemplate.class);

    backendApiUriService = new BackendApiUriService(
        "http",
        "host",
        "8080",
        "/employer-service-context"
    );

    fileService = Mockito.spy(new FileService(backendApiUriService, restTemplate));

  }

  @Test
  public void getFileListTest() {
    String[] expectedFiles = new String[0];

    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/file",
        String[].class)).willReturn(expectedFiles);

    Optional<String[]> actualFiles = fileService.getFiles();

    assertThat(actualFiles.get(), is(sameInstance((expectedFiles))));
  }

  @Test
  public void getFilesReturnsOptionalEmptyWhenHttpClientErrorExceptionThrown() {

    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/file",
        String[].class)).willThrow(HttpClientErrorException.class);

    Optional<String[]> actualFiles = fileService.getFiles();

    assertFalse(actualFiles.isPresent());
  }

  @Test
  public void getFileTest() {

    byte[] expectedFile = new byte[0];

    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/file/"+ FILE_DATE,
        byte[].class)).willReturn(expectedFile);

    Optional<byte[]> actualFile = fileService.getFile(FILE_DATE);

    assertThat(actualFile.get(), is(sameInstance((expectedFile))));
  }

  @Test
  public void getFileReturnsOptionalEmptyWhenHttpClientErrorExceptionThrown() {

    given(restTemplate
        .getForObject("http://host:8080/employer-service-context/finance/file/" + FILE_DATE,
            byte[].class)).willThrow(HttpClientErrorException.class);

    Optional<byte[]> actualFile = fileService.getFile(FILE_DATE);

    assertFalse(actualFile.isPresent());
  }

}