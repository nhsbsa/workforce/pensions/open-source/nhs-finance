package com.nhsbsa.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import com.nhsbsa.security.CreateEaRequest;
import com.nhsbsa.security.CreateOrganisationRequest;
import com.nhsbsa.security.CreateOrganisationResponse;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RequestForTransferServiceTest.class)
@WithMockUser
public class OrganisationServiceTest {

  private static final String EXPECTED_ORG = "EA1234";

  private OrganisationService organisationService;

  private BackendApiUriService backendApiUriService;

  private RestTemplate restTemplate;

  private final EmployingAuthorityView EA = EmployingAuthorityView.builder().eaCode("EA1234")
      .name("Name").employerType("").build();
  private final List<EmployingAuthorityView> EA_LIST = Collections.singletonList(EA);
  private final OrganisationView ORGANISATION = OrganisationView.builder()
      .identifier(UUID.fromString("a1ed70a7-966f-48b0-bcc8-351a5ff9c4ca")).name("Name").eas(EA_LIST)
      .build();

  @Before
  public void before() {
    restTemplate = Mockito.mock(RestTemplate.class);

    backendApiUriService = new BackendApiUriService(
        "http",
        "host",
        "8080",
        "/employer-service-context"
    );

    organisationService = Mockito.spy(new OrganisationService(backendApiUriService, restTemplate));

  }

  @Test
  public void getOrganisationByEACode() {

    given(restTemplate
        .getForObject("http://host:8080/employer-service-context/organisation/" + EXPECTED_ORG,
            OrganisationView.class)).willReturn(ORGANISATION);

    Optional<OrganisationView> actualOrg = organisationService
        .getOrganisationByEaCode(EXPECTED_ORG);

    assertThat(actualOrg.get(), is(sameInstance((ORGANISATION))));
  }

  @Test
  public void getOrganisationByEACodeReturnsEmpty() {

    given(organisationService.buildCreateOrganisationRequest(null, null, null))
        .willReturn(Optional.empty());

    Optional<OrganisationView> actualOrg = organisationService
        .getOrganisationByEaCode(EXPECTED_ORG);

    assertFalse(actualOrg.isPresent());
  }

  @Test
  public void registerOrganisation() {

    Optional<CreateOrganisationRequest> organisationRequest =
        organisationService.buildCreateOrganisationRequest(ORGANISATION.getEas().get(0).getEaCode(),
            ORGANISATION.getName(), ORGANISATION.getEas().get(0).getEmployerType());

    given(restTemplate.postForObject("http://host:8080/employer-service-context/organisation",
        organisationRequest.get(), OrganisationView.class)).willReturn(ORGANISATION);

    Optional<OrganisationView> actualOrg =
        organisationService
            .registerOrganisation(ORGANISATION.getEas().get(0).getEaCode(), ORGANISATION.getName(),
                ORGANISATION.getEas().get(0).getEmployerType());

    assertEquals(actualOrg.get().getEas().get(0).getEaCode(),
        ORGANISATION.getEas().get(0).getEaCode());

    assertEquals(actualOrg.get().getEas().get(0).getEaCode(),
        ORGANISATION.getEas().get(0).getEaCode());
  }

  @Test
  public void registerOrganisationReturnsEmpty() {

    given(organisationService.buildCreateOrganisationRequest(null, null, null))
        .willReturn(Optional.empty());

    Optional<OrganisationView> actualOrg = organisationService
        .registerOrganisation(null, null, null);

    assertEquals(actualOrg, Optional.empty());
  }

  @Test
  public void registerOrganisationWithoutEa() {
    String orgName = "Org Name";
    UUID orgUuid = UUID.randomUUID();

    CreateOrganisationRequest organisationRequest = CreateOrganisationRequest.builder()
        .name(orgName)
        .build();

    CreateOrganisationResponse organisationResponse = CreateOrganisationResponse.builder()
        .name(orgName)
        .uuid(orgUuid.toString())
        .build();

    given(restTemplate.postForObject("http://host:8080/employer-service-context/organisation-no-ea", organisationRequest, CreateOrganisationResponse.class))
        .willReturn(organisationResponse);

    Optional<CreateOrganisationResponse> actualOrg =
        organisationService.registerOrganisationWithoutEa(orgName);

    assertEquals(actualOrg.get().getName(), orgName);
    assertEquals(actualOrg.get().getUuid(), orgUuid.toString());
  }

  @Test
  public void registerOrganisationWithoutEaReturnsEmpty() {
    String orgName = "Org Name";

    CreateOrganisationRequest organisationRequest = CreateOrganisationRequest.builder()
        .name(orgName)
        .build();

    given(restTemplate.postForObject("http://host:8080/employer-service-context/organisation-no-ea", organisationRequest, CreateOrganisationResponse.class))
        .willReturn(null);

    Optional<CreateOrganisationResponse> actualOrg =
        organisationService.registerOrganisationWithoutEa(orgName);

    assertFalse(actualOrg.isPresent());

  }

  @Test
  public void registerEa() {
    UUID orgUuid = UUID.fromString("a1ed70a7-966f-48b0-bcc8-351a5ff9c4ca");

    CreateEaRequest eaRequest = CreateEaRequest.builder()
        .eaCode("EA1234")
        .eaName("Name")
        .employerType("STAFF")
        .organisationUuid(orgUuid.toString())
        .build();

    given(restTemplate.postForObject("http://host:8080/employer-service-context/employing-authority", eaRequest, OrganisationView.class))
        .willReturn(ORGANISATION);

    Optional<OrganisationView> actualOrg =
        organisationService.registerEa(eaRequest);

    assertEquals(actualOrg.get().getName(), ORGANISATION.getName());
    assertEquals(actualOrg.get().getIdentifier().toString(), orgUuid.toString());
    assertEquals(actualOrg.get().getEas(), EA_LIST);
  }

  @Test
  public void registerEaReturnsEmpty() {
    UUID orgUuid = UUID.fromString("a1ed70a7-966f-48b0-bcc8-351a5ff9c4ca");

    CreateEaRequest eaRequest = CreateEaRequest.builder()
        .eaCode("EA1234")
        .eaName("Name")
        .employerType("STAFF")
        .organisationUuid(orgUuid.toString())
        .build();

    given(restTemplate.postForObject("http://host:8080/employer-service-context/employing-authority", eaRequest, OrganisationView.class))
        .willReturn(null);

    Optional<OrganisationView> actualOrg =
        organisationService.registerEa(eaRequest);

    assertFalse(actualOrg.isPresent());

  }

  @Test
  public void getOrganisationByNameReturnsResult() {
    String orgName = "Organisation Name";

    CreateOrganisationRequest orgRequest = CreateOrganisationRequest.builder()
        .name("Organisation Name")
        .build();

    given(restTemplate.postForObject("http://host:8080/employer-service-context/organisation-name", orgRequest, String.class))
        .willReturn(orgName);

    Optional<String> response =
        organisationService.getOrganisationByName(orgName);

    assertTrue(response.isPresent());
    assertEquals(response.get(), orgName);

  }

  @Test
  public void getOrganisationByNameReturnsNull() {
    String orgName = "Organisation Name";
    CreateOrganisationRequest orgRequest = CreateOrganisationRequest.builder()
        .name("Organisation Name")
        .build();

    given(restTemplate.postForObject("http://host:8080/employer-service-context/organisation-name", orgRequest, String.class))
        .willReturn(null);

    Optional<String> response =
        organisationService.getOrganisationByName(orgName);

    assertFalse(response.isPresent());

  }

  @Test
  public void getEaByEaCodeReturnsResult() {
    String eaCode = "EA1234";

    given(restTemplate.getForObject("http://host:8080/employer-service-context/employing-authority/EA1234", String.class))
        .willReturn(eaCode);

    Optional<String> response =
        organisationService.getEaByEaCode(eaCode);

    assertTrue(response.isPresent());
    assertEquals(response.get(), eaCode);

  }

  @Test
  public void getEaByEaCodeReturnsNull() {
    String eaCode = "EA1234";

    given(restTemplate.getForObject("http://host:8080/employer-service-context/employing-authority/EA1234", String.class))
        .willReturn(null);

    Optional<String> response =
        organisationService.getEaByEaCode(eaCode);

    assertFalse(response.isPresent());

  }
}