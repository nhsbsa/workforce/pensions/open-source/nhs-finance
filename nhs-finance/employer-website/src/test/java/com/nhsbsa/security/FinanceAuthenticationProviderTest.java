package com.nhsbsa.security;

import com.nhsbsa.service.authentication.FinanceAuthenticationService;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class FinanceAuthenticationProviderTest {

  private static final String USER_NAME = "USER-NAME";
  private static final String PASSWORD = "PASS";

  @Mock
  FinanceAuthenticationService service;

  FinanceAuthenticationProvider provider;

  Authentication auth;

  @Before
  public void setup(){

    provider = new FinanceAuthenticationProvider(service);

    auth = new Authentication() {
      @Override
      public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
      }

      @Override
      public Object getCredentials() {
        return PASSWORD;
      }

      @Override
      public Object getDetails() {
        return null;
      }

      @Override
      public Object getPrincipal() {
        return null;
      }

      @Override
      public boolean isAuthenticated() {
        return false;
      }

      @Override
      public void setAuthenticated(boolean b) throws IllegalArgumentException {

      }

      @Override
      public String getName() {
        return USER_NAME;
      }
    };
  }


  @Test
  public void userAuthenticationTest(){

    provider.authenticate(auth);

    verify(service, times(0)).authenticate(USER_NAME, PASSWORD);
    verify(service, times(1)).authenticate(USER_NAME.toLowerCase(), PASSWORD);

  }

}
