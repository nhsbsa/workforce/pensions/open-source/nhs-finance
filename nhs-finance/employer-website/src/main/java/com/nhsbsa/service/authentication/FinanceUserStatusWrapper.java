package com.nhsbsa.service.authentication;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.security.AuthenticationResponse.AuthenticationStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class FinanceUserStatusWrapper {

  private FinanceUser financeUser;

  private AuthenticationStatus status;

}
