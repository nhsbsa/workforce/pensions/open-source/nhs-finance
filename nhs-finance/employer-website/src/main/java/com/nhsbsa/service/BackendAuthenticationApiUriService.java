package com.nhsbsa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Created by Mark Lishman on 18/08/2016.
 */

@Service
public class BackendAuthenticationApiUriService {

    private UriComponentsBuilder uriComponentsBuilder;

    @Autowired
    public BackendAuthenticationApiUriService(@Value("${authorization.protocol}") final String backendProtocol,
                                @Value("${authorization.url}") final String backendHost,
                                @Value("${authorization.port}") final String backendPort,
                                @Value("${authorization.context}") final String backendContext) {

        uriComponentsBuilder = UriComponentsBuilder
                .newInstance()
                .scheme(backendProtocol)
                .host(backendHost)
                .port(backendPort)
                .path(backendContext);
    }

    public BackendUri path(String path) {
        UriComponents uriComponents = uriComponentsBuilder
                .cloneBuilder()
                .path(path)
                .build();
        return new BackendUri(uriComponents);
    }

}
