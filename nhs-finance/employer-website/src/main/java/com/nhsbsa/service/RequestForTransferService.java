package com.nhsbsa.service;

import com.nhsbsa.model.EmployerTypes;
import com.nhsbsa.view.RequestForTransferView;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Persistence services for {@code RequestForTransfer} entity.
 *
 * Created by Mark Lishman on 18/08/2016.
 *
 * @see RequestForTransferView
 */
@Slf4j
@Service
public class RequestForTransferService {

  @SuppressWarnings("squid:S1075") // https://next.sonarqube.com/sonarqube/coding_rules#rule_key=squid%3AS1075
  private static final String GET_REQUEST_FOR_TRANSFER_PATH = "/finance/requestfortransfer/{rftId}";
  @SuppressWarnings("squid:S1075")
  private static final String SAVE_REQUEST_FOR_TRANSFER_PATH = "/finance/requestfortransfer";
  @SuppressWarnings("squid:S1075")
  private static final String GET_EMPLOYER_TYPE_PATH = "/employer-type/{eaCode}";
  @SuppressWarnings("squid:S1075")
  private static final String GET_SUBMIT_DATE_PATH = "/finance/submitdate/{eaCode}";

  private final RestTemplate restTemplate;
  private final BackendUri getRequestForTransferUri;
  private final BackendUri saveRequestForTransferUri;
  private final BackendUri getEmployerTypeUri;
  private final BackendUri getSubmitDateUri;


  @Autowired
  public RequestForTransferService(final BackendApiUriService backendApiUriService,
      final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
    this.getRequestForTransferUri = backendApiUriService.path(GET_REQUEST_FOR_TRANSFER_PATH);
    this.saveRequestForTransferUri = backendApiUriService.path(SAVE_REQUEST_FOR_TRANSFER_PATH);
    this.getEmployerTypeUri = backendApiUriService.path(GET_EMPLOYER_TYPE_PATH);
    this.getSubmitDateUri = backendApiUriService.path(GET_SUBMIT_DATE_PATH);
  }

  public RequestForTransferView getRequestForTransferByRftUuid(final String rftUuid) {
    final String uri = getRequestForTransferUri.params(rftUuid);
    return restTemplate.getForObject(uri, RequestForTransferView.class);
  }

  public RequestForTransferView saveRequestForTransfer(final RequestForTransferView requestForTransfer) {

    if(requestForTransfer.getRftUuid()==null) { // Get EmployerType for EaCode to save
      final String eaUri = getEmployerTypeUri.params(requestForTransfer.getEaCode());
      String empType = restTemplate.getForObject(eaUri, String.class);
      requestForTransfer.setIsGp(EmployerTypes.GP.name().equals(empType));
    }

    final String uri = saveRequestForTransferUri.toUri();

    return restTemplate.postForObject(uri, requestForTransfer, RequestForTransferView.class);
  }

  public RequestForTransferView saveContributionPayment(final String rftUuid,
      final RequestForTransferView requestForTransfer) {

    final RequestForTransferView rft = getRequestForTransferByRftUuid(rftUuid);
    rft.setTotalPensionablePay(requestForTransfer.getTotalPensionablePay());
    rft.setEmployeeContributions(requestForTransfer.getEmployeeContributions());
    rft.setEmployerContributions(requestForTransfer.getEmployerContributions());
    rft.setEmployeeAddedYears(requestForTransfer.getEmployeeAddedYears());
    rft.setAdditionalPension(requestForTransfer.getAdditionalPension());
    rft.setErrbo(requestForTransfer.getErrbo());
    rft.setTotalDebitAmount(requestForTransfer.getTotalDebitAmount());
    rft.setAdjustmentsRequired(requestForTransfer.getAdjustmentsRequired());

    rft.setAdjustment(requestForTransfer.getAdjustment());

    saveRequestForTransfer(rft);
    return rft;
  }

  public RequestForTransferView submitContributionPayment(final String rftUuid) {

    final RequestForTransferView rft = getRequestForTransferByRftUuid(rftUuid);

    Date now = Date.from(Instant.now());
    rft.setSubmitDate(now);

    saveRequestForTransfer(rft);
    return rft;
  }


  /**
   * Returns the {@code RequestForTransfer} entity identified by {@code rftUuid} in an {@code
   * Optional}, or an empty {@code Optional} if no entity was found with the uuid provided.
   *
   * @param rftUuid a uuid of a {@code RequestForTransfer} entity that may or may not exist. If the
   * uuid is not in a valid format as defined by {@link java.util.UUID}, the method will throw an
   * {@code IllegalArgumentException}.
   * @return Loaded entity, if any, or an empty {@code Optional} otherwise.
   * @throws IllegalArgumentException if {@code rftUuid} is not a syntactically valid {@link
   * java.util.UUID}.
   */
  public Optional<RequestForTransferView> getRequestForTransferByRftUuidValidating(String rftUuid) {
    try {
      // For side-affect (exception) only
      UUID.fromString(rftUuid);
      return Optional.ofNullable(getRequestForTransferByRftUuid(rftUuid));
    } catch (IllegalArgumentException notAUuid) {
      log.debug("Illegal parameter value '{}' passed as uuid", rftUuid);
      throw notAUuid;
    }
  }

  public Optional<RequestForTransferView> getOptionalRequestForTransferByRftUuid(String rftUuid) {

      return Optional.ofNullable(getRequestForTransferByRftUuid(rftUuid));
  }

  public Optional<LocalDate> getSubmitDateByEaCode(final String eaCode) {

    final String uri = getSubmitDateUri.params(eaCode);
    return Optional.ofNullable(restTemplate.getForObject(uri, LocalDate.class));
  }
}
