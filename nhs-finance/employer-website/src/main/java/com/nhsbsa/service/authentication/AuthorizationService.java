package com.nhsbsa.service.authentication;

import com.nhsbsa.exceptions.UserNotFoundAuthenticationException;
import com.nhsbsa.model.AccountUser;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.ForgottenPassword;
import com.nhsbsa.model.User;
import com.nhsbsa.model.UserRoles;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.AuthenticationResponse.AuthenticationStatus;
import com.nhsbsa.security.LoginRequest;
import com.nhsbsa.security.RegistrationRequest;
import com.nhsbsa.security.ResetEmailTokenResponse;
import com.nhsbsa.security.SetPassword;
import com.nhsbsa.security.SetPasswordResponse;
import com.nhsbsa.security.ValidateEmailResponse;
import com.nhsbsa.service.BackendAuthenticationApiUriService;
import com.nhsbsa.service.BackendUri;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by jeffreya on 24/10/2016.
 */

@Log4j
@Service
@SuppressWarnings("squid:S1075")
public class AuthorizationService {

    private final RestTemplate ficRestTemplate;
    private final BackendUri getUserUri;
    private final BackendUri getUserByUuidUri;
    private final BackendUri deleteUserByUuidUri;

    @Value("${authorization.authenticate.url}")
    private String authorizationBackendUri;

    @Value("${finance.authenticate.url}")
    private String financeAuthUrl;

    @Value("${finance.details.url}")
    private String financeEmailUrl;

    @Value("${authorization.authenticate.url}/update-password")
    private String authorizationBackendUpdateDetailsUri;

    @Value("${authorization.authenticate.url}/validate-password")
    private String authorizationBackendValidateDetailsUri;

    @Value("${authorization.register.url}")
    private String authorizationBackendRegisterUserUri;

    @Value("${authorization.authenticate.url}/validate-email")
    private String authorizationBackendResetPassUri;

    @Value("${authorization.authenticate.url}/verify-token")
    private String authorizationBackendVerifyTokenUri;

    @Value("${authorization.authenticate.url}/delete-token")
    private String authorizationBackendDeleteTokenUri;

    private static final String GET_USER_BY_EMAIL_PATH = "/user/email/{email}/";

    private static final String GET_USER_ROLES = "/user/{uuid}";

    private static final String DELETE_USER = "/user/delete-user/{uuid}";

    @Autowired
    protected AuthorizationService(final BackendAuthenticationApiUriService backendAuthenticationApiUriService, final RestTemplate ficRestTemplate) {
        this.ficRestTemplate = ficRestTemplate;
        this.getUserUri = backendAuthenticationApiUriService.path(GET_USER_BY_EMAIL_PATH);
        this.getUserByUuidUri = backendAuthenticationApiUriService.path(GET_USER_ROLES);
        this.deleteUserByUuidUri = backendAuthenticationApiUriService.path(DELETE_USER);
    }

    public FinanceUserStatusWrapper authenticateAndRetrieveFinanceUser(final LoginRequest loginRequest) {
        Optional<AuthenticationResponse> maybeAuthenticationResponse = authenticate(loginRequest);

        return maybeAuthenticationResponse.flatMap(response -> {

            if (AuthenticationStatus.AUTH_SUCCESS.equals(response.getAuthenticationStatus())) {

                loginRequest.setUuid(response.getUuid());
                loginRequest.setFirstLogin(response.getFirstLogin());

                Optional<FinanceUser> maybeFinanceUser = retrieveFinanceUser(loginRequest);

                maybeFinanceUser.ifPresent(fUser -> fUser.setRole(response.getRole()));

                return maybeFinanceUser.map(financeUser ->
                    FinanceUserStatusWrapper.builder()
                        .financeUser(financeUser)
                        .status(AuthenticationStatus.AUTH_SUCCESS)
                        .build()
                );
            } else {
                return Optional.of(
                    FinanceUserStatusWrapper.builder()
                        .status(response.getAuthenticationStatus())
                        .build()
                );
            }

        }).orElseThrow(UserNotFoundAuthenticationException::new);
    }

    private Optional<FinanceUser> retrieveFinanceUser(LoginRequest loginRequest) {
        try {
            return Optional.of(
                ficRestTemplate.postForObject(financeAuthUrl, loginRequest, FinanceUser.class));
        } catch (Exception e) {
            log.error("Failed to retrieve finance user", e);
            return Optional.empty();
        }
    }

    public Optional<AuthenticationResponse> authenticate(final LoginRequest loginRequest) {
        try {
            AuthenticationResponse response = ficRestTemplate
                    .postForObject(authorizationBackendUri, loginRequest, AuthenticationResponse.class);

            if (response.getAuthenticationStatus() != null) {
                return Optional.of(response);
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            log.error("Failed to authenticate login request", e);
            return Optional.empty();
        }
    }

    public SetPasswordResponse savePassword(final SetPassword setPassword) {
        try {
            return ficRestTemplate.postForObject(authorizationBackendUpdateDetailsUri, setPassword, SetPasswordResponse.class);
        } catch (Exception e) {
            log.error(e);
        }
        log.error("Failed to save password");
        return null;
    }

    public boolean validatePassword(final SetPassword setPassword) {
        try {

            final SetPasswordResponse setPasswordResponse =
                    ficRestTemplate
                        .postForObject(authorizationBackendValidateDetailsUri, setPassword, SetPasswordResponse.class);

            return setPasswordResponse.isPasswordSameAsPrevious();
        } catch (Exception e) {
            log.error(e);
        }
        log.error("Failed to validate password");
        return false;
    }

    public AuthenticationResponse registerUser(final RegistrationRequest registrationRequest) {
        try {
            return ficRestTemplate
                    .postForObject(authorizationBackendRegisterUserUri, registrationRequest,
                            AuthenticationResponse.class);
        } catch (Exception e) {
            log.error(e);
        }
        log.error(String.format("Failed to register user %s", registrationRequest.getUsername()));
        return null;
    }

    public ValidateEmailResponse checkUsername(final ForgottenPassword forgottenPassword) {
        try {
            return ficRestTemplate.postForObject(authorizationBackendResetPassUri, forgottenPassword, ValidateEmailResponse.class);

        } catch (Exception e) {
            log.error(e);
        }
        log.error(String.format("Failed to get Username = %s", forgottenPassword.getEmail()));
        return null;
    }

    public FinanceUser sendEmail(final ValidateEmailResponse validateEmailResponse) {

        return ficRestTemplate.postForObject(financeEmailUrl, validateEmailResponse, FinanceUser.class);
    }

    // Check email token is Not null and exists
    public Optional<ResetEmailTokenResponse> getOptionalUuidByEmailToken(String emailToken) {

        return Optional.ofNullable(getUuidByEmailToken(emailToken));
    }

    public ResetEmailTokenResponse getUuidByEmailToken(final String emailToken) {

        try {

            return ficRestTemplate.postForObject(authorizationBackendVerifyTokenUri, emailToken, ResetEmailTokenResponse.class);

        } catch (Exception e) {
            log.error(e);
        }
        log.error("Failed to verify Token");
        return null;
    }

    public boolean deleteEmailToken(final String emailToken) {

        try {

            return ficRestTemplate
                .postForObject(authorizationBackendDeleteTokenUri, emailToken, boolean.class);

        } catch (Exception e) {
            log.error(e);
        }
        log.error("Failed to delete Token");
        return false;
    }

    public Optional<User> getUserByEmail(final String email){

        try {
            return Optional.ofNullable(ficRestTemplate.getForObject(getUserUri.params(email), User.class));
        } catch (Exception e) {
            log.error("Failed getting authentication user", e);
            return Optional.empty();
        }
    }

    public List<AccountUser> getStandardUsers(List<FinanceUser> financeUsers) {
        List<AccountUser> accountUsers = new ArrayList<>();

        for (FinanceUser financeUser : financeUsers) {

            // Need to get User Role from Authorization DB for each financeUser

            getUserByUuid(financeUser.getUuid())
                .filter(user -> user.getRole().equals(UserRoles.ROLE_STANDARD.name())
                    && !user.userIsDeleted()) // Only get Standard users that aren't deleted
            .ifPresent(users -> accountUsers.add(AccountUser.builder()
                .name(financeUser.getFirstName() + ' ' + financeUser.getLastName())
                .email(financeUser.getContactEmail())
                .userRole(financeUser.getRole())
                .uuid(financeUser.getUuid())
                .isAdmin(false)
                .build()));
        }

        return accountUsers;
    }

    public Optional<User> getUserByUuid( String uuid) {

        try {

            return Optional.ofNullable(
                ficRestTemplate.getForObject(getUserByUuidUri.params(uuid), User.class));

        } catch (Exception e) {
            log.error("Failed getting authentication user with uuid", e);
            return Optional.empty();
        }
    }

    public Optional<User> deleteUser(final String uuid) {

        try {

            return Optional.of(
                ficRestTemplate
                .postForObject(deleteUserByUuidUri.params(uuid), uuid, User.class));

        } catch (Exception e) {
            log.error(e);
        }
        log.error("Failed to delete User");
        return Optional.empty();
    }




}
