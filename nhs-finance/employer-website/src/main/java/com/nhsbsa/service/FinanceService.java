package com.nhsbsa.service;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.security.EmailRequest;
import com.nhsbsa.security.EmailResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class FinanceService {

  @SuppressWarnings("squid:S1075")
  private static final String CREATE_USER = "/finance/user";

  @SuppressWarnings("squid:S1075")
  private static final String GET_FINANCE_USER_PATH = "/finance/user/{uuid}";
  @SuppressWarnings("squid:S1075")
  private static final String GET_FINANCE_USER_BY_ORG_PATH = "/finance/org-users/{orgUuid}";

  @SuppressWarnings("squid:S1075")
  private static final String SEND_STANDARD_USER_EMAIL = "/finance/standard-user-email";

  private final RestTemplate ficRestTemplate;
  private final BackendUri createUserUri;
  private final BackendUri getFinanceUserUri;
  private final BackendUri getFinanceUsersByOrgUri;
  private final BackendUri standardUserEmailUri;

  @Autowired
  public FinanceService(final BackendApiUriService backendApiUriService,
      final RestTemplate ficRestTemplate) {
    this.ficRestTemplate = ficRestTemplate;
    this.createUserUri = backendApiUriService.path(CREATE_USER);
    this.getFinanceUserUri = backendApiUriService.path(GET_FINANCE_USER_PATH);
    this.getFinanceUsersByOrgUri = backendApiUriService.path(GET_FINANCE_USER_BY_ORG_PATH);
    this.standardUserEmailUri = backendApiUriService.path(SEND_STANDARD_USER_EMAIL);
  }

  public Optional<FinanceUser> getFinanceUserByUuid(final String uuid) {
    final String uri = getFinanceUserUri.params(uuid);

    try {

      return Optional.of(ficRestTemplate.getForObject(uri, FinanceUser.class));

    } catch (HttpClientErrorException e) {
      log.error("User not found");
    }
    return Optional.empty();
  }

  public List<FinanceUser> getFinanceUserByOrgUuid(final String orgUuid) {
    final String uri = getFinanceUsersByOrgUri.params(orgUuid);

    try {

      return Optional.ofNullable(ficRestTemplate.getForObject(uri, FinanceUser[].class)).map(Arrays::asList).orElseGet(ArrayList::new);

    } catch (HttpClientErrorException e) {
      log.error("User not found");
    }

    return Collections.emptyList();
  }

  public CreateUserResponse createUser(final CreateUserRequest createUserRequest) {

    try {
      return ficRestTemplate
          .postForObject(createUserUri.toUri(), createUserRequest, CreateUserResponse.class);

    } catch (Exception e) {
      log.error(e.getMessage());
    }
    log.error(String.format("Failed to register user %s", createUserRequest.getUsername()));
    return null;

  }

  public Optional<EmailResponse> sendStandardUserCreatedEmail(EmailRequest emailRequest) {

    try {
      return Optional.ofNullable(ficRestTemplate
          .postForObject(standardUserEmailUri.toUri(), emailRequest, EmailResponse.class));
    } catch (Exception e) {
      log.error(e.getMessage());
    }
    log.error(String.format("Failed to send email to  %s", emailRequest.getEmail()));
    return Optional.empty();
  }
}
