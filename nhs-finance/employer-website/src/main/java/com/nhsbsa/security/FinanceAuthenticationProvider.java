package com.nhsbsa.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.nhsbsa.service.authentication.FinanceAuthenticationService;

/**
 * Created by jeffreya on 19/08/2016.
 */

@Component
public class FinanceAuthenticationProvider implements AuthenticationProvider {

    private final FinanceAuthenticationService financeAuthenticationService;

    @Autowired
    public FinanceAuthenticationProvider(final FinanceAuthenticationService financeAuthenticationService) {
        this.financeAuthenticationService = financeAuthenticationService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) {
        final String name = authentication.getName().toLowerCase();
        final String password = authentication.getCredentials().toString();
        return this.financeAuthenticationService.authenticate(name, password);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}