package com.nhsbsa.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;


@Slf4j
public class UrlAuthenticationFailureHandler implements AuthenticationFailureHandler {


    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
        HttpServletResponse response, AuthenticationException e)
        throws IOException, ServletException {


        log.error("Failure handler error", e);

        if ( e instanceof LockedException){
            redirectStrategy.sendRedirect(request, response, "/login?locked");
        } else {
            redirectStrategy.sendRedirect(request, response, "/login?error");
        }
    }
}