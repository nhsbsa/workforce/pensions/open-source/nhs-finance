package com.nhsbsa.exceptions;

public class AlreadySubmittedException extends Exception {

  public AlreadySubmittedException() {
    super();
  }
}
