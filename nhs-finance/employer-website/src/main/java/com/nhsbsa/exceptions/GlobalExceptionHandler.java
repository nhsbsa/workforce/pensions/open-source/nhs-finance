package com.nhsbsa.exceptions;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

  private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  @ExceptionHandler(AlreadySubmittedException.class)
  public String handleAlreadySubmittedxception(HttpServletRequest request,
      HttpServletResponse response, Exception ex)
      throws IOException {
    logger.info("Already Submitted Exception Occured: URL = " + request.getRequestURL());
    logoutCurrentSession();

    response.sendRedirect("/login");
    return null;
  }

  private void logoutCurrentSession() {

    SecurityContextHolder.getContext().setAuthentication(null);
  }
}
