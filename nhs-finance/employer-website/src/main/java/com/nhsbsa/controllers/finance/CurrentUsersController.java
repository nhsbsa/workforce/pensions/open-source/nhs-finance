package com.nhsbsa.controllers.finance;

import com.nhsbsa.model.AccountUser;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.service.FinanceService;
import com.nhsbsa.service.authentication.AuthorizationService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@PreAuthorize("hasRole('ADMIN')")
public class CurrentUsersController {

  private static final String CURRENT_USERS_VIEW_NAME = "currentusers";

  private static final String CURRENT_USERS_ENDPOINT = "/current-users";

  private final AuthorizationService authorizationService;
  private final FinanceService financeService;

  @Autowired
  public CurrentUsersController(AuthorizationService authorizationService, FinanceService financeService) {
    this.authorizationService = authorizationService;
    this.financeService = financeService;
  }

  @GetMapping(value = CURRENT_USERS_ENDPOINT)
  public ModelAndView showCurrentUsersPage() {
    log.info("GET - Accessing to Create Standard Employer screen");

    List<AccountUser> accountUsers = new ArrayList<>();
    Optional<FinanceUser> currentUser = getCurrentUser();
    // Add current user to accountUsers list
    if(currentUser.isPresent()){
      accountUsers.add(AccountUser.builder()
          .name(currentUser.get().getFirstName() + ' ' + currentUser.get().getLastName())
          .email(currentUser.get().getContactEmail())
          .userRole(currentUser.get().getRole())
          .uuid(currentUser.get().getUuid())
          .isAdmin(true)
          .build());

      // Get list of users from Employer DB with same Organisation ID
      //TODO Change getOrganisation().get(0) once we know how to handle multiple orgs
      List<FinanceUser> financeUsers = financeService.getFinanceUserByOrgUuid(currentUser.get().getOrganisations().get(0).getIdentifier().toString());
      // Get Standard Users for same Organisation Id
      accountUsers.addAll(authorizationService.getStandardUsers(financeUsers));
    }


    ModelAndView modelAndView = new ModelAndView(CURRENT_USERS_VIEW_NAME);

    modelAndView.addObject("users", accountUsers);
    return modelAndView;
  }


  public Optional<FinanceUser> getCurrentUser() {

    final Object details = SecurityContextHolder.getContext().getAuthentication().getDetails();
    if (details instanceof FinanceUser) {
      return Optional.of((FinanceUser) details);
    }

    return Optional.empty();

  }

}
