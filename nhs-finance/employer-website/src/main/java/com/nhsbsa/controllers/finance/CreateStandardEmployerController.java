package com.nhsbsa.controllers.finance;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.StandardEmployerUser;
import com.nhsbsa.model.User;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.security.EmailRequest;
import com.nhsbsa.security.EmailResponse;
import com.nhsbsa.service.FinanceService;
import com.nhsbsa.service.authentication.AuthorizationService;
import com.nhsbsa.service.authentication.UserRegistrationService;
import java.io.IOException;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@PreAuthorize("hasRole('ADMIN')")
public class CreateStandardEmployerController {

  private static final String CREATE_STANDARD_EMPLOYER_VIEW_NAME = "createStandardEmployer";

  private static final String CREATE_STANDARD_EMPLOYER_ENDPOINT = "/create-standard-employer";

  private static final String EMPLOYER_CREATED_FORWARD = "forward:/employer-created";

  private static final String ENDPOINT = "endpointAttribute";

  private static final String CURRENT_USERS_ENDPOINT = "/current-users";

  private final UserRegistrationService userRegistrationService;
  private final AuthorizationService authorizationService;
  private final FinanceService financeService;

  static final String NEW_USER_NAME = "firstName";

  @Autowired
  public CreateStandardEmployerController(final UserRegistrationService userRegistrationService, AuthorizationService authorizationService, FinanceService financeService) {
    this.userRegistrationService = userRegistrationService;
    this.authorizationService = authorizationService;
    this.financeService = financeService;
  }

  @GetMapping(value = CREATE_STANDARD_EMPLOYER_ENDPOINT)
  public ModelAndView showCreateStandardEmployerPage() {
    log.info("GET - Accessing to Create Standard Employer screen");

    ModelAndView modelAndView = new ModelAndView(CREATE_STANDARD_EMPLOYER_VIEW_NAME);

    modelAndView.addObject("standardEmployerUser", new StandardEmployerUser());
    return modelAndView;
  }

  @PostMapping(value = "/create-standard-employer")
  public String createStandardEmployer(
      @ModelAttribute @Valid StandardEmployerUser newStandardEmployerUser,
      final BindingResult bindingResult, ModelMap model,
      HttpServletResponse httpResponse) throws IOException {

    log.info("POST - Create standard employer");

    if (bindingResult.hasErrors()) {
      return CREATE_STANDARD_EMPLOYER_VIEW_NAME;
    }

    Optional<FinanceUser> currentUser = getCurrentUser();

    if(!currentUser.isPresent()){
        httpResponse.sendError(500);
        return null;
    }

    String email = newStandardEmployerUser.getEmail().toLowerCase();
    log.info("Create standard employer - Retrieving user by email = {}", email);
    Optional<User> maybeAuthenticationUser = authorizationService.getUserByEmail(email);
    String uuid;
    if (maybeAuthenticationUser.isPresent()) {

      uuid = maybeAuthenticationUser.get().getUuid();

      log.info("Create standard employer - Retrieving finance user by uuid = {}", uuid);
      Optional<FinanceUser> maybeFinanceUser = financeService.getFinanceUserByUuid(uuid);

      if (maybeFinanceUser.isPresent()) {

        model.addAttribute("userExists", true);
        return CREATE_STANDARD_EMPLOYER_VIEW_NAME;
      }

      newStandardEmployerUser.setUuid(uuid);
    } else{

      log.info("Create standard employer - Registering authentication user");
      Optional<AuthenticationResponse> response = registerAuthenticationUser(newStandardEmployerUser);
      if (!response.isPresent()) {

        httpResponse.sendError(500);
        return null;
      }

      uuid = response.get().getUuid();
    }

    newStandardEmployerUser.setUuid(uuid);
    newStandardEmployerUser.setOrganisations(currentUser.get().getOrganisations());

    log.info("Create standard employer - Registering employer user");
    Optional<CreateUserResponse> response = registerEmployerUser(newStandardEmployerUser);

    if (!response.isPresent()) {

      httpResponse.sendError(500);
      return null;
    }

    //send email to new standard user
    Optional<EmailResponse> emailResponse = financeService.sendStandardUserCreatedEmail(EmailRequest.builder()
        .firstName(newStandardEmployerUser.getFirstName())
        .lastName(newStandardEmployerUser.getLastName())
        .email(newStandardEmployerUser.getEmail())
        .createdBy(currentUser.get().getFirstName()+' '+currentUser.get().getLastName()).build());

    if ( emailResponse.isPresent()){
      log.info("Create standard employer - Email sent");
    } else {
      log.info("Create standard employer - Email not sent");
    }

    model.addAttribute(NEW_USER_NAME, emailResponse.map(EmailResponse::getName).orElse(""));
    model.addAttribute(ENDPOINT, CURRENT_USERS_ENDPOINT);


    return EMPLOYER_CREATED_FORWARD;
  }

  private Optional<CreateUserResponse> registerEmployerUser(StandardEmployerUser standardEmployerUser) {

    return userRegistrationService.createEmployerUser(standardEmployerUser);
  }

  private Optional<AuthenticationResponse> registerAuthenticationUser(StandardEmployerUser standardEmployerUser) {

    return userRegistrationService.createAuthenticationUser(standardEmployerUser);
  }


  Optional<FinanceUser> getCurrentUser() {

    final Object details = SecurityContextHolder.getContext().getAuthentication().getDetails();
    if (details instanceof FinanceUser) {
      return Optional.of((FinanceUser) details);
    }

    return Optional.empty();

  }

}
