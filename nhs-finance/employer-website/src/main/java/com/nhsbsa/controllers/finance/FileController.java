package com.nhsbsa.controllers.finance;

import com.nhsbsa.service.FileService;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@PreAuthorize("hasRole('MASTER')")
@SessionAttributes("adminEmployerUser")
public class FileController {

  private static final String DOWNLOAD_FILES_VIEW_NAME = "downloadFiles";
  private static final String DOWNLOAD_FILES_ENDPOINT = "/download-files";

  private static final String FILENAME_PREFIX = "ficposts_";
  private static final String CSV_SUFFIX = ".csv";

  private final FileService fileService;

  @Autowired
  public FileController(final FileService fileService) {

    this.fileService = fileService;
  }

  @GetMapping(value = DOWNLOAD_FILES_ENDPOINT)
  public ModelAndView showDownloadFilesPage() {

    log.info("GET - Accessing to Download Files screen");

    ModelAndView modelAndView = new ModelAndView(DOWNLOAD_FILES_VIEW_NAME);
    modelAndView.addObject("fileList", fileService.getFiles().orElse(new String[0]));
    return modelAndView;
  }

  @GetMapping(value = DOWNLOAD_FILES_ENDPOINT + "/{fileDate}")
  public HttpEntity<byte[]> downloadFile(
      @PathVariable("fileDate") String fileDate,
      HttpServletResponse response) throws IOException {

    log.info("GET - Retrieving file with date = {}", fileDate);

    if (!isStringDateValid(fileDate)) {
      response.sendError(404);
      return null;
    }

    Optional<byte[]> maybeFile = fileService.getFile(fileDate);

    if (!maybeFile.isPresent()) {
      response.sendError(404);
      return null;
    }

    byte[] documentBody = maybeFile.get();
    HttpHeaders header = buildHeader(fileDate, documentBody.length);

    return new HttpEntity<>(documentBody, header);
  }

  private boolean isStringDateValid(String fileDate) {
    String dateFormat = "uuuu-MM-dd";
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter
        .ofPattern(dateFormat)
        .withResolverStyle(ResolverStyle.STRICT);

    try {
      LocalDate.parse(fileDate, dateTimeFormatter);
      return true;
    } catch (DateTimeParseException e) {
      log.error("invalid Date format");
      return false;
    }
  }

  private HttpHeaders buildHeader(String fileDate, int length) {
    HttpHeaders header = new HttpHeaders();
    header.setContentType(MediaType.TEXT_PLAIN);
    header.set(HttpHeaders.CONTENT_DISPOSITION,
        String
            .format("attachment; filename=%s%s_%s_%s%s", FILENAME_PREFIX, fileDate.substring(8, 10),
                fileDate.substring(5, 7), fileDate.substring(0, 4), CSV_SUFFIX));
    header.setContentLength(length);

    return header;
  }

}
