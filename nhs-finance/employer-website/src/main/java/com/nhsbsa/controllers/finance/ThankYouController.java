package com.nhsbsa.controllers.finance;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.service.RequestForTransferService;
import com.nhsbsa.view.OrganisationView;
import com.nhsbsa.view.RequestForTransferView;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * MVC controller for all requests to paths matching {@code /thankyou/.*}
 *
 * Created by duncan on 15/02/2017.
 */
@Slf4j
@Controller
@RequestMapping("/thankyou")
@PreAuthorize("hasRole('ADMIN') or hasRole('STANDARD')")
public class ThankYouController {

  public static final String SNAP_SURVEY_URL = "https://www.snapsurveys.com/wh/s.asp?k=151257549007";
  private RequestForTransferService requestForTransferService;
  private SecurityContextLogoutHandler securityContextLogoutHandler;

  private final DateFormat submittedDateFormat = new SimpleDateFormat("d MMMM yyyy 'at' HH:mm");

    @Autowired
    public ThankYouController(RequestForTransferService service, SecurityContextLogoutHandler handler) {
      this.requestForTransferService = service;
      this.securityContextLogoutHandler = handler;
    }

    /**
     * {@code GET} endpoint when the uuid of a {@code RequestForTransfer} entity
     * is provided.
     *
     * @param rftUuid The uuid of a {@code RequestForTransfer} entity
     * @param response A 404 status will be returned if there is no entity for the
     *                 uuid provided
     * @return Spring {@code ModelAndView}
     * @throws IOException if there is an IO issue during the {@code sendError} call
     * @throws IllegalArgumentException if the uuid is not a valid {@code String}
     * representation of a {@link java.util.UUID}
     */
    @GetMapping(value = "/{rftUuid}")
    public ModelAndView schedulePaymentWithRft(@PathVariable("rftUuid") String rftUuid,
        HttpServletResponse response) throws IOException {

      log.info("GET - Accessing to Thank You screen with uuid = {}", rftUuid);

      FinanceUser user = (FinanceUser) SecurityContextHolder.getContext().getAuthentication()
          .getDetails();

      Optional<RequestForTransferView> optionalRft = requestForTransferService
          .getOptionalRequestForTransferByRftUuid(rftUuid);

      if (optionalRft.isPresent()) {

        List<OrganisationView> org = user.getOrganisations();

        if (!org.isEmpty()) {
          ModelAndView modelAndView = new ModelAndView("thankyou");
          modelAndView.addObject("rft", optionalRft.get());
          modelAndView.addObject("orgName", org.get(0).getName());
          modelAndView.addObject("eaCode", optionalRft.get().getEaCode());
          modelAndView.addObject("fullName", user.getFirstName() + " " + user.getLastName());
          modelAndView.addObject("contactNumber", user.getContactTelephone());
          modelAndView.addObject("contactEmail", user.getContactEmail());
          //TODO Change getOrganisation().get(0) once we know how to handle multiple orgs
          modelAndView.addObject("multiEa", user.getOrganisations().get(0).getEas().size()>1);

          modelAndView.addObject("formattedSubmittedDate",
              optionalRft.map(RequestForTransferView::getSubmitDate).map(submittedDate -> {

                    try {
                      return this.formatSubmittedDate(submittedDate);
                    } catch (Exception e) {
                      log.error(String.format("Unable to format submitted date for rftId = %s", rftUuid));
                      return "";
                    }
                  }
              ).orElse(""));


          return modelAndView;
        }
        // If we end up with an RequestForTransfer without an organisation then something has gone wrong.
        log.error("Organisation is not present for request with uuid = {}", rftUuid);
        response.sendError(500);
        return null;
      }

      log.error("request for transfer with uuid = {} not found", rftUuid);
      response.sendError(404);
      return null;
    }

   String formatSubmittedDate(Date submitDate) {
    return submittedDateFormat.format(submitDate);
  }


  /*
  * Method to log user out when they click on Finish button before re-directing to snap shot survey
  * */
  @PostMapping(value="/finish")
  public String logOutAndRedirect (HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null){
      securityContextLogoutHandler.logout(request, response, auth);
    }
    return "redirect:"+SNAP_SURVEY_URL; // re-direct to snap survey link
  }

}
