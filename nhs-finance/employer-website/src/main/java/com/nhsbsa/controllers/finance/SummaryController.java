package com.nhsbsa.controllers.finance;

import com.nhsbsa.exceptions.AlreadySubmittedException;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.service.RequestForTransferService;
import com.nhsbsa.view.OrganisationView;
import com.nhsbsa.view.RequestForTransferView;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * MVC controller for all requests to paths matching {@code /summary/.*}
 *
 * Created by duncan on 15/02/2017.
 */
@Slf4j
@Controller
@RequestMapping("/summary")
@PreAuthorize("hasRole('ADMIN') or hasRole('STANDARD')")
public class SummaryController {

  private RequestForTransferService requestForTransferService;

  @Autowired
  public SummaryController(RequestForTransferService requestForTransferService) {
    this.requestForTransferService = requestForTransferService;
  }

  @GetMapping(value = "/{rftUuid}")
  public ModelAndView summaryWithRft(@PathVariable("rftUuid") String rftUuid,
      HttpServletResponse response) throws IOException, AlreadySubmittedException {

    log.info("GET - Accessing to Summary screen with rft uuid = {}", rftUuid);

    FinanceUser user = (FinanceUser) SecurityContextHolder.getContext().getAuthentication()
        .getDetails();

    Optional<RequestForTransferView> optionalRft = requestForTransferService
        .getOptionalRequestForTransferByRftUuid(rftUuid);
    if (optionalRft.isPresent()) {

      if(isRequestAlreadySubmitted(optionalRft.get())){
        throw new AlreadySubmittedException();
      }

      List<OrganisationView> org = user.getOrganisations();

      if (!org.isEmpty()) {
        ModelAndView modelAndView = new ModelAndView("summary");
        modelAndView.addObject("rft", optionalRft.get());
        modelAndView.addObject("orgName", org.get(0).getName());
        modelAndView.addObject("eaCode", optionalRft.get().getEaCode());
        modelAndView.addObject("fullName", user.getFirstName() + " " + user.getLastName());
        modelAndView.addObject("contactNumber", user.getContactTelephone());
        modelAndView.addObject("contactEmail", user.getContactEmail());

        return modelAndView;
      }
      //If we end up with an RequestForTransfer without an organisation then something has gone wrong.
      log.error("Organisation is not present for request with uuid = {}", rftUuid);
      response.sendError(500);
      return null;
    }

    log.error("request for transfer with uuid = {} not found", rftUuid);
    response.sendError(404);
    return null;
  }

  private boolean isRequestAlreadySubmitted(RequestForTransferView requestForTransfer) {
    return requestForTransfer.getSubmitDate() != null;
  }

  @PostMapping(value = "/{rftUuid}")
  public String submitContributionPayment(@PathVariable("rftUuid") final String rftUuid) {

    log.info("POST - Submitting request with uuid = {}", rftUuid);

    // All data ok, save what is in "requestForTransfer" into rft and then store in DB
    log.debug("About to submit 'Contributions and payment' in the Database");
    requestForTransferService.submitContributionPayment(rftUuid);
    log.debug("Submission saved ok in the Database");
    return String.format("redirect:/thankyou/%s", rftUuid);
  }
}
