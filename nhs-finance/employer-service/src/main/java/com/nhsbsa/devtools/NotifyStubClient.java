package com.nhsbsa.devtools;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import uk.nhs.nhsbsa.notify.api.v1.model.Notification;
import uk.nhs.nhsbsa.notify.api.v1.service.INotificationService;

/**
 * stub Notify client - logs the notifications messages instead of sending them
 * to Notify
 *
 */
@Slf4j
@Component
@Primary
@ConditionalOnProperty("stub-notify")
public class NotifyStubClient implements INotificationService {

	@Override
	public void send(Notification notification) {
		log.info("Not posting email to bsa-notify.  Mocking to log instead.");
		log.info("ID: {}", notification.getId());
		log.info("From: {}", notification.getFrom());
		log.info("To: {}", notification.getTo());
		if(notification.hasCcRecipients())log.info("CC: {}", notification.getCc());
		if(notification.hasBccRecipients())log.info("BCC: {}", notification.getBcc());
		log.info("Subject: {}", notification.getSubject());
		if(notification.getHtmlMessage()!=null)log.info("HTMLMessage: \n{}", notification.getHtmlMessage());
		if(notification.getPlainMessage()!=null)log.info("PlainMessage: \n{}", notification.getPlainMessage());
	}
}
