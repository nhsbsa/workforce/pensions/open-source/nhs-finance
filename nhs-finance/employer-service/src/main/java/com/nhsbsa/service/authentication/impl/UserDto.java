package com.nhsbsa.service.authentication.impl;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nhsbsa.view.OrganisationView;
import java.util.List;
import java.util.UUID;
import lombok.Builder;

/**
 * Model a user.
 *
 * Created by marklishman on 24/03/2017.
 */
@lombok.Value
@Builder
public final class UserDto {

  private final UUID identifier;
  private final String firstName;
  private final String lastName;
  private final String contactNumber;
  private final String emailAddress;
  private final List<OrganisationView> organisations;
  private final String role;

  @JsonCreator
  public static UserDto jsonFactory(@JsonProperty("identifier") UUID id,
      @JsonProperty("firstName") String firstName,
      @JsonProperty("lastName") String lastName,
      @JsonProperty("contactNumber") String contactNumber,
      @JsonProperty("emailAddress") String emailAddress,
      @JsonProperty("organisations") List<OrganisationView> organistions,
      @JsonProperty("role") String role) {
    return UserDto.builder()
        .identifier(id)
        .firstName(firstName)
        .lastName(lastName)
        .contactNumber(contactNumber)
        .emailAddress(emailAddress)
        .organisations(organistions)
        .role(role)
        .build();
  }
}
