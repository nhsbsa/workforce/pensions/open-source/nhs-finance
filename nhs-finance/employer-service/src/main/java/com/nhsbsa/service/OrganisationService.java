package com.nhsbsa.service;

import com.nhsbsa.security.CreateEaRequest;
import com.nhsbsa.security.CreateOrganisationResponse;
import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import com.nhsbsa.security.CreateOrganisationRequest;
import com.nhsbsa.service.authentication.impl.OrganisationDto;
import com.nhsbsa.view.OrganisationView;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service(value = "organisationService")
public class OrganisationService {

  @SuppressWarnings("squid:S1075")
  // https://next.sonarqube.com/sonarqube/coding_rules#rule_key=squid%3AS3437
  private static final String GET_REQUEST_FOR_ORGANISATION_USING_EACODE =
      "/organisation/ea-code/{eaCode}";

  private static final String POST_REQUEST_FOR_REGISTERING_ORGANISATION = "/organisation";

  private static final String ORGANISATION_ID_ENDPOINT = "/organisation/{organisation-id}";

  private static final String GET_REQUEST_FOR_EMPTYPE_USING_EACODE = "/employer-type/{eaCode}";

  private static final String POST_REGISTER_ORGANISATION_WITHOUT_EA = "/organisation-no-ea";

  private static final String POST_REGISTER_EA = "/employing-authority";

  private static final String ORGANISATION_NAME_ENDPOINT = "/organisation-name";

  private static final String EA_CODE_ENDPOINT = "/employing-authority/{eaCode}";

  private RestTemplate ficRestTemplate;

  private final UriComponentsBuilder baseUriComponentsBuilder;
  
  @Autowired
  public OrganisationService(final RestTemplate ficRestTemplate,
      @Value("${api.employer-details-service.url}") final String employerDetailsUrl) {

    this.ficRestTemplate = ficRestTemplate;
    baseUriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(employerDetailsUrl);
  }

  public OrganisationView getOrganisationByEaCode(String eaCode) {
    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final URI orgUri = uriComponentsBuilder.path(GET_REQUEST_FOR_ORGANISATION_USING_EACODE)
        .buildAndExpand(eaCode).toUri();

    return ficRestTemplate.getForObject(orgUri, OrganisationView.class);
    
  }
  
  public OrganisationDto getOrganisationByID(String id) {

    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final URI organisationUri =
        uriComponentsBuilder.path(ORGANISATION_ID_ENDPOINT).buildAndExpand(id).toUri();

    return ficRestTemplate.getForObject(organisationUri, OrganisationDto.class);
  }


  public OrganisationView registerOrganisation(CreateOrganisationRequest createOrganisationRequest) {
    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final String orgUri = uriComponentsBuilder.path(POST_REQUEST_FOR_REGISTERING_ORGANISATION)
        .toUriString();

    return ficRestTemplate.postForObject(orgUri, createOrganisationRequest, OrganisationView.class);
  }

  public Optional<String> getEmployerTypeByEaCode(String eaCode) {
    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final URI eaUri = uriComponentsBuilder.path(GET_REQUEST_FOR_EMPTYPE_USING_EACODE)
        .buildAndExpand(eaCode).toUri();

    return Optional.ofNullable(ficRestTemplate.getForObject(eaUri, String.class));

  }

  public Optional<CreateOrganisationResponse> registerOrganisationWithoutEa(CreateOrganisationRequest createOrganisationRequest) {
    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final String orgUri = uriComponentsBuilder.path(POST_REGISTER_ORGANISATION_WITHOUT_EA)
        .toUriString();

    return Optional.ofNullable(ficRestTemplate.postForObject(orgUri, createOrganisationRequest, CreateOrganisationResponse.class));
  }

  public Optional<OrganisationView> registerEa(CreateEaRequest createEaRequest) {
    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final String eaUri = uriComponentsBuilder.path(POST_REGISTER_EA)
        .toUriString();

    return Optional.ofNullable(ficRestTemplate.postForObject(eaUri, createEaRequest, OrganisationView.class));
  }

  public Optional<String> getOrganisationByName(CreateOrganisationRequest organisationRequest) {
    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final String orgUri = uriComponentsBuilder.path(ORGANISATION_NAME_ENDPOINT).toUriString();

    return Optional.ofNullable(ficRestTemplate.postForObject(orgUri, organisationRequest, String.class));
  }

  public Optional<String> getEaByCode(String eaCode) {
    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final URI eaUri = uriComponentsBuilder.path(EA_CODE_ENDPOINT)
        .buildAndExpand(eaCode).toUri();

    return Optional.ofNullable(ficRestTemplate.getForObject(eaUri, String.class));
  }

}