package com.nhsbsa.service.authentication.impl;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nhsbsa.view.EmployingAuthorityView;
import java.util.List;
import java.util.UUID;
import lombok.Builder;

/**
 * Model an employer authority.
 *
 * Created by marklishman on 09/03/2017.
 */
@lombok.Value
@Builder
public final class OrganisationDto {

  private final UUID identifier;
  private final List<EmployingAuthorityView> eas;
  private final String name;
  private final String accountName;
  private final String accountNumber;

  @JsonCreator
  public static OrganisationDto jsonFactory(@JsonProperty("identifier") UUID id,
      @JsonProperty("eas") List<EmployingAuthorityView> eas,
      @JsonProperty("name") String name,
      @JsonProperty("accountName") String accountName,
      @JsonProperty("accountNumber") String accountNumber) {
    return OrganisationDto.builder()
        .identifier(id)
        .eas(eas)
        .name(name)
        .accountName(accountName)
        .accountNumber(accountNumber)
        .build();
  }
}
