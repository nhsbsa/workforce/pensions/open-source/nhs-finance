package com.nhsbsa.service.email.impl;

import java.util.List;
import java.util.Map;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import com.nhsbsa.service.email.EmailConfig;
import com.nhsbsa.service.email.EmailService;

import lombok.extern.slf4j.Slf4j;
import uk.nhs.nhsbsa.notify.api.v2.model.EmailNotificationRequest;
import uk.nhs.nhsbsa.notify.api.v2.model.NotificationResponse;
import uk.nhs.nhsbsa.notify.api.v2.service.INotificationService;

@Slf4j
@Profile("email-provider-GDSMail")
@Service(value = "bsaNotifyV2Service") //V2 uses Gov.UK Notify
public class BSANotifyV2Service implements EmailService{

  private INotificationService notificationClient;

  private final String serviceName;
  
  @Autowired
  public BSANotifyV2Service(INotificationService notificationClient, final @Value("${bsa.notify.service.name}") String serviceName) {
    this.notificationClient = notificationClient;
    this.serviceName = serviceName;
  }

  private EmailNotificationRequest createNotification(String toEmail,
      String emailSubject, Map<String, String> personalisation, String templateName) {
    
    personalisation.put("emailSubject", emailSubject);
    
    EmailNotificationRequest notification = new EmailNotificationRequest();
    notification.setId(RandomStringUtils.randomAlphabetic(15));
    notification.setEmailAddress(toEmail);
    notification.setTemplate(templateName);
    notification.setService(serviceName);
    notification.setPersonalisation(personalisation);

    return notification;
  }

  @Override
  public void sendEmail(List<String> toEmails, String emailSubject, EmailConfig emailConfig,
      Map<String, String> personalisation) {

    for (String email : toEmails) {
      EmailNotificationRequest notification = createNotification(
          email, 
          emailSubject, 
          personalisation, 
          emailConfig.getTemplateName());

      log.info("ID [{}]: Sending '{}' email to BSA Notify for: {}", notification.getId(),
          emailSubject, email);

      NotificationResponse response = notificationClient.send(notification);
      
      if(!response.isSuccess()) {
        log.error("Error sending email. Error code: {} Error message {}", response.getErrorCode(),  response.getErrorDescription());
      }
    }
  }

}
