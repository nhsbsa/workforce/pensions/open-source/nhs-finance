package com.nhsbsa.config;

import com.nhsbsa.interceptors.LoggingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@Import(LoggingInterceptor.class)
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private LoggingInterceptor loggingInterceptor;

    @Value("${rest.template.connect.timeout:0}")
    private Integer connectTimeout;

    @Value("${rest.template.read.timeout:0}")
    private Integer readTimeout;

    @Bean("ficRestTemplate")
    @Profile("!user-propagation")
    public RestTemplate ficRestTemplate(RestTemplateBuilder builder) {
        return builder
            .setConnectTimeout(connectTimeout)
            .setReadTimeout(readTimeout).build();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(loggingInterceptor).addPathPatterns("/**");
    }
}
