package com.nhsbsa.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.nhsbsa.service.email.EmailConfig;

@Configuration
public class EmailContextConfig  {
  
  @Value("${bsa.contributions.reminder.email.from}") 
  String reminderFromEmail;
  
  @Value("${bsa.resetpassword.email.from}") 
  String resetPasswordFromEmail;

  @Value("${bsa.createStandardUser.email.from}")
  String standardUserFromEmail;
  
  @Bean
  @ConditionalOnProperty("activateEmailReminderScheduler")
  public EmailConfig reminderEmailConfig() {    
    return new EmailConfig(reminderFromEmail, "email-fic-reminder");
  }
  
  @Bean
  public EmailConfig resetEmailConfig() {
    return new EmailConfig(resetPasswordFromEmail,"email-fic-reset-pw");
  }

  @Bean
  public EmailConfig standardUserCreatedConfig() {
    return new EmailConfig(standardUserFromEmail,"email-fic-new-standard-user");
  }
  
}
