package com.nhsbsa.config;

import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import com.nhsbsa.exception.TooManyExcludedDatesException;

@Component
@PropertySource("classpath:dates.properties")
public class ConfigurationGuard implements InitializingBean {

  private List<String> excludedDates;
  
  @SuppressWarnings("squid:S00116")
  public final int MAX_EXCLUDED_DATES;

public ConfigurationGuard(
    @Value("#{'${paymentdate.excluded.dates}'.split(',')}")List<String> excludedDates,
    @Value("${maximum.excluded.dates:30}") int maxDates) {
    this.excludedDates = excludedDates;
    this.MAX_EXCLUDED_DATES = maxDates;
  }

public void afterPropertiesSet() {
    if (excludedDates != null && excludedDates.size() > MAX_EXCLUDED_DATES) {
          throw new TooManyExcludedDatesException(MAX_EXCLUDED_DATES, excludedDates.size());
    }
 }
}