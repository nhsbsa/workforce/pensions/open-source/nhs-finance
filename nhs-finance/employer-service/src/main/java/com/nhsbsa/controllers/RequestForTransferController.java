package com.nhsbsa.controllers;

import com.nhsbsa.service.FinanceService;
import com.nhsbsa.view.RequestForTransferView;
import io.swagger.annotations.ApiOperation;
import java.time.LocalDate;
import java.util.Optional;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by marklishman on 5/11//2016.
 */

@RequestMapping("/finance")
@RestController
@Slf4j
public class RequestForTransferController {

  private final FinanceService financeService;

  @Autowired
  public RequestForTransferController(final FinanceService financeService) {
    this.financeService = financeService;
  }

  @GetMapping(value = "/requestfortransfer/{rftUuid}")
  @ApiOperation(value = "default", notes = "default")
  public ResponseEntity<RequestForTransferView> getRequestForTransferByRftUuid(
      @PathVariable("rftUuid") final String rftUuid) {
    log.info("GET - requestfortransfer service with rftId = {}", rftUuid);
    RequestForTransferView rft = financeService.getRequestForTransferByRftUuid(rftUuid);
    return ResponseEntity.ok(rft);
  }

  @PostMapping(value = "/requestfortransfer")
  @ApiOperation(value = "default", notes = "default")
  public ResponseEntity<RequestForTransferView> saveRequestForTransfer(
      @RequestBody @Valid final RequestForTransferView requestForTransfer) {

    log.info("POST - requestfortransfer service with rftId = {}", requestForTransfer.getRftUuid());
    // only set rftUuid if it hasnt already been set as this method is used to Create And Update
    if (requestForTransfer.getRftUuid() == null) {
      requestForTransfer.setRftUuid(java.util.UUID.randomUUID().toString());
    }

    RequestForTransferView rft = financeService.saveRequestForTransfer(requestForTransfer);
    return ResponseEntity.ok(rft);
  }

  @GetMapping(value = "/file")
  @ApiOperation(value = "default", notes = "default")
  public ResponseEntity<String[]> getFileList() {

    log.info("GET file list");
    return ResponseEntity.ok(financeService.getFileList());
  }

  @GetMapping(value = "/file/{fileName}")
  public ResponseEntity<byte[]> getFile(@PathVariable("fileName") final String fileName) {

    log.info("GET file {}", fileName);
    return ResponseEntity.ok(financeService.getFile(fileName));
  }

  @GetMapping(value = "/submitdate/{eaCode}")
  @ApiOperation(value = "default", notes = "default")
  public ResponseEntity<LocalDate> getFormattedSubmitDateByEaCode(
      @PathVariable("eaCode") final String eaCode) {
    log.info("GET - requestfortransfer Submit Date with eaCode = {}", eaCode);

    Optional<LocalDate> maybeDate  = financeService.getSubmitDateByEaCode(eaCode);

    return maybeDate.map(u -> ResponseEntity.ok(u)).orElse( ResponseEntity.noContent().build());
  }
}