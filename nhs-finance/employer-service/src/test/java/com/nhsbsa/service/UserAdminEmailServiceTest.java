package com.nhsbsa.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.nhsbsa.security.EmailRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.nhsbsa.service.email.EmailConfig;
import com.nhsbsa.service.email.EmailService;
import com.nhsbsa.service.email.UserAdminEmailService;

@RunWith(MockitoJUnitRunner.class)
public class UserAdminEmailServiceTest {

  private UserAdminEmailService service;

  @Mock
  private EmailService emailService;
  
  EmailConfig resetEmailConfig;

  EmailConfig standardUserCreatedConfig;
  
  private final String TOKEN = "1234uuid";

  private static final String FROM_EMAIL = "test.test@test.com";
  private static final String RESET_EMAIL_TEMPLATE = "test.email";
  private static final String RESET_EMAIL_SUBJECT = "Make payments to the NHS Pension Scheme - Reset Password";
  private static final String WEB_ADDRESS = "http://localhost:8080/employer-website";
  private static final String URL_ADDRESS = "http://localhost:8080/employer-website/set-password";
  private static final String STANDARD_USER_EMAIL_SUBJECT = "Make payments to the NHS Pension Scheme";
  private static final String STANDARD_USER_EMAIL_URL = "http://localhost:8080/employer-website/start/";
  private static final String STANDARD_USER_EMAIL_TEMPLATE = "test.standard.email";


  @Before
  public void setup() throws Exception{
    
   resetEmailConfig = new EmailConfig(FROM_EMAIL, RESET_EMAIL_TEMPLATE);

   standardUserCreatedConfig = new EmailConfig(FROM_EMAIL, STANDARD_USER_EMAIL_TEMPLATE);
    
    service = new UserAdminEmailService(
            resetEmailConfig, standardUserCreatedConfig,
            emailService);

    FieldUtils.writeDeclaredField(service, "appBaseAddress", WEB_ADDRESS, true);
    //Reset Password email fields
    FieldUtils.writeDeclaredField(service, "resetEmailSubject", RESET_EMAIL_SUBJECT, true);
    FieldUtils.writeDeclaredField(service, "setPasswordUrl", URL_ADDRESS, true);
    //Standard User email fields
    FieldUtils.writeDeclaredField(service, "standardEmailSubject", STANDARD_USER_EMAIL_SUBJECT, true);
    FieldUtils.writeDeclaredField(service, "standardLinkUrl", STANDARD_USER_EMAIL_URL, true);
  }

  @Test
  public void resetPasswordEmailIsSent() throws Exception
  {
    String userEmail = "test.test@test.com";

    service.sendPasswordResetEmail(userEmail, TOKEN);
    
    Map<String, String> personalisation = new HashMap<>();
    personalisation.put("setPasswordUrl", URL_ADDRESS + TOKEN);
    personalisation.put("appBaseAddress", WEB_ADDRESS);

    verify(emailService, times(1)).sendEmail(Arrays.asList(userEmail), RESET_EMAIL_SUBJECT, resetEmailConfig, personalisation);

  }

  @Test
  public void standardUserCreatedEmailIsSent() throws Exception
  {
    EmailRequest request = EmailRequest.builder().firstName("test").lastName("test").email("test.test@test.com").createdBy("Sam Jones").build();

    service.sendStandardUserCreatedEmail(request);

    Map<String, String> personalisation = new HashMap<>();
    personalisation.put("standardLinkUrl", STANDARD_USER_EMAIL_URL);
    personalisation.put("appBaseAddress", WEB_ADDRESS);
    personalisation.put("userName", request.getEmail());
    personalisation.put("createdBy", request.getCreatedBy());

    verify(emailService, times(1)).sendEmail(Arrays.asList(request.getEmail()), STANDARD_USER_EMAIL_SUBJECT, standardUserCreatedConfig, personalisation);

  }

}