package com.nhsbsa.controllers;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nhsbsa.model.EmployerTypes;
import com.nhsbsa.security.CreateEaRequest;
import com.nhsbsa.security.CreateOrganisationRequest;
import com.nhsbsa.security.CreateOrganisationResponse;
import com.nhsbsa.service.OrganisationService;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.text.CollationElementIterator;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Created by wdotto on 31/10/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class OrganisationControllerTest {

  private MockMvc mockMvc;

  private OrganisationView organisation;

  @Autowired
  private ObjectMapper objectMapper;

  private final UUID ORGANISATION_UUID = UUID.randomUUID();
  private final static String ORGANISATION_NAME = "Company Account Name";
  private final static String EA_CODE = "EA1234";
  private final static String EA_NAME = "EA Name";
  private final static String EMPLOYER_TYPE = "STAFF";
  private final static EmployingAuthorityView EA_VIEW = EmployingAuthorityView.builder().eaCode("123456").name("").employerType("").build();
  private final static List<EmployingAuthorityView> EA_LIST = Collections.singletonList(EA_VIEW);

  public static final String INVALID_CONTENT = "{" +
      "\"organisationName\" : \"" + ORGANISATION_NAME + "\"" +
      "}";

  public static final String INVALID_EA_CONTENT = "{"+
      "\"eaName\" : \"" + EA_NAME + "\"" +
      "}";

  @Mock
  private OrganisationService organisationService;

  @Before
  public void setUp() throws Exception {

    organisation = OrganisationView.builder().identifier(ORGANISATION_UUID).name(ORGANISATION_NAME).eas(EA_LIST).build();

    mockMvc = MockMvcBuilders
        .standaloneSetup(new OrganisationController(organisationService))
        .build();
  }

  @Test
  public void getOrganisationByEaCode_verify_body_response() throws Exception {

    when(organisationService.getOrganisationByEaCode(Mockito.anyString()))
        .thenReturn(organisation);

    mockMvc.perform(get("/organisation/{eaCode}", 1L))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(successResponse()))));

    verify(organisationService, times(1)).getOrganisationByEaCode(Mockito.any(String.class));
  }

  @Test
  public void getEmployerTypeByEaCode_verify_body_response() throws Exception {

    Optional<String> empType = Optional.of(EmployerTypes.STAFF.name());
    String content = EmployerTypes.STAFF.name();

    when(organisationService.getEmployerTypeByEaCode(Mockito.anyString()))
        .thenReturn(empType);

    mockMvc.perform(get("/employer-type/{eaCode}", 1L))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(content))));

    verify(organisationService, times(1)).getEmployerTypeByEaCode(Mockito.any(String.class));
  }

  @Test
  public void getEmployerTypeByEaCode_returns_no_content() throws Exception {

    Optional<String> empType = Optional.of(EmployerTypes.STAFF.name());
    String content = EmployerTypes.STAFF.name();

    when(organisationService.getEmployerTypeByEaCode(Mockito.anyString()))
        .thenReturn(Optional.empty());

    mockMvc.perform(get("/employer-type/{eaCode}", 1L))
        .andExpect(status().isNoContent())
        .andExpect(content().string(is(equalTo(""))));

    verify(organisationService, times(1)).getEmployerTypeByEaCode(Mockito.any(String.class));
  }

  @Test
  public void register_organisation_returns_ok_response() throws Exception {

    CreateOrganisationRequest request = CreateOrganisationRequest.builder()
        .name(ORGANISATION_NAME)
        .eas(EA_LIST)
        .build();
    given(organisationService.registerOrganisation(request)).willReturn(organisation);

    mockMvc.perform(MockMvcRequestBuilders.post("/organisation")
        .contentType(MediaType.APPLICATION_JSON)
        .content(validOrgContent()))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(successResponse()))));

    verify(organisationService, times(1)).registerOrganisation(Mockito.any(CreateOrganisationRequest.class));
  }

  @Test
  public void organisationIsNotRegisteredIfRequestDataIsInvalid() throws Exception {

    mockMvc.perform(MockMvcRequestBuilders.post("/organisation")
        .contentType(MediaType.APPLICATION_JSON)
        .content(INVALID_CONTENT))
        .andExpect(status().isBadRequest())
        .andExpect(content().string(isEmptyString()));

    verify(organisationService, times(0)).registerOrganisation(Mockito.any(CreateOrganisationRequest.class));
  }

  private String successResponse() throws JsonProcessingException {
    return objectMapper.writeValueAsString(OrganisationView
        .builder()
        .identifier(ORGANISATION_UUID)
        .name(ORGANISATION_NAME)
        .eas(EA_LIST)
        .build());
  }

  private String validOrgContent() throws JsonProcessingException {

    EmployingAuthorityView eaView = EmployingAuthorityView.builder().eaCode("123456").name("").employerType("").build();
    List<EmployingAuthorityView> eaList = Collections.singletonList(eaView);

    return objectMapper.writeValueAsString(CreateOrganisationRequest
        .builder()
        .name(ORGANISATION_NAME)
        .eas(eaList)
        .build());
  }

  @Test
  public void register_organisation_without_ea_returns_ok_response() throws Exception {

    CreateOrganisationRequest request = CreateOrganisationRequest.builder()
        .name(ORGANISATION_NAME)
        .eas(Collections.emptyList())
        .build();
    CreateOrganisationResponse response = CreateOrganisationResponse.builder()
        .name(ORGANISATION_NAME)
        .uuid(ORGANISATION_UUID.toString())
        .build();
    given(organisationService.registerOrganisationWithoutEa(request)).willReturn(Optional.of(response));

    mockMvc.perform(MockMvcRequestBuilders.post("/organisation-no-ea")
        .contentType(MediaType.APPLICATION_JSON)
        .content(validOrgWithoutEaContent()))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(OrgWithoutEaSuccessResponse()))));

    verify(organisationService, times(1)).registerOrganisationWithoutEa(Mockito.any(CreateOrganisationRequest.class));
  }

  private String OrgWithoutEaSuccessResponse() throws JsonProcessingException {
    return objectMapper.writeValueAsString(CreateOrganisationResponse
        .builder()
        .uuid(ORGANISATION_UUID.toString())
        .name(ORGANISATION_NAME)
        .build());
  }

  private String validOrgWithoutEaContent() throws JsonProcessingException {

    return objectMapper.writeValueAsString(CreateOrganisationRequest
        .builder()
        .name(ORGANISATION_NAME)
        .eas(Collections.emptyList())
        .build());
  }

  @Test
  public void organisationWithoutEaIsNotRegisteredIfRequestDataIsInvalid() throws Exception {

    mockMvc.perform(MockMvcRequestBuilders.post("/organisation-no-ea")
        .contentType(MediaType.APPLICATION_JSON)
        .content(INVALID_CONTENT))
        .andExpect(status().isBadRequest())
        .andExpect(content().string(isEmptyString()));

    verify(organisationService, times(0)).registerOrganisationWithoutEa(Mockito.any(CreateOrganisationRequest.class));
  }

  @Test
  public void register_ea_returns_ok_response() throws Exception {

    CreateEaRequest request = CreateEaRequest.builder()
        .eaCode(EA_CODE)
        .eaName(EA_NAME)
        .employerType(EMPLOYER_TYPE)
        .organisationUuid(ORGANISATION_UUID.toString())
        .build();
    EmployingAuthorityView eaView = EmployingAuthorityView.builder()
        .eaCode(EA_CODE)
        .name(EA_NAME)
        .employerType(EMPLOYER_TYPE)
        .build();
    OrganisationView response = OrganisationView.builder()
        .name(ORGANISATION_NAME)
        .identifier(ORGANISATION_UUID)
        .eas(Collections.singletonList(eaView))
        .build();

    given(organisationService.registerEa(request)).willReturn(Optional.of(response));

    mockMvc.perform(MockMvcRequestBuilders.post("/employing-authority")
        .contentType(MediaType.APPLICATION_JSON)
        .content(validEaContent()))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(OrgWithEaSuccessResponse()))));

    verify(organisationService, times(1)).registerEa(Mockito.any(CreateEaRequest.class));
  }

  @Test
  public void eaIsNotRegisteredIfRequestDataIsInvalid() throws Exception {

    mockMvc.perform(MockMvcRequestBuilders.post("/employing-authority")
        .contentType(MediaType.APPLICATION_JSON)
        .content(INVALID_EA_CONTENT))
        .andExpect(status().isBadRequest())
        .andExpect(content().string(isEmptyString()));

    verify(organisationService, times(0)).registerEa(Mockito.any(CreateEaRequest.class));
  }

  private String validEaContent() throws JsonProcessingException {

    return objectMapper.writeValueAsString(CreateEaRequest
        .builder()
        .eaName(EA_NAME)
        .eaCode(EA_CODE)
        .employerType(EMPLOYER_TYPE)
        .organisationUuid(ORGANISATION_UUID.toString())
        .build());
  }

  private String OrgWithEaSuccessResponse() throws JsonProcessingException {
    EmployingAuthorityView eaView = EmployingAuthorityView.builder()
        .eaCode(EA_CODE)
        .name(EA_NAME)
        .employerType(EMPLOYER_TYPE)
        .build();
    return objectMapper.writeValueAsString(OrganisationView
        .builder()
        .identifier(ORGANISATION_UUID)
        .name(ORGANISATION_NAME)
        .eas(Collections.singletonList(eaView))
        .build());
  }

  @Test
  public void get_organisation_by_name_returns_ok_response() throws Exception {

    when(organisationService.getOrganisationByName(Mockito.any(CreateOrganisationRequest.class)))
        .thenReturn(Optional.of(ORGANISATION_NAME));

    mockMvc.perform(MockMvcRequestBuilders.post("/organisation-name")
        .contentType(MediaType.APPLICATION_JSON)
        .content(validOrgRequestContent()))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo("Company Account Name"))));

    verify(organisationService, times(1)).getOrganisationByName(Mockito.any(CreateOrganisationRequest.class));
  }

  @Test
  public void get_organisation_by_name_returns_empty_response() throws Exception {

    when(organisationService.getOrganisationByName(Mockito.any(CreateOrganisationRequest.class)))
        .thenReturn(Optional.empty());

    mockMvc.perform(MockMvcRequestBuilders.post("/organisation-name")
        .contentType(MediaType.APPLICATION_JSON)
        .content(validOrgRequestContent()))
        .andExpect(status().isNoContent())
        .andExpect(content().string(isEmptyString()));

    verify(organisationService, times(1)).getOrganisationByName(Mockito.any(CreateOrganisationRequest.class));
  }

  private String validOrgRequestContent() throws JsonProcessingException {

    return objectMapper.writeValueAsString(CreateOrganisationRequest
        .builder()
        .name(ORGANISATION_NAME)
        .eas(Collections.emptyList())
        .build());
  }

  @Test
  public void getEaByEaCode_verify_body_response() throws Exception {

    String eaCode = "EA124";
    when(organisationService.getEaByCode(Mockito.anyString()))
        .thenReturn(Optional.of(eaCode));

    mockMvc.perform(get("/employing-authority/{eaCode}", 1L))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(eaCode))));

    verify(organisationService, times(1)).getEaByCode(Mockito.any(String.class));
  }

  @Test
  public void getEaByEaCode_returns_empty_response() throws Exception {

    when(organisationService.getEaByCode(Mockito.anyString()))
        .thenReturn(Optional.empty());

    mockMvc.perform(get("/employing-authority/{eaCode}", 1L))
        .andExpect(status().isNoContent())
        .andExpect(content().string(isEmptyString()));

    verify(organisationService, times(1)).getEaByCode(Mockito.any(String.class));
  }


}
