package com.nhsbsa.model.validation;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.nhsbsa.view.RequestForTransferView;
import java.math.BigDecimal;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder.NodeBuilderCustomizableContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by Mark Lishman on 18/11/2016.
 */
public class EmployeeContributionThresholdValidatorTest {

  private static final ConstraintValidatorContext NOT_REQUIRED = null;

  private EmployeeContributionThresholdValidator validator;

  @Before
  public void before() {
    validator = new EmployeeContributionThresholdValidator();
  }

  @Test
  public void isValidReturnsTrueWhenTotalPensionablePayIsNull() {
    RequestForTransferView rft = RequestForTransferView
        .builder()
        .employeeContributions(new BigDecimal("123.45"))
        .build();

    boolean actual = validator.isValid(rft, NOT_REQUIRED);

    assertThat(actual, is(equalTo(true)));
  }

  @Test
  public void isValidReturnsTrueWhenEmployeeContributionsIsNull() {
    RequestForTransferView rft = RequestForTransferView
        .builder()
        .totalPensionablePay(new BigDecimal("123.45"))
        .build();

    boolean actual = validator.isValid(rft, NOT_REQUIRED);

    assertThat(actual, is(equalTo(true)));
  }

  @Test
  public void isValidReturnsTrueWhenEmployeeContributionsIsAboveThreshold() {
    RequestForTransferView rft = RequestForTransferView
        .builder()
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("5"))
        .build();

    boolean actual = validator.isValid(rft, NOT_REQUIRED);

    assertThat(actual, is(equalTo(true)));
  }

  @Test
  public void isValidReturnsTrueWhenEmployeeContributionsIsBelowThreshold() {
    RequestForTransferView rft = RequestForTransferView
        .builder()
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("14.5"))
        .build();

    boolean actual = validator.isValid(rft, NOT_REQUIRED);

    assertThat(actual, is(equalTo(true)));
  }

  @Test
  public void isValidReturnsFalseWhenEmployeeContributionsIsBelowThreshold() {
    RequestForTransferView rft = RequestForTransferView
        .builder()
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("4.99"))
        .build();

    ConstraintValidatorContext context = Mockito.mock(ConstraintValidatorContext.class);
    ConstraintViolationBuilder b = Mockito.mock(ConstraintViolationBuilder.class);
    NodeBuilderCustomizableContext nContext = Mockito.mock(NodeBuilderCustomizableContext.class);
    when(context.buildConstraintViolationWithTemplate(anyString())).thenReturn(b);
    when(b.addPropertyNode(anyString())).thenReturn(nContext);

    boolean actual = validator.isValid(rft, context);

    assertThat(actual, is(equalTo(false)));
    Mockito.verify(context, times(1)).disableDefaultConstraintViolation();
    Mockito.verify(nContext, times(1)).addConstraintViolation();
  }

  @Test
  public void isValidReturnsFalseWhenEmployeeContributionsIsAboveThreshold() {
    RequestForTransferView rft = RequestForTransferView
        .builder()
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("14.51"))
        .build();

    ConstraintValidatorContext context = Mockito.mock(ConstraintValidatorContext.class);
    ConstraintViolationBuilder b = Mockito.mock(ConstraintViolationBuilder.class);
    NodeBuilderCustomizableContext nContext = Mockito.mock(NodeBuilderCustomizableContext.class);
    when(context.buildConstraintViolationWithTemplate(anyString())).thenReturn(b);
    when(b.addPropertyNode(anyString())).thenReturn(nContext);

    boolean actual = validator.isValid(rft, context);

    assertThat(actual, is(equalTo(false)));
    Mockito.verify(context, times(1)).disableDefaultConstraintViolation();
    Mockito.verify(nContext, times(1)).addConstraintViolation();
  }

}