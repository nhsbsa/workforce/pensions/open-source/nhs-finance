package com.nhsbsa.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Created by Mark Lishman on 30/03/2017.
 */
public class FinanceUserTest {

  private FinanceUser financeUser;

  @Before
  public void before() {
    financeUser = new FinanceUser();
  }

  @Test
  public void given_finance_user_is_created_when_password_is_requested_then_it_is_blank() {
    assertThat(financeUser.getPassword(), is(equalTo("")));
  }

  @Test
  public void given_finance_user_is_created_when_account_non_expired_is_requested_then_it_is_true() {
    assertThat(financeUser.isAccountNonExpired(), is(equalTo(true)));
  }

  @Test
  public void given_finance_user_is_created_when_account_non_locked_is_requested_then_it_is_false() {
    assertThat(financeUser.isAccountNonLocked(), is(equalTo(true)));
  }

  @Test
  public void given_finance_user_is_created_when_account_credentials_non_locked_is_requested_then_it_is_true() {
    assertThat(financeUser.isCredentialsNonExpired(), is(equalTo(true)));
  }

}