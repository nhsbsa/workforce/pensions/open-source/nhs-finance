package com.nhsbsa.model.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.nhsbsa.model.TransferFormDate;
import com.nhsbsa.view.RequestForTransferView;
import java.util.Arrays;

import javax.validation.ConstraintValidatorContext;

import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder.NodeBuilderCustomizableContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
public class IsNotExcludedDateValidatorTest {

	private IsNotExcludedDateValidator validator;
	private ConstraintValidatorContext context;

	@Before
	public void setUp() {
		validator = new IsNotExcludedDateValidator();
		ReflectionTestUtils.setField(validator, "excludedDates", Arrays.asList("06/06","25/12"));

		context = Mockito.mock(ConstraintValidatorContext.class);

		final ConstraintValidatorContext.ConstraintViolationBuilder constraintViolationBuilder =
				Mockito.mock(ConstraintValidatorContext.ConstraintViolationBuilder.class);

		when(context.buildConstraintViolationWithTemplate(Mockito.anyString()))
				.thenReturn(constraintViolationBuilder);

		NodeBuilderCustomizableContext customCtx = mock(NodeBuilderCustomizableContext.class);

		doReturn(customCtx).when(constraintViolationBuilder).addPropertyNode(any());
		doReturn(context).when(customCtx).addConstraintViolation();
	}

	@Test
	public void given_a_date_that_is_not_excluded_when_isValid_called_then_return_true() throws Exception {
		RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
				TransferFormDate.builder().days("3").month("March").year("2003").build()).build();
		final Boolean result = validator.isValid(rft, context);
		assertTrue(result);
	}

	@Test
	public void given_a_date_that_is_excluded_when_isValid_called_then_return_false() throws Exception {
		RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
				TransferFormDate.builder().days("6").month("June").year("2018").build()).build();
		final Boolean result = validator.isValid(rft, context);
		assertFalse(result);
	}

	@Test
	public void given_a_null_date_when_isValid_called_then_return_true() {
		RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
				TransferFormDate.builder().days("").month("").year("").build()).build();
		final Boolean result = validator.isValid(rft, context);
		assertTrue(result);
	}

	@Test
	public void given_invalid_date_format_when_isValid_called_then_return_true() {
		RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
				TransferFormDate.builder().days("15").month("May").year("18").build()).build();
		final Boolean result = validator.isValid(rft, context);
		assertTrue(result);
	}


}
