package com.nhsbsa.model;

import com.nhsbsa.model.validation.DateLessThan31DaysFromTodayValidator;
import java.time.LocalDate;

public class DateLessThan31DaysFromTodayTestableValidator extends
    DateLessThan31DaysFromTodayValidator {

  public final static LocalDate DATE = LocalDate.of(2018, 3, 7);

  @Override
  public LocalDate getDate() {
    return DATE;
  }
}
