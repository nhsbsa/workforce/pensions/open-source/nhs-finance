package com.nhsbsa.model;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Created by nataliehulse on 10/11/2016.
 */
@RunWith(Parameterized.class)
public class MonthNumTest {

  private final MonthNum months = new MonthNum();
  private Integer expectedResult;
  private String inputWelsh;
  private String input;


  public MonthNumTest(Integer expectedResult, String input, String inputWelsh) {
    this.expectedResult = expectedResult;
    this.input = input;
    this.inputWelsh = inputWelsh;
  }

  @Parameterized.Parameters
  public static Collection monthNumber() {
    return Arrays.asList(new Object[][]{
        {1, "January", "Ionawr"},
        {2, "February", "Chwefror"},
        {3, "March", "Mawrth"},
        {4, "April", "Ebrill"},
        {5, "May", "Mai"},
        {6, "June", "Mehefin"},
        {7, "July", "Gorffennaf"},
        {8, "August", "AWST"},
        {9, "September", "MEDI"},
        {10, "October", "HYDREF"},
        {11, "November", "TACHWEDD"},
        {12, "December", "RHAGFYR"}
    });
  }


  @Test
  public void getMonthFromNumberTest() {
    assertThat(expectedResult, is(months.getMonthNumFromName(input)));
    assertThat(expectedResult, is(months.getMonthNumFromName(inputWelsh)));
  }

  @Test
  public void incorrectEnglishMonthShouldReturn0() {

    int monthNum = months.getMonthNumFromName("jan");
    assertEquals("Should be equal", 0, monthNum);
  }

  @Test
  public void incorrectWelshMonthShouldReturn0() {

    int monthNum = months.getMonthNumFromName("Rhagfy");
    assertEquals("Should be equal", 0, monthNum);
  }

  @Test
  public void emptyMonthShouldReturn0() {

    int monthNum = months.getMonthNumFromName("");
    assertEquals("Should be equal", 0, monthNum);
  }
}



