package com.nhsbsa.model.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.nhsbsa.model.TransferFormDate;
import com.nhsbsa.view.RequestForTransferView;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder.NodeBuilderCustomizableContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class ValidFormDateValidatorTest {

  private ValidFormDateValidator test;
  private ConstraintValidatorContext context;

  @Before
  public void initializeObject() {

    test = new ValidFormDateValidator();
    context = Mockito.mock(ConstraintValidatorContext.class);

    final ConstraintValidatorContext.ConstraintViolationBuilder constraintViolationBuilder =
        Mockito.mock(ConstraintValidatorContext.ConstraintViolationBuilder.class);

    when(context.buildConstraintViolationWithTemplate(Mockito.anyString()))
        .thenReturn(constraintViolationBuilder);

    NodeBuilderCustomizableContext customCtx = mock(NodeBuilderCustomizableContext.class);

    doReturn(customCtx).when(constraintViolationBuilder).addPropertyNode(any());
    doReturn(context).when(customCtx).addConstraintViolation();
  }

  @Test
  public void emptyFieldsReturnTrue() {
    RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N")
    .transferDate(TransferFormDate.builder().year("").month("").days("").build())
        .build();
    final boolean value = test.isValid(rft, null);
    assertThat(value, is(true));
  }

  @Test
  public void invalidYearFormatReturnsFalse() throws Exception {
    RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N")
    .transferDate(TransferFormDate.builder().year("1").month("May").days("18").build()).build();
    final boolean result = test.isValid(rft, context);
    assertThat(result, is(false));
  }

  @Test
  public void invalidDateFormatReturnsFalse() throws Exception {
    RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N")
    .transferDate(TransferFormDate.builder().year("32").month("May").days("2018").build()).build();
    final boolean result = test.isValid(rft, context);
    assertThat(result, is(false));
  }

}
