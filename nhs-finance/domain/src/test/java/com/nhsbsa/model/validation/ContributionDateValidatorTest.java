package com.nhsbsa.model.validation;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.nhsbsa.model.ContributionDate;
import java.time.YearMonth;
import java.util.Arrays;
import java.util.Collection;
import javax.validation.ConstraintValidatorContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;

@RunWith(Parameterized.class)
public class ContributionDateValidatorTest {

  private static ContributionDateValidator contributionDateValidator;
  private static ConstraintValidatorContext constraintValidatorContext;

  @Before
  public void init() {
    final ContributionDateValid contributionDateValid = Mockito.mock(ContributionDateValid.class);
    when(contributionDateValid.monthsInAdvanceLimit()).thenReturn(2);

    contributionDateValidator = new ContributionDateValidator() {
      @Override
      public YearMonth getNow() {
        return YearMonth.now().withYear(2003).withMonth(1);
      }
    };
    contributionDateValidator.initialize(contributionDateValid);

    constraintValidatorContext = Mockito.mock(ConstraintValidatorContext.class);

    final ConstraintValidatorContext.ConstraintViolationBuilder constraintViolationBuilder =
        Mockito.mock(ConstraintValidatorContext.ConstraintViolationBuilder.class);
    when(constraintValidatorContext.buildConstraintViolationWithTemplate(Mockito.anyString()))
        .thenReturn(constraintViolationBuilder);

  }

  @Parameterized.Parameters(name = "{index}: contribution({0}) expected: {1}")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][]{
        {null, false},
        {ContributionDate.builder()
            .contributionMonth(null)
            .contributionYear(2000)
            .build(), false},

        {ContributionDate.builder()
            .contributionMonth("January")
            .contributionYear(null)
            .build(), false},

        {ContributionDate.builder()
            .contributionMonth("May")
            .contributionYear(2000)
            .build(), false},

        {ContributionDate.builder()
            .contributionMonth("January")
            .contributionYear(2000)
            .build(), false},

        {ContributionDate.builder()
            .contributionMonth("February")
            .contributionYear(2003)
            .build(), true},

        {ContributionDate.builder()
            .contributionMonth("March")
            .contributionYear(2000)
            .build(), false},

        {ContributionDate.builder()
            .contributionMonth("January")
            .contributionYear(2003)
            .build(), true},

        {ContributionDate.builder()
            .contributionMonth("-1")
            .contributionYear(2003)
            .build(), false},
        {ContributionDate.builder()
            .contributionMonth("1")
            .contributionYear(-2003)
            .build(), false},
        {ContributionDate.builder()
            .contributionMonth(null)
            .contributionYear(-2003)
            .build(), false},
        {ContributionDate.builder()
            .contributionMonth("-1")
            .contributionYear(null)
            .build(), false},

    });
  }

  @Parameterized.Parameter
  public ContributionDate contributionDate;

  @Parameterized.Parameter(value = 1)
  public boolean valid;

  @Test
  public void test() {

    final boolean isValid = contributionDateValidator
        .isValid(contributionDate, constraintValidatorContext);
    assertEquals("Should match", valid, isValid);
  }


}