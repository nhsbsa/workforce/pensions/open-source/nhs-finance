package com.nhsbsa.model.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

@RunWith(Theories.class)
public class EACodeFormatValidatorTest {

  public EACodeFormatValidator eaCodeFormatValidator = new EACodeFormatValidator();
  private boolean expectedResult;

  public static final @DataPoints("InvalidEACodes") String[] StringInvalidEACodeCandidates =
      {"EA123","GP1234","AE1234","GPSS11"};



  public static final @DataPoints("validEACodes") String[] StringValidEACodesCandidates =
      {"EA1234","GPA123"};


  @Test
  @Theory
  public void containsInvalidFormatReturnsFalse(@FromDataPoints("InvalidEACodes") String candidate) throws Exception {

    expectedResult = eaCodeFormatValidator.isValid(candidate, null);
    assertThat(expectedResult, is(false));
  }

  @Test
  @Theory
  public void containsValidFormatReturnsTrue(@FromDataPoints("validEACodes") String candidate) throws Exception {

    expectedResult = eaCodeFormatValidator.isValid(candidate, null);
    assertThat(expectedResult, is(true));
  }

}