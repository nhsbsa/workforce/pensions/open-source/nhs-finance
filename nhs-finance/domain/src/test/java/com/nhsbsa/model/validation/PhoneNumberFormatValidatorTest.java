package com.nhsbsa.model.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

@RunWith(Theories.class)
public class PhoneNumberFormatValidatorTest {

  public PhoneNumberFormatValidator phoneNumberFormatValidator = new PhoneNumberFormatValidator();
  private boolean expectedResult;

  public static final @DataPoints("InvalidPhoneNumbers") String[] StringInvalidEACodeCandidates =
      {"0123456789","+0770982146","077098214623","0770982146"};



  public static final @DataPoints("validPhoneNumbers") String[] StringValidEACodesCandidates =
      {"01234567890"};


  @Test
  @Theory
  public void containsInvalidFormatReturnsFalse(@FromDataPoints("InvalidPhoneNumbers") String candidate) throws Exception {

    expectedResult = phoneNumberFormatValidator.isValid(candidate, null);
    assertThat(expectedResult, is(false));
  }

  @Test
  @Theory
  public void containsValidFormatReturnsTrue(@FromDataPoints("validPhoneNumbers") String candidate) throws Exception {

    expectedResult = phoneNumberFormatValidator.isValid(candidate, null);
    assertThat(expectedResult, is(true));
  }

}