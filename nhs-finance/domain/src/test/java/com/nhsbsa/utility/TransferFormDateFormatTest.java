package com.nhsbsa.utility;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.nhsbsa.model.TransferFormDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class TransferFormDateFormatTest {

  private TransferFormDateFormat transferFormDateFormat;

  @Before
  public void setup() {
    transferFormDateFormat = new TransferFormDateFormat();
  }

  @Test
  public void testValidDayReturnsTrue() {

    TransferFormDate transferFormDate = TransferFormDate.builder().days("15").month("May").year("2018").build();

    boolean result = transferFormDateFormat.isValidDate(transferFormDate);

    assertTrue(result);
  }

  @Test
  public void testValidDay1DigitsReturnsTrue() {

    TransferFormDate transferFormDate = TransferFormDate.builder().days("1").month("May").year("2018").build();

    boolean result = transferFormDateFormat.isValidDate(transferFormDate);

    assertTrue(result);
  }

  @Test
  public void testInvalidDayReturnsFalse() {

    TransferFormDate transferFormDate = TransferFormDate.builder().days("32").month("May").year("2018").build();

    boolean result = transferFormDateFormat.isValidDate(transferFormDate);

    assertFalse(result);
  }

  @Test
  public void testInvalidMonthReturnsFalse() {

    TransferFormDate transferFormDate = TransferFormDate.builder().days("15").month("Marching").year("2018").build();

    boolean result = transferFormDateFormat.isValidDate(transferFormDate);

    assertFalse(result);
  }

  @Test
  public void testInvalidYearReturnsFalse() {

    TransferFormDate transferFormDate = TransferFormDate.builder().days("15").month("May").year("18").build();

    boolean result = transferFormDateFormat.isValidDate(transferFormDate);

    assertFalse(result);
  }


}
