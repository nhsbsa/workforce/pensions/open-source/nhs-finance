package com.nhsbsa.interceptors;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.nhsbsa.filters.UserPropagationFilter;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.User;
import java.util.UUID;
import javax.servlet.FilterChain;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.HttpClientErrorException;


public class UserPropagationInterceptorTest {

  private HttpRequest request;
  private ClientHttpRequestExecution execution;
  private UserPropagationInterceptor interceptor = new UserPropagationInterceptor();

  @Before
  public void before() {
    request   = Mockito.mock(HttpRequest.class);
    execution = Mockito.mock(ClientHttpRequestExecution.class);
  }

  @Test
  public void testGetRequest() throws Exception {

    when(request.getMethod()).thenReturn(HttpMethod.GET);

    verifyNoMoreInteractions(request);
    interceptor.intercept(request, null, execution);

    verify(execution, times(1)).execute(request, null);
  }

  @Test
  public void testWithFinanceUser() throws Exception {
    HttpHeaders headers = Mockito.mock(HttpHeaders.class);
    String uuid = UUID.randomUUID().toString();

    when(request.getMethod()).thenReturn(HttpMethod.POST);
    when(request.getHeaders()).thenReturn(headers);

    TestingAuthenticationToken authentication = new TestingAuthenticationToken(null, null);
    authentication.setDetails(FinanceUser.builder().uuid(uuid).build());
    SecurityContextHolder.getContext().setAuthentication(authentication);
    interceptor.intercept(request, null, execution);

    verify(execution, times(1)).execute(request, null);
    verify(request, times(1)).getHeaders();
    verify(headers, times(1)).add("PROPAGATED_UUID", uuid);
  }

}