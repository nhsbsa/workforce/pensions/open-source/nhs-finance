package com.nhsbsa.testUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

public class TestableAnnotationsUtil {

  private static final String ANNOTATION_METHOD = "annotationData";
  private static final String ANNOTATIONS = "annotations";

  @SuppressWarnings("unchecked")
  public static Annotation alterAnnotationValueJDK8(Class<?> targetClass, Class<? extends Annotation> targetAnnotation, Annotation targetValue) {
    try {
      Method method = Class.class.getDeclaredMethod(ANNOTATION_METHOD, null);
      method.setAccessible(true);

      Object annotationData = method.invoke(targetClass);

      Field annotations = annotationData.getClass().getDeclaredField(ANNOTATIONS);
      annotations.setAccessible(true);

      Map<Class<? extends Annotation>, Annotation> map = (Map<Class<? extends Annotation>, Annotation>) annotations.get(annotationData);
      Annotation annotation = map.get(targetAnnotation);
      map.put(targetAnnotation, targetValue);
      return annotation;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

}
