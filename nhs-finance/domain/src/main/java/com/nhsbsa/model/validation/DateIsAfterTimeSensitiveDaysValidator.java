package com.nhsbsa.model.validation;

import com.nhsbsa.utility.TransferFormDateFormat;
import com.nhsbsa.view.RequestForTransferView;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateIsAfterTimeSensitiveDaysValidator implements ConstraintValidator<DateIsAfterTimeSensitiveDays, RequestForTransferView> {

  private TransferFormDateFormat transferFormDateFormat = new TransferFormDateFormat();
	
  @Override
  public final void initialize(final DateIsAfterTimeSensitiveDays annotation) {
    // No implementation needed. Nothing to initialise.
  }

  @Override
  public final boolean isValid(final RequestForTransferView rft, final ConstraintValidatorContext constraintValidatorContext) {

    if ("Y".equals(rft.getPayAsSoonAsPossible()) || rft.getTransferDate().getDate() == null
        || !transferFormDateFormat.isValidDate(rft.getTransferDate())) {
      return true;
    }

    LocalDate dateToValidate = LocalDateTime.ofInstant(Instant.ofEpochMilli(rft.getTransferDate().getDate().getTime()), ZoneId.systemDefault()).toLocalDate();
    
    LocalDateTime now = getDateTime();
    int daysAhead = 3;

    if(now.getHour() < 15) {
      daysAhead = 2;
    }

    LocalDate minDate = now.plusDays(daysAhead).toLocalDate();
    if(dateToValidate.isBefore(minDate)) {
      constraintValidatorContext.disableDefaultConstraintViolation();
      if(daysAhead == 2) {
        constraintValidatorContext
            .buildConstraintViolationWithTemplate("{transferDate.afterTimeSensitiveDays.before}")
            .addPropertyNode("transferDate")
            .addConstraintViolation();
      }else {
        constraintValidatorContext
            .buildConstraintViolationWithTemplate("{transferDate.afterTimeSensitiveDays.after}")
            .addPropertyNode("transferDate")
            .addConstraintViolation();
      }
      return false;
    }

    return true;
  }
  
  public LocalDateTime getDateTime() {
      return LocalDateTime.now();
    }

}