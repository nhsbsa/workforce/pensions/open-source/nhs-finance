package com.nhsbsa.model.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.lang3.StringUtils;


public class NameFormatValidator implements ConstraintValidator<NameFormat, String> {

  private final Pattern regPwPattern;
  private static final String REGEXP_VALIDATION = "^[a-zA-Z-' ]+";


  public NameFormatValidator() {
    regPwPattern = Pattern.compile(REGEXP_VALIDATION, Pattern.CASE_INSENSITIVE);
  }

  @Override
  public final void initialize(final NameFormat name) {
    // No implementation needed. Nothing to initialise.
  }

  @Override
  public final boolean isValid(final String name, final ConstraintValidatorContext context) {

    if (StringUtils.isBlank(name)) {
      return true;
    } else {
      return isValidFormat(name);
    }

  }

  /*
   * Check Name is in correct format
   * If all ok return true
   * Invalid format return false
   */
  private boolean isValidFormat(String name) {

    Matcher matcher = regPwPattern.matcher(name);
    return matcher.matches();
  }
}
