package com.nhsbsa.model.validation;

import com.nhsbsa.utility.TransferFormDateFormat;
import com.nhsbsa.view.RequestForTransferView;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by nataliehulse on 08/11/2016.
 */
public class DateLessThan31DaysFromTodayValidator implements
    ConstraintValidator<DateLessThan31DaysFromToday, RequestForTransferView> {

  private String greaterThan31Days;

  private TransferFormDateFormat transferFormDateFormat = new TransferFormDateFormat();

  @Override
  public final void initialize(final DateLessThan31DaysFromToday annotation) {
    greaterThan31Days = annotation.message();
  }

  @Override
  public final boolean isValid(final RequestForTransferView rft,
      final ConstraintValidatorContext context) {

    if ("Y".equals(rft.getPayAsSoonAsPossible()) || rft.getTransferDate().getDate() == null
        || !transferFormDateFormat.isValidDate(rft.getTransferDate())) {
      return true;
    }

    final LocalDate today = getDate();
    // Have to create an Instant to convert Java.Util.Date to Java.Time LocalDate
    Instant instant = Instant.ofEpochMilli(rft.getTransferDate().getDate().getTime());
    // Create variable of Instance
    LocalDate futureDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
    long daysBetween = ChronoUnit.DAYS.between(today, futureDate);
    if (daysBetween > 31) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate(greaterThan31Days)
          .addPropertyNode("transferDate")
          .addConstraintViolation();
      return false;
    }

    return true;
  }

  public LocalDate getDate() {
    return LocalDate.now();
  }

}
