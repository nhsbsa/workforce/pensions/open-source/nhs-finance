package com.nhsbsa.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DeleteUser {

  private String uuid;

  @NotBlank(message = "{delete.user.notBlank}")
  private String deleteUserYesNo;

}
