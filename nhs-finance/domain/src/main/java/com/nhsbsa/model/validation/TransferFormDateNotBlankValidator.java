package com.nhsbsa.model.validation;

/**
 * Created by Mark Lishman on 07/11/2016.
 */

import com.nhsbsa.view.RequestForTransferView;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.lang3.StringUtils;

public class TransferFormDateNotBlankValidator implements ConstraintValidator<TransferFormDateNotBlank, RequestForTransferView> {

  private String dateNotBlank;

  @Override
  public void initialize(TransferFormDateNotBlank constraintAnnotation) {
    dateNotBlank = constraintAnnotation.message();
  }

  @Override
  public boolean isValid(RequestForTransferView validator, ConstraintValidatorContext context) {

    if ("N".equals(validator.getPayAsSoonAsPossible()) && (
        StringUtils.isEmpty(validator.getTransferDate().getDays()) ||
        StringUtils.isEmpty(validator.getTransferDate().getMonth()) ||
        StringUtils.isEmpty(validator.getTransferDate().getYear()))) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate(dateNotBlank)
          .addPropertyNode("transferDate")
          .addConstraintViolation();
      return false;
    }

    return true;

  }
}
