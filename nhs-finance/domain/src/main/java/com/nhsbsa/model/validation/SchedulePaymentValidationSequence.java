package com.nhsbsa.model.validation;

import javax.validation.GroupSequence;
import javax.validation.groups.Default;

/**
 * Created by Mark Lishman on 17/11/2016.
 */

@GroupSequence({FirstSchedulePaymentValidationGroup.class, SecondSchedulePaymentValidationGroup.class, SchedulePaymentValidationGroup.class})
public interface SchedulePaymentValidationSequence extends Default {

}

