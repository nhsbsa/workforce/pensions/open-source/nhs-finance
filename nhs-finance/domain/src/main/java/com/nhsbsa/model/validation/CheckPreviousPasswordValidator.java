package com.nhsbsa.model.validation;

import com.nhsbsa.security.SetPassword;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by nataliehulse on 02/10/2017.
 */

public class CheckPreviousPasswordValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(SetPassword.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        if (errors.hasErrors()) {
            return;
        }

        SetPassword setPassword = (SetPassword) target;
        if (setPassword.isPasswordSameAsPrevious()) {
            errors.rejectValue("newPassword", "setPassword.passwordSameAsPrevious");
            return;
        }
    }
}
