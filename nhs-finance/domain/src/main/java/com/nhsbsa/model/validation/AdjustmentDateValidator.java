package com.nhsbsa.model.validation;

import com.nhsbsa.model.Adjustment;
import com.nhsbsa.model.MonthNum;
import com.nhsbsa.model.ContributionDate;

public final class AdjustmentDateValidator {

  private AdjustmentDateValidator(){}

  public static boolean isAdjustmentDateBeforeContributionDate(Adjustment adjustment, ContributionDate contributionDate) {
    MonthNum monthNum = new MonthNum();
    if (adjustment.getAdjustmentContributionDate().getContributionYear() == null ||
        adjustment.getAdjustmentContributionDate().getContributionMonth() == null) {
      return true;
    }
    Integer adjustmentMonths =
        (adjustment.getAdjustmentContributionDate().getContributionYear() * 12)
            + monthNum.getMonthNumFromName(
            adjustment.getAdjustmentContributionDate().getContributionMonth());

    Integer contributionMonths = (contributionDate.getContributionYear() * 12)
        + monthNum.getMonthNumFromName(contributionDate.getContributionMonth());

    if (monthNum
        .getMonthNumFromName(adjustment.getAdjustmentContributionDate().getContributionMonth())
        == 0) {
      return true;
    }
    return adjustmentMonths < contributionMonths;
  }



}
