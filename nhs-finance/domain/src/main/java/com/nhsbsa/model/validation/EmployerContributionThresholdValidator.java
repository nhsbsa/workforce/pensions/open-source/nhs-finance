package com.nhsbsa.model.validation;

import com.nhsbsa.view.RequestForTransferView;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

/**
 * Created by Mark Lishman on 18/11/2016.
 */
public class EmployerContributionThresholdValidator implements
    ConstraintValidator<EmployerContributionThreshold, RequestForTransferView> {

  private static final BigDecimal MINIMUM_PERCENT = new BigDecimal("0.1433");
  private static final BigDecimal ERROR_MESSAGE_PERCENT = new BigDecimal("0.1438");
  private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("##.00");


  @Override
  public void initialize(EmployerContributionThreshold constraint) {
    // No implementation needed. Nothing to initialise.
  }

  @Override
  public boolean isValid(RequestForTransferView rft, ConstraintValidatorContext context) {
    if (rft.getTotalPensionablePay() == null || rft.getEmployerContributions() == null) {
      return true;
    }
    final BigDecimal minimumValue = rft.getTotalPensionablePay().multiply(MINIMUM_PERCENT);


    boolean isValid = rft.getEmployerContributions().compareTo(minimumValue) >= 0;
    if (!isValid) {

      String value = "£"+ DECIMAL_FORMAT.format(rft.getTotalPensionablePay().multiply(ERROR_MESSAGE_PERCENT));

      HibernateConstraintValidatorContext hibernateContext = context.unwrap( HibernateConstraintValidatorContext.class );

      hibernateContext.disableDefaultConstraintViolation();
      hibernateContext.addExpressionVariable( "value", value )
          .buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
          .addPropertyNode("employerContributions")
          .addConstraintViolation();
    }

    return isValid;
  }
}
