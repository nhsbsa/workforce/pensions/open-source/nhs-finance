package com.nhsbsa.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import com.nhsbsa.model.validation.AdjustmentContributionDateValid;
import com.nhsbsa.model.validation.AdjustmentNotNull;
import com.nhsbsa.model.validation.AdjustmentNotNullValidationGroup;
import com.nhsbsa.model.validation.AdjustmentValidationGroup;
import com.nhsbsa.view.validator.NotZero;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@Table(name = "adjustment")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@AdjustmentNotNull(message = "{adjustment.notBlank}", groups = AdjustmentNotNullValidationGroup.class)
public class Adjustment implements BaseEntity<Long> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "adj_id", insertable = false, updatable = false)
  private Long id;

  @Embedded
  @AdjustmentContributionDateValid(groups = {AdjustmentValidationGroup.class})
  private ContributionDate adjustmentContributionDate = ContributionDate.builder().build();

  @NotZero(message = "{adjustment.employeeContributions.notZero}", groups = AdjustmentValidationGroup.class)
  @Digits(integer=10, fraction=2, message = "{adjustment.employeeContributions.invalid}", groups = AdjustmentValidationGroup.class)
  @DecimalMin(value = "-99999999.99", message = "{adjustment.employeeContributions.invalid}", groups = AdjustmentValidationGroup.class)
  @DecimalMax(value = "99999999.99", message = "{adjustment.employeeContributions.invalid}", groups = AdjustmentValidationGroup.class)
  private BigDecimal employeeContributions;

  @NotZero(message = "{adjustment.employeeAddedYears.notZero}", groups = AdjustmentValidationGroup.class)
  @Digits(integer=10, fraction=2, message = "{adjustment.employeeAddedYears.invalid}", groups = AdjustmentValidationGroup.class)
  @DecimalMin(value = "-99999999.99", message = "{adjustment.employeeAddedYears.invalid}", groups = AdjustmentValidationGroup.class)
  @DecimalMax(value = "99999999.99", message = "{adjustment.employeeAddedYears.invalid}", groups = AdjustmentValidationGroup.class)
  private BigDecimal employeeAddedYears;

  @NotZero(message = "{adjustment.additionalPension.notZero}", groups = AdjustmentValidationGroup.class)
  @Digits(integer=10, fraction=2, message = "{adjustment.additionalPension.invalid}", groups = AdjustmentValidationGroup.class)
  @DecimalMin(value = "-99999999.99", message = "{adjustment.additionalPension.invalid}", groups = AdjustmentValidationGroup.class)
  @DecimalMax(value = "99999999.99", message = "{adjustment.additionalPension.invalid}", groups = AdjustmentValidationGroup.class)
  private BigDecimal additionalPension;

  @NotZero(message = "{adjustment.errbo.notZero}", groups = AdjustmentValidationGroup.class)
  @Digits(integer=10, fraction=2, message = "{adjustment.errbo.invalid}", groups = AdjustmentValidationGroup.class)
  @DecimalMin(value = "-99999999.99", message = "{adjustment.errbo.invalid}", groups = AdjustmentValidationGroup.class)
  @DecimalMax(value = "99999999.99", message = "{adjustment.errbo.invalid}", groups = AdjustmentValidationGroup.class)
  private BigDecimal errbo;

  @NotZero(message = "{adjustment.employerContributions.notZero}", groups = AdjustmentValidationGroup.class)
  @Digits(integer=10, fraction=2, message = "{adjustment.employerContributions.invalid}", groups = AdjustmentValidationGroup.class)
  @DecimalMin(value = "-99999999.99", message = "{adjustment.employerContributions.invalid}", groups = AdjustmentValidationGroup.class)
  @DecimalMax(value = "99999999.99", message = "{adjustment.employerContributions.invalid}", groups = AdjustmentValidationGroup.class)
  private BigDecimal employerContributions;

}
