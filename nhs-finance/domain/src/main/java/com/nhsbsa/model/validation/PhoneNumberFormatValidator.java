package com.nhsbsa.model.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by nataliehulse on 29/09/2017.
 */

public class PhoneNumberFormatValidator implements ConstraintValidator<PhoneNumberFormat, String> {

  private final Pattern regFullNamePattern;
  private static final String REGEXP_FULLNAME_VALIDATION = "([0-9]{11})";

  public PhoneNumberFormatValidator() {

    regFullNamePattern = Pattern.compile(REGEXP_FULLNAME_VALIDATION);
  }

  @Override
  public void initialize(PhoneNumberFormat phoneNumber) {
    //nothing needed from annotation
  }

  @Override
  public final boolean isValid(final String phoneNumber, final ConstraintValidatorContext context) {

    return StringUtils.isNotBlank(phoneNumber) && isValidFormat(phoneNumber);

  }

  /*
   * Checks for valid Phone Number format
   * If all ok return true
   * Invalid pattern return false
   */
  private boolean isValidFormat(String phoneNumber) {

    Matcher matcher = regFullNamePattern.matcher(phoneNumber);
    return matcher.matches();
  }

}


