package com.nhsbsa.model.validation;


import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;

/**
 * Created by stuartbradley on 27/03/2017.
 */

@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = TotalAmountToBeDebitedValidator.class)
@Documented
@ReportAsSingleViolation
public @interface TotalAmountToBeDebited {

  String message() default "{contsAndPayments.totalAmountRange}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
