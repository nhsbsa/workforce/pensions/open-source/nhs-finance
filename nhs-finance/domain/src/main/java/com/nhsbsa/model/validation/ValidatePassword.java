package com.nhsbsa.model.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by nataliehulse on 29/09/2017.
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = {PasswordFormatValidator.class})
@Documented
@ReportAsSingleViolation
public @interface ValidatePassword {

  String message() default "{setPassword.IncorrectFormat}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}

