package com.nhsbsa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.validation.constraints.NotNull;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jeffreya on 26/09/2016.
 * User for authorization
 */

@Builder()
@Data
@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id"})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", insertable = false, updatable = false)
    private Long id;

    private String name;
    private String hash;
    private String uuid;
    private boolean firstLogin;
    @Builder.Default private Integer retries = 0;
    @Builder.Default private boolean locked = false;
    @Builder.Default private Date deletedOn = null;

    @NotNull
    @Builder.Default private String role = UserRoles.ROLE_STANDARD.name();

    @OneToMany
    @JoinTable(name = "user_app",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "application_id"))
    private List<AuthorizedApplication> applications;

    @JsonIgnore
    public void addApplication(final AuthorizedApplication authorizedApplication) {
        if (applications == null) {
            applications = new ArrayList<>();
        }
        applications.add(authorizedApplication);
    }

    @JsonIgnore
    public void increaseRetries() {
        this.retries++;
    }

    @JsonIgnore
    public boolean userIsDeleted() {
        return deletedOn != null;
    }
}
