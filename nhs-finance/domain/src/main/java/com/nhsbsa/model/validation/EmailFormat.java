package com.nhsbsa.model.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Nat Hulse on 24/11/2017.
 *
 * Annotation @EmailFormat is used around the class member
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = EmailFormatValidator.class)
@Documented
@ReportAsSingleViolation
public @interface EmailFormat {

  String message() default "{forgottenPassword.emailFormat}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}
