package com.nhsbsa.model.validation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;

@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = {EACodeFormatValidator.class})
@Documented
@ReportAsSingleViolation
public @interface EACodeFormat {

  String message() default "{eaCodeFormat.NotValid}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}

