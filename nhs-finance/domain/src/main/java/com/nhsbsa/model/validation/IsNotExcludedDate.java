package com.nhsbsa.model.validation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;

@Target({TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = IsNotExcludedDateValidator.class)
@Documented
@ReportAsSingleViolation
public @interface IsNotExcludedDate {

  String message() default "{transferDate.notExcludedDate}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}