package com.nhsbsa.model;

import java.io.Serializable;

/**
 * Created by MArk Lishman on 31/10/2016.
 */


public interface BaseEntity<T> extends Serializable {

  T getId();
}
