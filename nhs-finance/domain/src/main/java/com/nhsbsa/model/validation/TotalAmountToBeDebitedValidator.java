package com.nhsbsa.model.validation;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by stuartbradley on 27/03/2017.
 */


public class TotalAmountToBeDebitedValidator implements
    ConstraintValidator<TotalAmountToBeDebited, BigDecimal> {

  @Override
  public final void initialize(final TotalAmountToBeDebited annotation) {
    //used to initialize the TotalAmountToBeDebited annotation
  }

  @Override
  public final boolean isValid(final BigDecimal amountValueBigDecimal,
      final ConstraintValidatorContext context) {

    if (amountValueBigDecimal == null) {
      return true;
    }

    final String amountValueString = amountValueBigDecimal.toString();
    return isValidFormat(amountValueString);
  }

  private boolean isValidFormat(String inputAmount) {

    final String regExp = "[-+]?([0-9]{1,8})?+([.][0-9]{0,2})?";

    Pattern pattern = Pattern.compile(regExp);
    Matcher matcher = pattern.matcher(inputAmount);
    return matcher.matches();
  }
}



