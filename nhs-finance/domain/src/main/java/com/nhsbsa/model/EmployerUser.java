package com.nhsbsa.model;

import com.nhsbsa.model.validation.EmailFormat;
import com.nhsbsa.model.validation.ValidatePassword;
import com.nhsbsa.view.OrganisationView;
import java.util.List;
import lombok.Data;

@Data
public abstract class EmployerUser {

  @EmailFormat
  private String email;

  @ValidatePassword
  private String firstPassword;

  private String uuid;

  public abstract String getFirstName();

  public abstract String getLastName();

  public abstract String getEmployerType();

  public abstract String getContactTelephone();

  public abstract UserRoles getRole();

  public abstract String getEaCode();

  public abstract List<OrganisationView> getOrganisations();


}
