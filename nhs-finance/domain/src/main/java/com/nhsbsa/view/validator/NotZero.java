package com.nhsbsa.view.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Validate that the annotated item is non-zero
 *
 * Created by duncan on 24/03/2017.
 */
@Target( {METHOD, FIELD, ANNOTATION_TYPE} )
@Retention(RUNTIME)
@Constraint(validatedBy = NotZeroValidator.class)
@Documented
public @interface NotZero {

    String message() default "{constraint.not.zero}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
