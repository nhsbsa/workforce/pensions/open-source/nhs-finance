package com.nhsbsa.view.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

/**
 * Implementation of NotZero constraint
 *
 * Created by duncan on 24/03/2017.
 */
public class NotZeroValidator implements ConstraintValidator<NotZero, BigDecimal> {

    @Override
    public void initialize(NotZero constraintAnnotation) {
        // No action needed
    }

    @Override
    public boolean isValid(BigDecimal value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        return value.compareTo(BigDecimal.ZERO) != 0;
    }
}
