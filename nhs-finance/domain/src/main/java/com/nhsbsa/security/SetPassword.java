package com.nhsbsa.security;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nhsbsa.model.validation.ValidatePassword;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by nataliehulse on 28/09/2017.
 */

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class SetPassword {

    private String uuid;

    private boolean firstLogin;

    @ValidatePassword
    private String newPassword;

    @JsonIgnore
    private String passwordConfirm;

    @JsonIgnore
    private boolean passwordSameAsPrevious;

    public String getNewPassword() {
        if (newPassword == null) {
            return "";
        }
        return newPassword;
    }

    public void setNewPassword(String newPassword) {

        this.newPassword = newPassword;
    }

    public String getPasswordConfirm() {
        if (passwordConfirm == null) {
            return "";
        }
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {

        this.passwordConfirm = passwordConfirm;
    }

}

