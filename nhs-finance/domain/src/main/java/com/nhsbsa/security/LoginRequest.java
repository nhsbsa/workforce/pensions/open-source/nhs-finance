package com.nhsbsa.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by jeffreya on 19/08/2016.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor // NOSONAR
public class LoginRequest {

    @NotNull
    private String username;

    @NotNull
    private String password;

    private AppToken appToken;

    private String uuid;

    private boolean firstLogin;

}
