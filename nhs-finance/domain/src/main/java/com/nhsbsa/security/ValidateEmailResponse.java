package com.nhsbsa.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by nataliehulse on 27/11/2017.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidateEmailResponse {


    private String uuid;

    private String userName;

    private String token;

}

