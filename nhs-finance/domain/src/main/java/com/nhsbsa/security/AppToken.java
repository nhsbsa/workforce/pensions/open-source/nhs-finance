package com.nhsbsa.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by jeffreya on 27/09/2016. AppToken
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "token"})
public class AppToken {

  private String id;
  private String token;
}
