package com.nhsbsa.interceptors;

import java.io.IOException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

/**
 * Created by jeffreya on 27/09/2016. ApplicationHeaderInterceptor
 */

public class ApplicationHeaderInterceptor implements ClientHttpRequestInterceptor {

  private static final String APP_NAME = "fic";

  @Override
  public ClientHttpResponse intercept(
      HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
      throws IOException {
    final HttpHeaders headers = request.getHeaders();
    headers.add("AUTH-APP-ID", APP_NAME);
    return execution.execute(request, body);
  }

}