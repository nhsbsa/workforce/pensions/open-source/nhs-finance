package com.nhsbsa.filters;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.User;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

@Slf4j
@Component
@Profile("user-propagation-filter")
public class UserPropagationFilter extends OncePerRequestFilter {

  @Autowired
  private RestTemplate restTemplate;

  private String authorizationUserUrl;

  private List<Pattern> exclusionPatterns;

  public UserPropagationFilter(
      @Autowired
      RestTemplate restTemplate,
      @Value("${authorization.user.url}")
      String authorizationUserUrl,
      @Value("${authorization.exclude:}#{T(java.util.Collections).emptyList()}")
      String[] exclusions
  ){
    this.restTemplate = restTemplate;
    this.authorizationUserUrl = authorizationUserUrl;

    if(exclusions.length > 0){
      logger.trace("Exclusions: "+ Arrays.toString(exclusions));
      this.exclusionPatterns = Arrays.stream(exclusions).map(Pattern::compile).collect(Collectors.toList());
    } else{
      logger.trace("Empty exclusion list");
      this.exclusionPatterns = new ArrayList<>();
    }
  }

  @Override
  protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

    String path = httpServletRequest.getServletPath();

    boolean matchesExclusions = exclusionPatterns.stream()
        .map(p -> p.matcher(path))
        .anyMatch(Matcher::matches);

    if ( matchesExclusions || HttpMethod.GET.name().equals(httpServletRequest.getMethod())) {
      filterChain.doFilter(httpServletRequest, httpServletResponse);
      return ;
    }

    String userUuid = httpServletRequest.getHeader("PROPAGATED_UUID");

    if ( userUuid != null && userUuid.trim().length() > 0 ) {

      String url = authorizationUserUrl + userUuid;
      ResponseEntity<User> userResponse;
      try {
        userResponse = restTemplate.getForEntity(url, User.class);
      } catch (HttpClientErrorException e){
        userResponse = ResponseEntity.status(e.getStatusCode()).build();
      }

      if ( userResponse.getStatusCode().is2xxSuccessful() ){
        User user = userResponse.getBody();

        FinanceUser financeUser = FinanceUser.builder()
            .username(user.getName())
            .uuid(user.getUuid())
            .role(user.getRole())
            .build();
        PreAuthenticatedAuthenticationToken token = new PreAuthenticatedAuthenticationToken(
            user.getName(), "", financeUser.getAuthorities());
        token.setDetails(financeUser);

        SecurityContextHolder.getContext().setAuthentication(token);

        filterChain.doFilter(httpServletRequest, httpServletResponse);

      } else {
        httpServletResponse.sendError(HttpStatus.BAD_REQUEST.value(), "UUID does not exist");
        logger.error(String.format("User retrieval error: %s", userResponse.getStatusCode().toString()));
      }

    } else {
      String errorMessage = "Missing PROPAGATED_UUID Http header on path: " + path;
      httpServletResponse.sendError(HttpStatus.BAD_REQUEST.value(), errorMessage);
      logger.error(errorMessage);
    }

  }
}
