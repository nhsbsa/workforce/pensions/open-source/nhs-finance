package uk.nhs.nhsbsa.finance.employercontributions.service;

import static org.junit.Assert.assertEquals;

import com.nhsbsa.model.EmployerTypes;
import com.nhsbsa.view.TransferView;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class TransferRecordTest {

  @Mock
  private TransferRecord transferRecord;


  @Before
  public void before() {
    transferRecord = Mockito.spy(new TransferRecord());
  }

  @Test
  public void testAbsoluteYearMonthToDate() throws Exception{

    SimpleDateFormat sdf = new SimpleDateFormat("yyMM");
    String dateInString = "201811";
    Date date = sdf.parse(dateInString);

    Date result = transferRecord.absoluteYearMonthToDate(dateInString);

    assertEquals(result, date);
  }

  @Test
  public void testDateToFinancialYearMonthNextFinancialYear() throws Exception{

    SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
    String dateInString = "01042018";
    Date date = sdf.parse(dateInString);

    String result = transferRecord.dateToFinancialYearMonth(date);

    assertEquals(result, "1901");
  }

  @Test
  public void testDateToFinancialYearMonthThisFinancialYear() throws Exception{

    SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
    String dateInString = "01032018";
    Date date = sdf.parse(dateInString);

    String result = transferRecord.dateToFinancialYearMonth(date);

    assertEquals(result, "1812");
  }

  @Test
  public void testCurrencyAmountToBaseAmount() throws Exception{

    BigDecimal value = new BigDecimal("-40");

    String result = transferRecord.currencyAmountToBaseAmount(value);

    assertEquals(result, "40");
  }

  @Test
  public void testCurrencyAmountToPaymentTypeReturnsCrn() throws Exception{

    BigDecimal value = new BigDecimal("-40");

    String result = transferRecord.currencyAmountToPaymentType(value);

    assertEquals(result, "CRN");
  }

  @Test
  public void testCurrencyAmountToPaymentTypeReturnsInv() throws Exception{

    BigDecimal value = new BigDecimal("0");

    String result = transferRecord.currencyAmountToPaymentType(value);

    assertEquals(result, "INV");
  }

  @Test
  public void testSalesAnalysisFromTypeAndAdjustmentContributionA() throws Exception{

    ContributionType contributionType = ContributionType.A;

    String result = transferRecord.salesAnalysisFromTypeAndAdjustment(contributionType, false, "A");

    assertEquals(result, "XR7201");
  }

  @Test
  public void testSalesAnalysisFromTypeAndAdjustmentContributionB() throws Exception{

    ContributionType contributionType = ContributionType.B;

    String result = transferRecord.salesAnalysisFromTypeAndAdjustment(contributionType, false, "B");

    assertEquals(result, "XR7203");
  }

  @Test
  public void testSalesAnalysisFromTypeAndAdjustmentContributionD() throws Exception{

    ContributionType contributionType = ContributionType.D;

    String result = transferRecord.salesAnalysisFromTypeAndAdjustment(contributionType, false, "D");

    assertEquals(result, "XR7202");
  }

  @Test
  public void testSalesAnalysisFromTypeAndAdjustmentContributionE() throws Exception{

    ContributionType contributionType = ContributionType.E;

    String result = transferRecord.salesAnalysisFromTypeAndAdjustment(contributionType, false, "E");

    assertEquals(result, "XR7200");
  }

  @Test
  public void testSalesAnalysisFromTypeAndAdjustmentContributionR() throws Exception{

    ContributionType contributionType = ContributionType.R;

    String result = transferRecord.salesAnalysisFromTypeAndAdjustment(contributionType, false, "R");

    assertEquals(result, "XR7100");
  }

  @Test
  public void testSalesAnalysisFromTypeAndAdjustmentContributionREmployerTypeG() throws Exception{

    ContributionType contributionType = ContributionType.R;

    String result = transferRecord.salesAnalysisFromTypeAndAdjustment(contributionType, false, "G");

    assertEquals(result, "XR7110");
  }

  @Test
  public void testSalesAnalysisFromTypeAndAdjustmentContributionEwithAdjustment() throws Exception{

    ContributionType contributionType = ContributionType.E;

    String result = transferRecord.salesAnalysisFromTypeAndAdjustment(contributionType, true, "E");

    assertEquals(result, "XR7200Adj");
  }

  @Test
  public void testFormatTransactionDate() throws Exception{

    LocalDate localDate = LocalDate.of(2018, 11, 01);
    String dateInString = "01/11/2018";

    String result = transferRecord.formatTransactionDate(localDate);

    assertEquals(result, dateInString);
  }

  @Test
  public void testMakeTransferRecordFrom() throws Exception{

    TransferView transferView = TransferView.builder()
        .accountCode("123")
        .transactionDate(LocalDate.of(2018,10,01))
        .contributionYearMonth("1810")
        .employerType(EmployerTypes.STAFF.name())
        .build();

     BigDecimal amount = new BigDecimal("45");

    TransferRecord result = transferRecord.makeTransferRecordFrom(transferView, amount, ContributionType.R, false);

    assertEquals(transferView.getAccountCode(), result.eaCode);
    assertEquals(amount.toString(), result.baseAmount);
    assertEquals(result.description, "JS: RFTOL REF: 1231907 DES: Online Monthly Cont'tions Staff/GP: STAFF");
    assertEquals(result.journalSource, "RFTOL");

  }

}