package uk.nhs.nhsbsa.finance.employercontributions.service;

import com.nhsbsa.view.ListWrappingView;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uk.nhs.nhsbsa.finance.employercontributions.model.RequestForTransfer;
import uk.nhs.nhsbsa.finance.employercontributions.repository.RequestForTransferRepository;
import uk.nhs.nhsbsa.finance.employercontributions.utility.FileDateGenerator;
import uk.nhs.nhsbsa.finance.employercontributions.view.TransferViewConverter;

/**
 * Created by Mark Lishman on 18/08/2016.
 */
@Slf4j
@Service
@Transactional
public class FinanceService {

  private RequestForTransferRepository requestForTransferRepository;
  private CsvGeneratingService csvGeneratingService;
  private final FileDateGenerator fileDateGenerator;

  @Value("${finance.facade.uri}")
  private String financeFacadeUri;

  private RestTemplate restTemplate;

  @SuppressWarnings("squid:S1075")
  private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

  @Autowired
  public FinanceService(
      final RequestForTransferRepository requestForTransferRepository,
      final RestTemplate restTemplate,
      final CsvGeneratingService csvGeneratingService,
      final FileDateGenerator fileDateGenerator

  ) {
    this.requestForTransferRepository = requestForTransferRepository;
    this.restTemplate = restTemplate;
    this.fileDateGenerator = fileDateGenerator;
    this.csvGeneratingService = csvGeneratingService;

  }

  /**
   * Testing assistance.
   */
  public void setFinanceFacadeUri(String uri) {
    this.financeFacadeUri = uri;
  }

  public RequestForTransfer getRequestForTransferByRftUuid(final String rftUuid) {
    return requestForTransferRepository.findByRftUuid(rftUuid);
  }

  public RequestForTransfer saveRequestForTransfer(final RequestForTransfer requestForTransfer) {
    return requestForTransferRepository.save(requestForTransfer);
  }

  private ListWrappingView convertToFinanceFacadeContract(Set<RequestForTransfer> processingSet) {
    return ListWrappingView.jsonFactory(
        processingSet
            .stream()
            .map(TransferViewConverter::of)
            .collect(Collectors.toList())
    );
  }

  public String[] getFileList() {

    // Get all possible dates from database
    List<LocalDate> allDates = Arrays.stream(requestForTransferRepository.getCsvFileList())
        .map(u -> LocalDate.parse(u, FORMATTER))
        .collect(Collectors.toList());

    // Filter allDates to only return working days (exclude weekends / bank holidays)
    List<LocalDate> workingDates = new ArrayList<>();
    allDates.forEach(
        d -> {
          if (!fileDateGenerator.isWorkingDay(d)) {
            workingDates.add(fileDateGenerator.getNearestWorkingDay(d, 1));
          } else {
            workingDates.add(d);
          }
        }
    );

    // Filter to only include working days (excluding today if before 1pm)
    List<LocalDate> fileList = new ArrayList<>();
    workingDates.forEach(
        d -> {
          if (!d.isEqual(dateNow())) {
            fileList.add(d);
          } if (d.isEqual(dateNow()) && fileDateGenerator.isAfterDeadlineHour()) {
            fileList.add(d);
          }
        });

    // desc order
    fileList.sort(Comparator.reverseOrder());

    // return a distinct list of dates (no duplicates)
    return fileList.stream().distinct().map(u -> u.toString()).toArray(String[]::new);

  }

  public Optional<byte[]> getFile(String fileName) {
    try {
      LocalDate date = LocalDate.parse(fileName, FORMATTER);
      LocalDate previousDate = fileDateGenerator.getNearestWorkingDay(date, -1);

      Set<RequestForTransfer> processingSet = requestForTransferRepository
          .findByFormattedCsvProcessedDate(fileName, previousDate.toString());
      ListWrappingView set = convertToFinanceFacadeContract(processingSet);
      return csvGeneratingService.generate(set);
    } catch (IOException e) {
      log.error("Error when generating csv file");
    }

    return Optional.empty();

  }

  public Optional<LocalDate> getSubmitDateByEaCode(String eaCode) {

    Optional<Instant> maybeDate = Optional.ofNullable(requestForTransferRepository.findSubmitDateByEaCode(eaCode));

    return maybeDate.map( date -> date.atZone(ZoneId.systemDefault()).toLocalDate());

  }

  public LocalDate dateNow() {

    return LocalDate.now();
  }


}
