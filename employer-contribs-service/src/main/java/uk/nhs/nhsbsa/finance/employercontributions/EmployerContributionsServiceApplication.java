package uk.nhs.nhsbsa.finance.employercontributions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(value = {"com.nhsbsa.model","uk.nhs.nhsbsa.finance.employercontributions.model"})
public class EmployerContributionsServiceApplication {

	public EmployerContributionsServiceApplication() {
		// Default constructor; Spring needs to be able to construct
		// instances.
	}

	public static void main(String[] args) {
		SpringApplication.run(EmployerContributionsServiceApplication.class, args);
	}

}
