package uk.nhs.nhsbsa.finance.employercontributions.model.validation;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.lang3.StringUtils;
import uk.nhs.nhsbsa.finance.employercontributions.model.RequestForTransfer;

public class TransferFormDateNotBlankValidator implements ConstraintValidator<TransferFormDateNotBlank, RequestForTransfer> {

  private String dateNotBlank;

  @Override
  public void initialize(TransferFormDateNotBlank constraintAnnotation) {
    dateNotBlank = constraintAnnotation.message();
  }

  @Override
  public boolean isValid(RequestForTransfer validator, ConstraintValidatorContext context) {

    if ("N".equals(validator.getPayAsSoonAsPossible()) &&
        StringUtils.isEmpty(validator.getTransferDate().getDays()) &&
        StringUtils.isEmpty(validator.getTransferDate().getMonth()) &&
        StringUtils.isEmpty(validator.getTransferDate().getYear())) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate(dateNotBlank)
          .addPropertyNode("transferDate")
          .addConstraintViolation();
      return false;
    }

    return true;

  }
}
