package uk.nhs.nhsbsa.finance.employercontributions.controller;

import static uk.nhs.nhsbsa.finance.employercontributions.view.RequestForTransferViewConverter.toView;

import com.nhsbsa.view.RequestForTransferView;
import io.swagger.annotations.ApiOperation;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import uk.nhs.nhsbsa.finance.employercontributions.model.RequestForTransfer;
import uk.nhs.nhsbsa.finance.employercontributions.service.FinanceService;

@Slf4j
@RestController
public class EmployerContributionsController {

  private final FinanceService financeService;

  @Autowired
  public EmployerContributionsController(final FinanceService financeService) {
    this.financeService = financeService;
  }

  @GetMapping(value = "/requestfortransfer/{rftUuid}")
  @ApiOperation(value = "default", notes = "default")
  public ResponseEntity<RequestForTransferView> getRequestForTransferByRftUuid(
      @PathVariable("rftUuid") final String rftUuid) {

    log.info("GET request for tranfer with rft uuid = {}", rftUuid);
    RequestForTransfer rft = financeService.getRequestForTransferByRftUuid(rftUuid);
    return ResponseEntity.ok(toView(rft));
  }

  @GetMapping(value = {"/csv","/csv/{date}"})
  public ResponseEntity<String> getCSV(
      @PathVariable(value = "date", required = false) String date){

    if(date == null || date.isEmpty()){
      date = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDate.now());
    }
    Optional<byte[]> file = financeService.getFile(date);

    if(! file.isPresent()){
      return ResponseEntity.notFound().build();
    }else{
      return ResponseEntity.ok(new String(file.get()));
    }
  }

  @PostMapping(value = "/requestfortransfer")
  @ApiOperation(value = "default", notes = "default")
  public ResponseEntity<RequestForTransferView> saveRequestForTransfer(
      @RequestBody @Valid final RequestForTransfer requestForTransfer) {

    log.info("Saving request for transfer with uuid = {}", requestForTransfer.getRftUuid());
    // only set rftUuid if it hasnt already been set as this method is used to Create And Update
    if (requestForTransfer.getRftUuid() == null) {
      requestForTransfer.setRftUuid(java.util.UUID.randomUUID().toString());
    }

    RequestForTransfer rft = financeService.saveRequestForTransfer(requestForTransfer);
    return ResponseEntity.ok(toView(rft));
  }

  @GetMapping(value = "/file")
  public ResponseEntity<String[]> getFileList() {

    log.info("Retrieving file list");
    return ResponseEntity.ok(financeService.getFileList());
  }

  @GetMapping(value = "/file/{fileName}")
  public ResponseEntity<byte[]> getFile(@PathVariable("fileName") final String fileName) {

    log.info("Retrieving file: {}", fileName);
    Optional<byte[]> maybeFile = financeService.getFile(fileName);

    if (!maybeFile.isPresent()) {
      log.error("Could not retrieve file");
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(maybeFile.get());

  }

  @GetMapping(value = "/submitdate/{eaCode}")
  public ResponseEntity<LocalDate> getSubmitDateByEaCode(@PathVariable("eaCode") final String eaCode) {

    log.info("Retrieving Submit Date for: {}", eaCode);
    Optional<LocalDate> maybeDate  = financeService.getSubmitDateByEaCode(eaCode);

    return maybeDate.map(u -> ResponseEntity.ok(u)).orElse( ResponseEntity.noContent().build());

  }

}