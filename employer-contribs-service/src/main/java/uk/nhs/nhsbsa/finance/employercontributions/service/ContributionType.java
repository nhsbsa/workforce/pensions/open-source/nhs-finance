package uk.nhs.nhsbsa.finance.employercontributions.service;

/**
 * Represents the different types of contribution
 *
 * Created by duncan on 31/03/2017.
 */
public enum ContributionType {
    /** employer contribution */
    R,

    /** employee contribution */
    E,

    /** employee added years */
    A,

    /** additional pension */
    D,

    /** ERRBO */
    B
}
