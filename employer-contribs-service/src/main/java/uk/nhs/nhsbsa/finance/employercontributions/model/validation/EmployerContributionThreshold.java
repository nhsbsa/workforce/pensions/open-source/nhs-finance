package uk.nhs.nhsbsa.finance.employercontributions.model.validation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;

/**
 * Created by Mark Lishman on 26/08/2016.
 */

@Target({TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = EmployerContributionThresholdValidator.class)
@Documented
@ReportAsSingleViolation
public @interface EmployerContributionThreshold {

  String message() default "{employer.contribution.threshold}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}
