package uk.nhs.nhsbsa.finance.employercontributions.config;

import com.nhsbsa.filters.UserPropagationFilter;
import com.nhsbsa.interceptors.LoggingInterceptor;
import com.nhsbsa.interceptors.UserPropagationInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@Import({LoggingInterceptor.class, UserPropagationFilter.class})
@Profile("user-propagation")
public class UserPropagationConfig extends WebMvcConfigurerAdapter {


  @Value("${rest.template.connect.timeout:0}")
  private Integer connectTimeout;

  @Value("${rest.template.read.timeout:0}")
  private Integer readTimeout;


  @Bean("ficRestTemplate")
  public RestTemplate restTemplate(RestTemplateBuilder builder) {
    return builder
        .interceptors(userPropagationInterceptor())
        .setConnectTimeout(connectTimeout)
        .setReadTimeout(readTimeout).build();
  }

  @Bean
  public UserPropagationInterceptor userPropagationInterceptor() {
    return new UserPropagationInterceptor();
  }
}