package uk.nhs.nhsbsa.finance.employercontributions.view;

import com.nhsbsa.view.RequestForTransferView;
import uk.nhs.nhsbsa.finance.employercontributions.model.RequestForTransfer;

/**
 * Created by Mark Lishman on 31/10/2016. RequestForTransfer
 */


public class RequestForTransferViewConverter {

  private RequestForTransferViewConverter(){}

  public static RequestForTransfer fromView(RequestForTransferView view){

    RequestForTransfer rft = RequestForTransfer.builder()
        .id(view.getId())
        .employerContributions(view.getEmployerContributions())
        .employeeContributions(view.getEmployeeContributions())
        .employeeAddedYears(view.getEmployeeAddedYears())
        .totalPensionablePay(view.getTotalPensionablePay())
        .transferDate(view.getTransferDate())
        .totalDebitAmount(view.getTotalDebitAmount())
        .adjustmentList(view.getAdjustmentList())
        .contributionDate(view.getContributionDate())
        .additionalPension(view.getAdditionalPension())
        .errbo(view.getErrbo())
        .isGp(view.getIsGp())
        .submitDate(view.getSubmitDate())
        .adjustmentsRequired(view.getAdjustmentsRequired())
        .rftUuid(view.getRftUuid())
        .eaCode(view.getEaCode())
        .csvProcessedDate(view.getCsvProcessedDate())
        .receiveDate(view.getReceiveDate())
        .payAsSoonAsPossible(view.getPayAsSoonAsPossible())
        .build();

      rft.setCreatedDate(view.getCreatedDate());
      rft.setCreatedBy(view.getCreatedBy());
    return rft;
  }

  public static RequestForTransferView toView(RequestForTransfer rft){
    return RequestForTransferView.builder()
        .id(rft.getId())
        .employerContributions(rft.getEmployerContributions())
        .employeeContributions(rft.getEmployeeContributions())
        .employeeAddedYears(rft.getEmployeeAddedYears())
        .totalPensionablePay(rft.getTotalPensionablePay())
        .transferDate(rft.getTransferDate())
        .totalDebitAmount(rft.getTotalDebitAmount())
        .adjustmentList(rft.getAdjustmentList())
        .contributionDate(rft.getContributionDate())
        .additionalPension(rft.getAdditionalPension())
        .errbo(rft.getErrbo())
        .isGp(rft.getIsGp())
        .submitDate(rft.getSubmitDate())
        .adjustmentsRequired(rft.getAdjustmentsRequired())
        .rftUuid(rft.getRftUuid())
        .eaCode(rft.getEaCode())
        .csvProcessedDate(rft.getCsvProcessedDate())
        .receiveDate(rft.getReceiveDate())
        .createdDate(rft.getCreatedDate())
        .createdBy(rft.getCreatedBy())
        .payAsSoonAsPossible(rft.getPayAsSoonAsPossible())
        .build();
  }
}