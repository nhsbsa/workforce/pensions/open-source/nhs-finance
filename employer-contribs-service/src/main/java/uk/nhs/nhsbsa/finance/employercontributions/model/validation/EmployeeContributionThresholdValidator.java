package uk.nhs.nhsbsa.finance.employercontributions.model.validation;

import java.math.BigDecimal;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import uk.nhs.nhsbsa.finance.employercontributions.model.RequestForTransfer;

/**
 * Created by Mark Lishman on 18/11/2016.
 */
public class EmployeeContributionThresholdValidator implements
    ConstraintValidator<EmployeeContributionThreshold, RequestForTransfer> {

  private static final BigDecimal MINIMUM_PERCENT = new BigDecimal("0.05");
  private static final BigDecimal MAXIMUM_PERCENT = new BigDecimal("0.145");

  @Override
  public void initialize(EmployeeContributionThreshold constraint) {
    // No implementation needed. Nothing to initialise.
  }

  @Override
  public boolean isValid(RequestForTransfer rft, ConstraintValidatorContext context) {
    if (rft.getTotalPensionablePay() == null || rft.getEmployeeContributions() == null) {
      return true;
    }
    final BigDecimal minimumValue = rft.getTotalPensionablePay().multiply(MINIMUM_PERCENT);
    final BigDecimal maximumValue = rft.getTotalPensionablePay().multiply(MAXIMUM_PERCENT);

    boolean isValid = rft.getEmployeeContributions().compareTo(minimumValue) >= 0
            && rft.getEmployeeContributions().compareTo(maximumValue) <= 0;
    if (!isValid) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate()).addPropertyNode("employeeContributions").addConstraintViolation();
    }

    return isValid;
  }
}
