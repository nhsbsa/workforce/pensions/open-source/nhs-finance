package uk.nhs.nhsbsa.finance.employercontributions.config;

import com.google.common.base.Predicate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Configure Swagger to document endpoints.
 *
 */
@ConditionalOnProperty(prefix = "swagger.enable", name = "dynamic", matchIfMissing = true)
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${swagger.all}")
    boolean swaggerShowAll = false;

    @Bean
    public Docket api() {
        final Predicate<RequestHandler> apisPredicate = apisPredicate();
        final Predicate<String> pathsPredicate = PathSelectors.any();
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(apisPredicate)
                .paths(pathsPredicate)
                .build();
    }

    private Predicate<RequestHandler> apisPredicate() {
        if (swaggerShowAll) {
            return RequestHandlerSelectors.any();
        }
        return RequestHandlerSelectors.basePackage("uk.nhs.nhsbsa.finance.employercontributions.controller");
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Finance IC Employer Contributions Service")
                .description("NHS Employer Contributions RESTful service")
                .termsOfServiceUrl("*REFERENCE TERMS OF SERVICE HERE*")
                .license("*NAME LICENSE HERE*")
                .licenseUrl("*REFERENCE LICENSE HERE*")
                .version("1.0-beta")
                .build();
    }
}