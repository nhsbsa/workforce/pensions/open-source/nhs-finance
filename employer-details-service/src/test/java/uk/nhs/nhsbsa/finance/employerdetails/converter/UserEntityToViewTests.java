package uk.nhs.nhsbsa.finance.employerdetails.converter;

import java.util.Collections;
import java.util.List;
import org.junit.Test;
import org.mockito.Mock;
import uk.nhs.nhsbsa.finance.employerdetails.entity.FinanceUserEntity;
import uk.nhs.nhsbsa.finance.employerdetails.entity.OrganisationEntity;
import uk.nhs.nhsbsa.finance.employerdetails.view.UserView;

import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.ValidatorFactory;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * Unit tests for finance user entity --> user view converter
 *
 * Created by duncan on 17/03/2017.
 */
public class UserEntityToViewTests {

    private UserEntityToView converter;

    @Mock
    private OrganisationEntityToView orgConverter;

    public UserEntityToViewTests() {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        this.orgConverter = mock(OrganisationEntityToView.class);
        this.converter = new UserEntityToView(vf.getValidator(), orgConverter);
    }

    @Test
    public void given_null_user_entity_then_return_null() {

        UserView view = converter.convert(null);

        assertThat(view).isNull();
    }

    @Test
    public void given_valid_user_entity_when_converted_then_view_generated() {

        final OrganisationEntity orgEntity = OrganisationEntity.builder().uuid(UUID.randomUUID().toString()).name("").eas(Collections.emptyList()).build();
        final List<OrganisationEntity> orgList = Collections.singletonList(orgEntity);
        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress("t.tester@example.com")
                .telephoneNumber("55544315827")
                .organisations(orgList)
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    /// First name - not blank, max length 200

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_first_name_missing_then_validation_exception_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .lastName("Tester")
                .emailAddress("t.tester@example.com")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_first_name_empty_then_validation_exception_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("")
                .lastName("Tester")
                .emailAddress("t.tester@example.com")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_user_entity_when_first_name_199_long_then_validation_ok() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName(makeStringOfLength(199))
                .lastName("Tester")
                .emailAddress("t.tester@example.com")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_user_entity_when_first_name_200_long_then_validation_ok() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName(makeStringOfLength(200))
                .lastName("Tester")
                .emailAddress("t.tester@example.com")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_first_name_201_long_then_validation_exception_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName(makeStringOfLength(201))
                .lastName("Tester")
                .emailAddress("t.tester@example.com")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    /// Last name - not blank, max length 200

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_last_name_missing_then_validation_exception_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .emailAddress("t.tester@example.com")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_last_name_empty_then_validation_exception_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("")
                .emailAddress("t.tester@example.com")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_user_entity_when_last_name_199_long_then_validation_ok() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName(makeStringOfLength(199))
                .emailAddress("t.tester@example.com")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_user_entity_when_last_name_200_long_then_validation_ok() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName(makeStringOfLength(200))
                .emailAddress("t.tester@example.com")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_last_name_201_long_then_validation_exception_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName(makeStringOfLength(201))
                .emailAddress("t.tester@example.com")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    /// Telephone number - not blank, max length 200

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_telephone_number_missing_then_validation_exception_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress("t.tester@example.com")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_telephone_number_empty_then_validation_exception_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress("t.tester@example.com")
                .telephoneNumber("")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_user_entity_when_telephone_number_199_long_then_validation_ok() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress("t.tester@example.com")
                .telephoneNumber(makeStringOfLength(199))
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_user_entity_when_telephone_number_200_long_then_validation_ok() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress("t.tester@example.com")
                .telephoneNumber(makeStringOfLength(200))
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_telephone_number_201_long_then_validation_exception_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress("t.tester@example.com")
                .telephoneNumber(makeStringOfLength(201))
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }


    @Test
    public void given_valid_user_entity__with_empty_organisation_when_converted_then_view_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
            .uuid(UUID.randomUUID().toString())
            .firstName("Trevor")
            .lastName("Tester")
            .emailAddress("t.tester@example.com")
            .telephoneNumber("55544315827")
            .organisations(Collections.emptyList())
            .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    /// Email - not blank, max length 200, email

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_email_missing_then_validation_exception_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_email_blank_then_validation_exception_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress("")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_email_not_email_then_validation_exception_generated() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress("mary_tudor") // mary@tudor is a valid email
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_user_entity_when_email_username_64_long_then_validation_ok() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress(makeStringOfLength(64) + "@example.com") // username part has a maxium of 64 characters
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_email_username_65_long_then_validation_exception() {

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress(makeStringOfLength(65) + "@example.com")
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_user_entity_when_email_199_long_then_validation_ok() {
        // Each part appears to have a maximum length. using random parts of 50 chars seems to work.
        String address = makeStringOfLength(50) + "@" // 51
                + makeStringOfLength(50) + "." // 102
                + makeStringOfLength(50) + "." // 153
                + makeStringOfLength(199 - 153);

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress(address)
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_user_entity_when_email_200_long_then_validation_ok() {
        // Each part appears to have a maximum length. using random parts of 50 chars seems to work.
        String address = makeStringOfLength(50) + "@" // 51
                + makeStringOfLength(50) + "." // 102
                + makeStringOfLength(50) + "." // 153
                + makeStringOfLength(200 - 153);

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress(address)
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_user_entity_when_email_201_long_then_validation_exception_generated() {
        // Each part appears to have a maximum length. using random parts of 50 chars seems to work.
        String address = makeStringOfLength(50) + "@" // 51
                + makeStringOfLength(50) + "." // 102
                + makeStringOfLength(50) + "." // 153
                + makeStringOfLength(201 - 153);

        FinanceUserEntity entity = FinanceUserEntity.builder()
                .uuid(UUID.randomUUID().toString())
                .firstName("Trevor")
                .lastName("Tester")
                .emailAddress(address)
                .telephoneNumber("55544315827")
                .organisations(Collections.emptyList())
                .build();

        UserView view = converter.convert(entity);

        assertThat(view).isNotNull();
    }

    private String makeStringOfLength(int length) {
        String source = "abcdefghij";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i += 1) {
            builder.append(source.charAt(i % 10));
        }
        return builder.toString();
    }
}