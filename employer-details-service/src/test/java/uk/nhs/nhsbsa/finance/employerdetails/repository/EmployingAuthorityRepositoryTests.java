package uk.nhs.nhsbsa.finance.employerdetails.repository;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.nhsbsa.model.EmployerTypes;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.nhs.nhsbsa.finance.employerdetails.entity.EmployingAuthorityEntity;
import uk.nhs.nhsbsa.finance.employerdetails.entity.OrganisationEntity;

/**
 * Employing Authority repository integration tests
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class EmployingAuthorityRepositoryTests {

    private static Validator validator;

    private static EmployingAuthorityEntity EA = EmployingAuthorityEntity.builder().eaCode("EA99999").name("org name").employerType(EmployerTypes.STAFF.name()).build();

    @Before
    public void setUp(){
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void given_ea_pass_validation() {
        EmployingAuthorityEntity employingAuthorityEntity = EmployingAuthorityEntity.builder()
            .eaCode("EA99999")
            .name("org name")
            .employerType(EmployerTypes.STAFF.name())
            .build();


        Set<ConstraintViolation<EmployingAuthorityEntity>> validate = validator.validate(employingAuthorityEntity);
        assertThat(validate, is(empty()));
    }

    @Test
    public void given_null_eacode_fail_validation() {

        EmployingAuthorityEntity employingAuthorityEntity = EmployingAuthorityEntity.builder()
            .eaCode(null)
            .name("org name")
            .employerType(EmployerTypes.STAFF.name())
            .build();

        Set<ConstraintViolation<EmployingAuthorityEntity>> validate = validator.validate(employingAuthorityEntity);
        assertThat(validate.size(), is(1));
        assertThat(validate.iterator().next().getPropertyPath().toString(), is("eaCode"));
    }

    @Test
    public void given_null_employer_type_fail_validation() {

        EmployingAuthorityEntity employingAuthorityEntity = EmployingAuthorityEntity.builder()
            .eaCode("EA99999")
            .name("org name")
            .employerType(null)
            .build();

        Set<ConstraintViolation<EmployingAuthorityEntity>> validate = validator.validate(employingAuthorityEntity);
        assertThat(validate.size(), is(1));
        assertThat(validate.iterator().next().getPropertyPath().toString(), is("employerType"));
    }

}
