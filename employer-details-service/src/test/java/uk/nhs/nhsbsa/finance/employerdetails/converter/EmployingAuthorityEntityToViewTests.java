package uk.nhs.nhsbsa.finance.employerdetails.converter;

import static org.assertj.core.api.Assertions.assertThat;

import com.nhsbsa.model.EmployerTypes;
import com.nhsbsa.view.EmployingAuthorityView;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.ValidatorFactory;
import org.junit.Test;
import uk.nhs.nhsbsa.finance.employerdetails.entity.EmployingAuthorityEntity;

/**
 * Unit tests for organisation entity --> view converter
 *
 */
public class EmployingAuthorityEntityToViewTests {

    private EmployingAuthorityEntityToView converter;

    private final String EA_CODE = "EA123";
    private final String EA_NAME = "EA Name";

    public EmployingAuthorityEntityToViewTests() {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        this.converter = new EmployingAuthorityEntityToView(vf.getValidator());
    }

    @Test
    public void given_valid_ea_entity_when_converted_then_view_generated() {

        final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
            .eaCode(EA_CODE)
            .name(EA_NAME)
            .employerType(EmployerTypes.STAFF.name())
            .build();

        EmployingAuthorityView view = converter.convert(eaEntity);


        assertThat(view).isNotNull();
    }

    /// EA code - not blank, max length 50

    @Test(expected = ValidationException.class)
    public void given_org_entity_when_eacode_empty_then_validation_exception() {

        final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
            .eaCode("")
            .name(EA_NAME)
            .employerType(EmployerTypes.STAFF.name())
            .build();

        EmployingAuthorityView view = converter.convert(eaEntity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_org_entity_when_eacode_49_len_then_validation_ok() {

        final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
            .eaCode(makeStringOfLength(49))
            .name(EA_NAME)
            .employerType(EmployerTypes.STAFF.name())
            .build();

        EmployingAuthorityView view = converter.convert(eaEntity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_org_entity_when_eacode_50_len_then_validation_ok() {

        final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
            .eaCode(makeStringOfLength(50))
            .name(EA_NAME)
            .employerType(EmployerTypes.STAFF.name())
            .build();

        EmployingAuthorityView view = converter.convert(eaEntity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_org_entity_when_eacode_51_len_then_validation_exception() {

        final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
            .eaCode(makeStringOfLength(51))
            .name(EA_NAME)
            .employerType(EmployerTypes.STAFF.name())
            .build();

        EmployingAuthorityView view = converter.convert(eaEntity);

        assertThat(view).isNotNull();
    }

    /// Org name - not blank, max length 1000

    @Test(expected = ValidationException.class)
    public void given_org_entity_when_employer_type_missing_then_validation_exception() {

        final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
            .eaCode(EA_CODE)
            .name(EA_NAME)
            .employerType("")
            .build();

        EmployingAuthorityView view = converter.convert(eaEntity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_org_entity_when_name_999_len_then_validation_ok() {

        final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
            .eaCode(EA_CODE)
            .name(makeStringOfLength(999))
            .employerType(EmployerTypes.STAFF.name())
            .build();

        EmployingAuthorityView view = converter.convert(eaEntity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_org_entity_when_name_1000_len_then_validation_ok() {

        final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
            .eaCode(EA_CODE)
            .name(makeStringOfLength(1000))
            .employerType(EmployerTypes.STAFF.name())
            .build();

        EmployingAuthorityView view = converter.convert(eaEntity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_org_entity_when_name_1001_len_then_validation_exception() {

        final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
            .eaCode(EA_CODE)
            .name(makeStringOfLength(1001))
            .employerType(EmployerTypes.STAFF.name())
            .build();

        EmployingAuthorityView view = converter.convert(eaEntity);

        assertThat(view).isNotNull();
    }


    private String makeStringOfLength(int length) {
        String source = "abcdefghij";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i += 1) {
            builder.append(source.charAt(i % 10));
        }
        return builder.toString();
    }
}
