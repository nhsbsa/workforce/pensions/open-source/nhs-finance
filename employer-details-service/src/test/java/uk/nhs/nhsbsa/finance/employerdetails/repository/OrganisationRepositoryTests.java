package uk.nhs.nhsbsa.finance.employerdetails.repository;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.nhsbsa.model.EmployerTypes;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.nhs.nhsbsa.finance.employerdetails.entity.EmployingAuthorityEntity;
import uk.nhs.nhsbsa.finance.employerdetails.entity.OrganisationEntity;

/**
 * Organisation repository integration tests
 *
 * Created by marklishman on 21/03/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class OrganisationRepositoryTests {

    private static Validator validator;

    private static EmployingAuthorityEntity EA = EmployingAuthorityEntity.builder().eaCode("EA99999").name("org name").employerType(EmployerTypes.STAFF.name()).build();
    private static List<EmployingAuthorityEntity> EA_LIST = Collections.singletonList(EA);

    @Before
    public void setUp(){
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void given_organisation_and_account_pass_validation() {
        final String uuid = UUID.randomUUID().toString();
        OrganisationEntity organisationEntity = OrganisationEntity.builder()
                .eas(EA_LIST)
                .uuid(uuid)
                .name("org name")
                .build();


        Set<ConstraintViolation<OrganisationEntity>> validate = validator.validate(organisationEntity);
        assertThat(validate, is(empty()));
    }

    @Test
    public void given_missing_uuid_fail_validation() {

        OrganisationEntity organisationEntity = OrganisationEntity.builder()
            .eas(EA_LIST)
            .name("org name")
            .build();

        Set<ConstraintViolation<OrganisationEntity>> validate = validator.validate(organisationEntity);
        assertThat(validate.size(), is(1));
        assertThat(validate.iterator().next().getPropertyPath().toString(), is("uuid"));
    }

    @Test
    public void given_missing_accountName_fail_validation() {

        OrganisationEntity organisationEntity = OrganisationEntity.builder()
            .eas(EA_LIST)
            .uuid(UUID.randomUUID().toString())
            .build();

        Set<ConstraintViolation<OrganisationEntity>> validate = validator.validate(organisationEntity);
        assertThat(validate.size(), is(1));
        assertThat(validate.iterator().next().getPropertyPath().toString(), is("name"));
    }

}
