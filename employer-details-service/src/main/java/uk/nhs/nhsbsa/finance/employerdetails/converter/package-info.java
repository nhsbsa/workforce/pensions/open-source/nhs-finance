/**
 * Classes that convert between types used by the service.
 *
 * Created by duncan on 17/03/2017.
 */
package uk.nhs.nhsbsa.finance.employerdetails.converter;