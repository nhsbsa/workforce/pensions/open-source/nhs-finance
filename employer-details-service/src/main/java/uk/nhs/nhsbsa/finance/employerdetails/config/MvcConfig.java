package uk.nhs.nhsbsa.finance.employerdetails.config;

import com.nhsbsa.interceptors.LoggingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@Import(LoggingInterceptor.class)
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private LoggingInterceptor loggingInterceptor;

    @Bean("ficRestTemplate")
    @Profile("!user-propagation")
    public RestTemplate restTemplate() {

        return new RestTemplate();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(loggingInterceptor).addPathPatterns("/**");
    }
}
