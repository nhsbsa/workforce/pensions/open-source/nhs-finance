package uk.nhs.nhsbsa.finance.employerdetails.config;

import com.nhsbsa.filters.UserPropagationFilter;
import com.nhsbsa.interceptors.LoggingInterceptor;
import com.nhsbsa.interceptors.UserPropagationInterceptor;
import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@Import({LoggingInterceptor.class, UserPropagationFilter.class})
@Profile("user-propagation")
public class UserPropagationConfig extends WebMvcConfigurerAdapter {

  @Bean
  public UserPropagationInterceptor userPropagationInterceptor() {
    return new UserPropagationInterceptor();
  }

  @Bean("ficRestTemplate")
  public RestTemplate restTemplate() {
    final RestTemplate restTemplate = new RestTemplate();
    restTemplate.setInterceptors(Collections.singletonList(userPropagationInterceptor()));
    return restTemplate;
  }
}