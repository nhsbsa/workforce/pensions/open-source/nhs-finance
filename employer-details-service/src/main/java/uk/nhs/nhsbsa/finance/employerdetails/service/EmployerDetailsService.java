package uk.nhs.nhsbsa.finance.employerdetails.service;

import com.nhsbsa.security.CreateEaRequest;
import com.nhsbsa.security.CreateOrganisationRequest;
import com.nhsbsa.security.CreateOrganisationResponse;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import uk.nhs.nhsbsa.finance.employerdetails.converter.OrganisationEntityToView;
import uk.nhs.nhsbsa.finance.employerdetails.converter.UserEntityToView;
import uk.nhs.nhsbsa.finance.employerdetails.entity.EmployingAuthorityEntity;
import uk.nhs.nhsbsa.finance.employerdetails.entity.FinanceUserEntity;
import uk.nhs.nhsbsa.finance.employerdetails.entity.OrganisationEntity;
import uk.nhs.nhsbsa.finance.employerdetails.repository.EmployingAuthorityRepository;
import uk.nhs.nhsbsa.finance.employerdetails.repository.FinanceUserRepository;
import uk.nhs.nhsbsa.finance.employerdetails.repository.OrganisationRepository;
import com.nhsbsa.view.OrganisationView;
import uk.nhs.nhsbsa.finance.employerdetails.view.UserView;

/**
 * Spring service implementation for Employer Details RESTful API service
 *
 * Created by duncan on 14/03/2017.
 */

@Slf4j
@Service
public class EmployerDetailsService {

  private OrganisationRepository organisationRepository;

  private FinanceUserRepository userRepository;

  private EmployingAuthorityRepository employingAuthorityRepository;

  private OrganisationEntityToView organisationEntityToView;
  private UserEntityToView userEntityToView;

  @Autowired
  public EmployerDetailsService(
      OrganisationRepository organisationRepository,
      FinanceUserRepository userRepository,
      EmployingAuthorityRepository employingAuthorityRepository,
      OrganisationEntityToView organisationEntityToView,
      UserEntityToView userEntityToView
  ) {
    this.organisationRepository = organisationRepository;
    this.userRepository = userRepository;
    this.employingAuthorityRepository = employingAuthorityRepository;
    this.organisationEntityToView = organisationEntityToView;
    this.userEntityToView = userEntityToView;
  }

  @Transactional(readOnly = true)
  public Optional<OrganisationView> findOrganisation(final UUID id) {
    // PRECONDITION - must be signed in as a user able to view the referenced EA
    // that means either a member of that EA or an administrator
    OrganisationEntity entity = organisationRepository.findByUuid(id.toString());
    OrganisationView view = organisationEntityToView.convert(entity);
    return Optional.ofNullable(view);
  }

  @Transactional(readOnly = true)
  public Optional<OrganisationView> findOrganisationByEaCode(final String eaCode) {
    // PRECONDITION - must be signed in as a user able to view the referenced EA
    // that means either a member of that EA or an administrator
    List<OrganisationEntity> entity = organisationRepository.findByEaCode(eaCode);

    if(!entity.isEmpty()) {
      OrganisationView view = organisationEntityToView.convert(entity.get(0));
      return Optional.ofNullable(view);
    }
    return Optional.empty();
  }

  /**
   * @param toStore Organisation details to persist
   * @return Organisation as persisted;
   * @throws IllegalArgumentException if {@code toStore}
   */
  @Transactional
  public OrganisationView storeOrganisation(CreateOrganisationRequest toStore) {

    EmployingAuthorityEntity eaEntity = employingAuthorityRepository
        .findByEaCode(toStore.getEas().get(0).getEaCode());

    Assert.isNull(eaEntity, "Attempt to store EA that already exists");

    EmployingAuthorityEntity employingAuthorityEntity = EmployingAuthorityEntity.builder()
        .eaCode(toStore.getEas().get(0).getEaCode())
        .name(toStore.getEas().get(0).getName())
        .employerType(toStore.getEas().get(0).getEmployerType())
        .build();

    EmployingAuthorityEntity savedEa = employingAuthorityRepository.save(employingAuthorityEntity);
    List<EmployingAuthorityEntity> savedEaList = Collections.singletonList(savedEa);

    OrganisationEntity organisationEntity = OrganisationEntity.builder()
        .name(toStore.getName())
        .eas(savedEaList)
        .uuid(UUID.randomUUID().toString())
        .build();

    OrganisationEntity registeredOrganisation = organisationRepository.save(organisationEntity);


    return organisationEntityToView.convert(registeredOrganisation);
  }

  @Transactional(readOnly = true)
  public Optional<UserView> findUser(final UUID id) {
    // PRECONDITION - must be signed in as a user able to view the referenced EA
    // that means either a member of that EA or an administrator
    FinanceUserEntity entity = userRepository.findByUuid(id.toString());
    UserView view = userEntityToView.convert(entity);
    return Optional.ofNullable(view);
  }

  @Transactional(readOnly = true)
  public Optional<List<UserView>> findUserByOrgUuid(final UUID orgUuid) {
    // Find List of Users with same orgUuid
    // that means either a member of that EA or an administrator
    List<FinanceUserEntity> entity = userRepository.findByOrgUuid(orgUuid.toString());

    return Optional.ofNullable(entity.stream().map(userEntityToView::convert)
        .collect(Collectors.toList()));
  }

  /**
   * @param toStore data to be persisted
   * @return reflection of the data stored; in particular this will have a populated unique
   * identifier by which the user can be referenced
   * @throws IllegalArgumentException if {@code toStore} has a populated identifier or if the
   * specified organisation does not exist
   */
  @Transactional
  public UserView storeUser(UserView toStore) {
    //TODO Change getOrganisation().get(0) once we know how to handle multiple orgs
    OrganisationEntity organisationEntity = organisationRepository
        .findByUuid(toStore.getOrganisations().get(0).getIdentifier().toString());
    Assert
        .notNull(organisationEntity, "Attempt to store User for Organisation that does not exist");

    UserView fresh = toStore.withIdentifier(
        toStore.getIdentifier() != null ? toStore.getIdentifier() : UUID.randomUUID());

    List<OrganisationEntity> organisationEntityList = new ArrayList<>();
    organisationEntityList.add(organisationEntity);

    FinanceUserEntity userEntity = FinanceUserEntity.builder()
        .emailAddress(fresh.getEmailAddress())
        .firstName(fresh.getFirstName())
        .lastName(fresh.getLastName())
        .organisations(organisationEntityList)
        .telephoneNumber(fresh.getContactNumber())
        .uuid(fresh.getIdentifier().toString())
        .build();

    userRepository.save(userEntity);
    return fresh;
  }

  @Transactional
  public CreateUserResponse storeUser(CreateUserRequest toStore) {

    //TODO Change getOrganisation().get(0) once we know how to handle multiple orgs
    OrganisationEntity organisationEntity = organisationRepository
        .findByUuid(toStore.getOrganisations().get(0).getIdentifier().toString());
    List<OrganisationEntity> organisationEntityList = new ArrayList<>();
    organisationEntityList.add(organisationEntity);
    Assert
        .notNull(organisationEntity, "Attempt to store User for Organisation that does not exist");

    FinanceUserEntity userEntity = FinanceUserEntity.builder()
        .emailAddress(toStore.getUsername())
        .firstName(toStore.getFirstName())
        .lastName(toStore.getSurname())
        .organisations(organisationEntityList)
        .telephoneNumber(toStore.getContactNumber())
        .uuid(toStore.getUuid())
        .build();

    try {
      FinanceUserEntity user = userRepository.save(userEntity);

      return getRegistrationResponse(user);
    } catch (Exception e) {
       log.error("Error when trying to save User");
    }

    return null;
  }

  private CreateUserResponse getRegistrationResponse(FinanceUserEntity validUser) {
    return CreateUserResponse
        .builder()
        .uuid(validUser.getUuid())
        .email(validUser.getEmailAddress())
        .build();
  }

  @Transactional(readOnly = true)
  public Optional<String> findEmployerTypeByEaCode(final String eaCode) {

    String entity = employingAuthorityRepository.findEmployerTypeByEaCode(eaCode);

    return Optional.ofNullable(entity);
  }

  @Transactional
  public Optional<CreateOrganisationResponse> storeOrganisationWithoutEa(CreateOrganisationRequest toStore) {

    OrganisationEntity organisationEntity = OrganisationEntity.builder()
        .name(toStore.getName())
        .uuid(UUID.randomUUID().toString())
        .build();

    OrganisationEntity registeredOrganisation = organisationRepository.save(organisationEntity);


    return Optional.of(CreateOrganisationResponse
        .builder()
        .uuid(registeredOrganisation.getUuid())
        .name(registeredOrganisation.getName())
        .build());
  }

  @Transactional
  public Optional<OrganisationView> storeEa(CreateEaRequest toStore) {

    EmployingAuthorityEntity eaEntity = employingAuthorityRepository
        .findByEaCode(toStore.getEaCode());

    Assert.isNull(eaEntity, "Attempt to store EA that already exists");

    EmployingAuthorityEntity eaRequest = EmployingAuthorityEntity.builder()
        .name(toStore.getEaName())
        .eaCode(toStore.getEaCode())
        .employerType(toStore.getEmployerType())
        .build();
    EmployingAuthorityEntity savedEa = employingAuthorityRepository.save(eaRequest);

    // Get current Organisation
    OrganisationEntity savedOrganisation = organisationRepository.findByUuid(toStore.getOrganisationUuid());

    savedOrganisation.getEas().add(savedEa);

    // Save Organisation with new Ea
    OrganisationEntity registeredOrganisation = organisationRepository.save(savedOrganisation);

    return Optional.of(organisationEntityToView.convert(registeredOrganisation));
  }

  @Transactional(readOnly = true)
  public Optional<String> findOrganisationByName(final String orgName) {

    Optional<OrganisationEntity> entity = Optional.ofNullable(organisationRepository.findByNameIgnoreCaseContaining(orgName));

    return entity.map(OrganisationEntity::getName);
  }

  @Transactional(readOnly = true)
  public Optional<String> findEaByCode(final String eaCode) {

    Optional<EmployingAuthorityEntity> entity = Optional.ofNullable(employingAuthorityRepository.findByEaCode(eaCode));

    return entity.map(EmployingAuthorityEntity::getEaCode);
  }
}