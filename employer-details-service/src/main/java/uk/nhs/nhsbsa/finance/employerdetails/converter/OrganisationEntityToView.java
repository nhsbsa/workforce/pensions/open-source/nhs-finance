package uk.nhs.nhsbsa.finance.employerdetails.converter;

import java.util.List;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import uk.nhs.nhsbsa.finance.employerdetails.entity.OrganisationEntity;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;

import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Convert a {@code OrganisationEntity} to an {@code OrganisationView}. The view
 * will have its members validated prior to returning. If the validation
 * fails, a ValidationException will be thrown.
 *
 * Created by duncan on 17/03/2017.
 */
@Component
public class OrganisationEntityToView implements Converter<OrganisationEntity, OrganisationView> {

    private Validator validator;

    private EmployingAuthorityEntityToView eaConverter;

    public OrganisationEntityToView(Validator validator,
        EmployingAuthorityEntityToView employingAuthorityEntityToView) {
        this.validator = validator;
        this.eaConverter = employingAuthorityEntityToView;
    }

    /**
     *
     * @param source Entity to convert
     * @return View from Entity
     * @throws ValidationException if the entity contains values that could
     * not be validated when used as a member in the view.
     */
    @Override
    public OrganisationView convert(OrganisationEntity source) {
        // Spring guarantees not to call this method with null, but it's not being
        // called by Spring
        if (source == null) {
            return null;
        }

        List<EmployingAuthorityView> eas = source.getEas().stream().map(eaConverter::convert)
            .collect(Collectors.toList());

        OrganisationView view = OrganisationView.builder()
                .name(source.getName())
                .eas(eas)
                .identifier(UUID.fromString(source.getUuid()))
                .build();
        return validateView(view);
    }

    private OrganisationView validateView(OrganisationView view) {
        Set<ConstraintViolation<OrganisationView>> violations = validator.validate(view);
        if (violations.isEmpty()) {
            return view;
        }
        String message = violations.stream()
                .map(v -> v.getPropertyPath() + ": " + v.getMessage())
                .collect(Collectors.joining(","));
        throw new ValidationException("Error validating Organisation - " + message);
    }

}