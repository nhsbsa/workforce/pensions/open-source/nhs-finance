package uk.nhs.nhsbsa.finance.employerdetails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployerDetailsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployerDetailsApplication.class, args);
	}

}
