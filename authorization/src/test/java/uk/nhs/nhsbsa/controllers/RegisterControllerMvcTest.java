package uk.nhs.nhsbsa.controllers;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.nhs.nhsbsa.rest.RestAuthentication.AUTH_APP_ID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nhsbsa.model.User;
import com.nhsbsa.model.UserRoles;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.RegistrationRequest;
import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import uk.nhs.nhsbsa.security.IdentityRequest;
import uk.nhs.nhsbsa.security.RegistrationWithIdentityRequest;
import uk.nhs.nhsbsa.service.UserRegistrationService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RegisterControllerMvcTest {

    private static final String REGISTER_URL = "/register";

    private static final String APPLICATION_ID = "application-id";
    private static final String USER_NAME = "user-name";
    private static final String PASSWORD = "password";
    private static final String USER_UUID = "user-uuid";
    private static final String NINO = "AB11223344C";
    private static final long SD_NUMBER = 12345;
    private static final LocalDate DOB = LocalDate.of(1950, 10, 11);

    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Mock private UserRegistrationService userRegistrationService;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders
                .standaloneSetup(new RegisterController(userRegistrationService))
                .build();
    }

    @Test
    public void userIsRegistered() throws Exception {

        given(userRegistrationService.register(eq(APPLICATION_ID), anyObject())).willReturn(user());

        mvc.perform(MockMvcRequestBuilders.post(REGISTER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTH_APP_ID, APPLICATION_ID)
                .content(validRegisterRequestAsJson()))
                .andExpect(status().isOk())
                .andExpect(content().string(is(equalTo(successResponse()))));
    }

    @Test
    public void userIsNotRegisteredIfRequestDataIsInvalid() throws Exception {

        final String content = objectMapper.writeValueAsString(
                RegistrationRequest
                        .builder()
                        .username(USER_NAME)
                        .build()
        );

        mvc.perform(MockMvcRequestBuilders.post(REGISTER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTH_APP_ID, APPLICATION_ID)
                .content(content))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void userIsNotRegisteredIfAppIdIsMissing() throws Exception {

        mvc.perform(MockMvcRequestBuilders.post(REGISTER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(validRegisterRequestAsJson()))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void userIsNotRegisteredIfDataCannotBeSaved() throws Exception {

        given(userRegistrationService.register(eq(APPLICATION_ID), anyObject())).willReturn(null);

        mvc.perform(MockMvcRequestBuilders.post(REGISTER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTH_APP_ID, APPLICATION_ID)
                .content(validRegisterRequestAsJson()))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(isEmptyString()));
    }

    private User user() {
        return User
                .builder()
                .uuid(USER_UUID)
                .build();
    }

    private String validRegisterRequestAsJson() throws JsonProcessingException {
        return objectMapper.writeValueAsString(validRegisterRequest());
    }

    private RegistrationRequest validRegisterRequest() throws JsonProcessingException {
        return RegistrationRequest
                        .builder()
                        .username(USER_NAME)
                        .password(PASSWORD)
                        .role(UserRoles.ROLE_STANDARD.name())
                        .build();
    }

    private IdentityRequest validIdentityRequest() {
        return IdentityRequest
                        .builder()
                        .sdNumber(SD_NUMBER)
                        .nino(NINO)
                        .dateOfBirth(DOB)
                        .build();
    }

    private String successResponse() throws JsonProcessingException {
        return objectMapper.writeValueAsString(AuthenticationResponse
                .builder()
                .uuid(USER_UUID)
                .token(RegisterController.OAUTH2_TOKEN)
                .build());
    }
}