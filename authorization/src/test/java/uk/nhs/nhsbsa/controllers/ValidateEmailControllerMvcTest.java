package uk.nhs.nhsbsa.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nhsbsa.model.ForgottenPassword;
import com.nhsbsa.model.User;
import com.nhsbsa.security.ResetEmailTokenResponse;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import uk.nhs.nhsbsa.model.ResetEmailToken;
import uk.nhs.nhsbsa.security.*;
import uk.nhs.nhsbsa.service.UserAuthenticationService;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyObject;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.nhs.nhsbsa.rest.RestAuthentication.AUTH_APP_ID;

/**
 * Created by nataliehulse on 30/11/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ValidateEmailControllerMvcTest {

    private static final String APPLICATION_ID = "application-id";
    private static final String VALIDATE_EMAIL_URL = "/authenticate/validate-email";
    private static final String VERIFY_TOKEN_URL = "/authenticate/verify-token";
    private static final String DELETE_TOKEN_URL = "/authenticate/delete-token";
    private static final String USERNAME = "user@email.com";
    private static final String UUID = "1";
    private static final String TOKEN = "123uuid";

    private MockMvc mvc;

    @Mock
    private UserAuthenticationService userAuthenticationService;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders
                .standaloneSetup(new ValidateEmailController(userAuthenticationService))
                .build();
    }

    @Test
    public void emailIsAuthenticated() throws Exception {

        final User validUser = User
                .builder()
                .name(USERNAME)
                .uuid(UUID)
                .build();

        given(userAuthenticationService.retrieveUserByValidEmail(anyObject(),anyObject())).willReturn(validUser);

        final ResetEmailToken resetEmailToken = ResetEmailToken
                .builder()
                .token(TOKEN)
                .userId(1L)
                .build();

        given(userAuthenticationService.generateEmailToken(anyObject())).willReturn(resetEmailToken);


        mvc.perform(MockMvcRequestBuilders.post(VALIDATE_EMAIL_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTH_APP_ID, APPLICATION_ID)
                .content(validUserSaveRequestAsJson()))
                .andExpect(status().isOk())
                .andExpect(content().string(is(equalTo(successResponseWithSaveUserCode()))));
    }

    @Test
    public void emailIsNotAuthenticated() throws Exception {


        given(userAuthenticationService.retrieveUserByValidEmail(anyObject(),anyObject())).willReturn(null);

        mvc.perform(MockMvcRequestBuilders.post(VALIDATE_EMAIL_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(validUserSaveRequestAsJson()))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void userIsDeletedSoEmailIsNotAuthenticated() throws Exception {

        LocalDate date = LocalDate.of(2018,10,1);
        Date deletedOn = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        final User deletedUser = User
            .builder()
            .name(USERNAME)
            .uuid(UUID)
            .deletedOn(deletedOn)
            .build();

        given(userAuthenticationService.retrieveUserByValidEmail(anyObject(),anyObject())).willReturn(deletedUser);

        mvc.perform(MockMvcRequestBuilders.post(VALIDATE_EMAIL_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(validUserSaveRequestAsJson()))
            .andExpect(status().isBadRequest())
            .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void tokenIsVerified() throws Exception {


        given(userAuthenticationService.validateEmailToken(anyObject())).willReturn(UUID);

        mvc.perform(MockMvcRequestBuilders.post(VERIFY_TOKEN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTH_APP_ID, APPLICATION_ID)
                .content(TOKEN))
                .andExpect(status().isOk())
                .andExpect(content().string(is(equalTo(successTokenResponse()))));
    }

    @Test
    public void tokenIsNotAuthenticated() throws Exception {

        given(userAuthenticationService.validateEmailToken(anyObject())).willReturn(null);

        mvc.perform(MockMvcRequestBuilders.post(VERIFY_TOKEN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TOKEN))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void tokenIsDeleted() throws Exception {


        given(userAuthenticationService.deleteEmailToken(anyObject())).willReturn(true);

        mvc.perform(MockMvcRequestBuilders.post(DELETE_TOKEN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TOKEN))
                .andExpect(status().isOk())
                .andExpect(content().string(is(equalTo("true"))));
    }

    @Test
    public void tokenIsNotDeleted() throws Exception {


        given(userAuthenticationService.deleteEmailToken(anyObject())).willReturn(false);

        mvc.perform(MockMvcRequestBuilders.post(DELETE_TOKEN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TOKEN))
                .andExpect(status().isOk())
                .andExpect(content().string(is(equalTo("false"))));
    }

    private String validUserSaveRequestAsJson() throws JsonProcessingException {
        return objectMapper.writeValueAsString(validUserRequest());
    }

    private ForgottenPassword validUserRequest() throws JsonProcessingException {
        return ForgottenPassword
                .builder()
                .email(USERNAME)
                .createEmailToken(true)
                .build();
    }

    private String successResponseWithSaveUserCode() throws JsonProcessingException {
        return objectMapper.writeValueAsString(ValidateEmailResponse
                .builder()
                .userName(USERNAME)
                .uuid(UUID)
                .token(TOKEN)
                .build());
    }

    private String successTokenResponse() throws JsonProcessingException {
        return objectMapper.writeValueAsString(ResetEmailTokenResponse
                .builder()
                .userUuid(UUID)
                .build());
    }

}
