package uk.nhs.nhsbsa.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nhsbsa.security.SetPasswordResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.nhsbsa.model.User;
import uk.nhs.nhsbsa.security.SetPasswordRequest;
import uk.nhs.nhsbsa.service.UserUpdateDetailsService;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyObject;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by nataliehulse on 09/10/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UpdatePasswordControllerMvcTest {

    public static final String UPDATE_PASSWORD_URL = "/authenticate/update-password";
    public static final String VALIDATE_PASSWORD_URL = "/authenticate/validate-password";
    public static final String PASSWORD = "password";
    public static final String USER_UUID = "user-uuid";
    public static final boolean FIRST_LOGIN = false;

    private MockMvc mvc;

    @Mock
    private UserUpdateDetailsService userUpdateDetailsService;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders
                .standaloneSetup(new UpdatePasswordController(userUpdateDetailsService))
                .build();
    }

    @Test
    public void userPasswordIsSaved() throws Exception {

        final User savedUser = User
                .builder()
                .uuid(USER_UUID)
                .build();

        given(userUpdateDetailsService.updatePassword(anyObject())).willReturn(savedUser);


        mvc.perform(MockMvcRequestBuilders.post(UPDATE_PASSWORD_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(validUserSaveRequestAsJson()))
                .andExpect(status().isOk())
                .andExpect(content().string(is(equalTo(successResponseWithSaveUserCode()))));
    }

    @Test
    public void userPasswordIsNotSaved() throws Exception {


        given(userUpdateDetailsService.updatePassword(anyObject())).willReturn(null);

        mvc.perform(MockMvcRequestBuilders.post(UPDATE_PASSWORD_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(validUserSaveRequestAsJson()))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void userPasswordHasBindingErrors() throws Exception {


        given(userUpdateDetailsService.updatePassword(anyObject())).willReturn(null);

        mvc.perform(MockMvcRequestBuilders.post(UPDATE_PASSWORD_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(invalidPasswordRequest()))
            .andExpect(status().isBadRequest())
            .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void testValidateNewPassword() throws Exception {


        given(userUpdateDetailsService.isNewPasswordSameAsPrevious(anyObject())).willReturn(false);

        mvc.perform(MockMvcRequestBuilders.post(VALIDATE_PASSWORD_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(validUserSaveRequestAsJson()))
            .andExpect(status().isOk())
            .andExpect(content().string(is(equalTo(validPasswordResponse()))));
    }

    @Test
    public void testValidateNewPasswordhasBindingError() throws Exception {


        given(userUpdateDetailsService.isNewPasswordSameAsPrevious(anyObject())).willReturn(false);

        mvc.perform(MockMvcRequestBuilders.post(VALIDATE_PASSWORD_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(invalidPasswordRequest()))
            .andExpect(status().isBadRequest())
            .andExpect(content().string(isEmptyString()));
    }

    private String validUserSaveRequestAsJson() throws JsonProcessingException {
        return objectMapper.writeValueAsString(validUserRequest());
    }

    private SetPasswordRequest validUserRequest() throws JsonProcessingException {
        return SetPasswordRequest
                .builder()
                .uuid(USER_UUID)
                .firstLogin(FIRST_LOGIN)
                .newPassword(PASSWORD)
                .build();
    }

    private String successResponseWithSaveUserCode() throws JsonProcessingException {
        return objectMapper.writeValueAsString(SetPasswordResponse
                .builder()
                .uuid(USER_UUID)
                .firstLogin(FIRST_LOGIN)
                .passwordSameAsPrevious(false)
                .build());
    }

    private String validPasswordResponse() throws JsonProcessingException {
        return objectMapper.writeValueAsString(SetPasswordResponse
            .builder()
            .passwordSameAsPrevious(false)
            .build());
    }

    private String invalidPasswordRequest() throws JsonProcessingException {
        return objectMapper.writeValueAsString(SetPasswordRequest
            .builder()
            .uuid(USER_UUID)
            .firstLogin(FIRST_LOGIN)
            .newPassword(null)
            .build());
    }


}
