package uk.nhs.nhsbsa.model;

import com.nhsbsa.model.AuthorizedApplication;
import com.nhsbsa.model.User;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * Created by Mark Lishman on 04/01/2017.
 */
public class UserTest {

    private User user;

    @Before
    public void before() {
        user = User.builder().build();
    }

    @Test
    public void applicationIsAddedToEmptyListForUser() {
        final AuthorizedApplication expectedApp = AuthorizedApplication.builder().build();
        user.addApplication(expectedApp);

        final List<AuthorizedApplication> actualApps = user.getApplications();

        assertThat(actualApps, hasSize(1));
        assertThat(actualApps.get(0), is(sameInstance(expectedApp)));
    }

    @Test
    public void applicationIsAddedToExistingListForUser() {
        user.setApplications(new ArrayList<>());
        final AuthorizedApplication existingApp = AuthorizedApplication.builder().build();
        user.getApplications().add(existingApp);

        final AuthorizedApplication expectedApp = AuthorizedApplication.builder().build();
        user.addApplication(expectedApp);

        final List<AuthorizedApplication> actualApps = user.getApplications();

        assertThat(actualApps, hasSize(2));
        assertThat(actualApps, is(hasItems(existingApp, expectedApp)));
    }

}