package uk.nhs.nhsbsa.service;

import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.AuthenticationResponse.AuthenticationStatus;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import org.mockito.Mockito;
import uk.nhs.nhsbsa.model.ResetEmailToken;
import uk.nhs.nhsbsa.repos.ResetEmailTokenRepository;
import com.nhsbsa.model.User;
import uk.nhs.nhsbsa.repos.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

/**
 * Created by Mark Lishman on 05/01/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserAuthenticationServiceTest {

    private static final String APP_NAME = "app-name";
    private static final String USER_NAME = "user-name";
    private static final String PASSWORD = "password";
    private static final String HASH = "hash";
    private static final User NO_USER = null;
    private static final Long USER_ID = 1L;
    private static final String UUID = "uuid";
    private static final String TOKEN = "1234-uuid";
    private static final LocalDateTime CREATED_DATE = LocalDateTime.of(2017, 01, 01, 11, 00);
    private static final ResetEmailToken NO_TOKEN = null;

    private static final User DEFAULT_USER = User
            .builder()
            .name(USER_NAME)
            .hash(HASH)
            .build();

    private UserAuthenticationService userAuthenticationService;

    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordService passwordService;
    @Mock
    private ResetEmailTokenRepository resetEmailTokenRepository;

    @Before
    public void before() {
        userAuthenticationService = new UserAuthenticationService(
                userRepository,
                passwordService,
                resetEmailTokenRepository, 3
        );
    }

    @Test
    public void userIsValid() {
        given(userRepository.findByNameAndApplicationsName(USER_NAME, APP_NAME)).willReturn(DEFAULT_USER);
        given(passwordService.isValidPassword(PASSWORD, HASH)).willReturn(true);

        final AuthenticationResponse actualUser = userAuthenticationService.authenticateUser(APP_NAME, USER_NAME, PASSWORD);

        assertThat(actualUser.getUuid(), is(DEFAULT_USER.getUuid()));
    }

    @Test
    public void userIsInvalidIfUserDoesNotExistOrIsNotAuthorisedToUseApp() {
        given(userRepository.findByNameAndApplicationsName(USER_NAME, APP_NAME)).willReturn(NO_USER);

        final AuthenticationResponse actualUser = userAuthenticationService.authenticateUser(APP_NAME, USER_NAME, PASSWORD);

        assertThat(actualUser.getAuthenticationStatus(), is(AuthenticationStatus.AUTH_FAILURE));
    }

    @Test
    public void userIsInvalidIfUserIsDeleted() {
        LocalDate date = LocalDate.of(2018,10,1);
        Date deletedOn = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());

        final User user = User
            .builder()
            .name(USER_NAME)
            .hash(HASH)
            .deletedOn(deletedOn)
            .build();

        given(userRepository.findByNameAndApplicationsName(USER_NAME, APP_NAME)).willReturn(user);
        given(passwordService.isValidPassword(PASSWORD, HASH)).willReturn(true);

        final AuthenticationResponse actualUser = userAuthenticationService.authenticateUser(APP_NAME, USER_NAME, PASSWORD);

        assertThat(actualUser.getAuthenticationStatus(), is(AuthenticationStatus.AUTH_FAILURE));
    }

    @Test
    public void userIsInvalidIfUserIsLocked() {

        final User user = User
            .builder()
            .name(USER_NAME)
            .hash(HASH)
            .locked(true)
            .build();

        given(userRepository.findByNameAndApplicationsName(USER_NAME, APP_NAME)).willReturn(user);
        given(passwordService.isValidPassword(PASSWORD, HASH)).willReturn(true);

        final AuthenticationResponse actualUser = userAuthenticationService.authenticateUser(APP_NAME, USER_NAME, PASSWORD);

        assertThat(actualUser.getAuthenticationStatus(), is(AuthenticationStatus.ACCOUNT_LOCKED));
    }


    @Test
    public void userIsInvalidIfPasswordsDoNotMatch() {

        given(userRepository.findByNameAndApplicationsName(USER_NAME, APP_NAME)).willReturn(DEFAULT_USER);
        given(passwordService.notAValidPassword(PASSWORD, HASH)).willReturn(true);

        final AuthenticationResponse actualUser = userAuthenticationService.authenticateUser(APP_NAME, USER_NAME, PASSWORD);

        assertThat(actualUser.getAuthenticationStatus(), is(AuthenticationStatus.AUTH_FAILURE));
    }

    @Test
    public void emailIsValid() {
        given(userRepository.findByNameAndApplicationsName(USER_NAME, APP_NAME)).willReturn(DEFAULT_USER);

        final User actualUser = userAuthenticationService.retrieveUserByValidEmail(APP_NAME, USER_NAME);

        assertThat(actualUser, is(sameInstance(DEFAULT_USER)));
    }

    @Test
    public void emailIsInvalid() {
        given(userRepository.findByNameAndApplicationsName(USER_NAME, APP_NAME)).willReturn(NO_USER);

        final User actualUser = userAuthenticationService.retrieveUserByValidEmail(APP_NAME, USER_NAME);

        assertThat(actualUser, is(nullValue()));
    }

    @Test
    public void EmailTokenIsSavedWithoutAnyPreviousTokens() throws Exception {

        Set<ResetEmailToken> previousTokens = null;
        given(resetEmailTokenRepository.findByUserId(USER_ID)).willReturn(previousTokens);

        verify(resetEmailTokenRepository, times(0)).delete(previousTokens);

        ResetEmailToken savedUser = ResetEmailToken.builder().id(1L).userId(USER_ID).token(TOKEN).createdDate(CREATED_DATE).build();
        given(resetEmailTokenRepository.save(Mockito.any(ResetEmailToken.class))).willReturn(savedUser);

        ResetEmailToken response = userAuthenticationService.generateEmailToken(USER_ID);

        assertThat(response, is(sameInstance(savedUser)));
        assertThat(response.getUserId(), is(USER_ID));
        assertThat(response.getToken(), is(TOKEN));
        assertThat(response.getCreatedDate(), is(CREATED_DATE));
    }

    private Set<ResetEmailToken> getTestResetEmailTokenSet() {
        ResetEmailToken token1 = ResetEmailToken.builder()
                .id(1L)
                .userId(USER_ID)
                .token("token1")
                .build();
        ResetEmailToken token2 = ResetEmailToken.builder()
                .id(2L)
                .userId(USER_ID)
                .token("token2")
                .build();
        Set<ResetEmailToken> set = new HashSet<>();
        set.add(token1);
        set.add(token2);
        return set;
    }

    @Test
    public void EmailTokenIsSavedWithPreviousTokensDeleted() throws Exception {

        Set<ResetEmailToken> previousTokens = getTestResetEmailTokenSet();
        given(resetEmailTokenRepository.findByUserId(USER_ID)).willReturn(previousTokens);

        ResetEmailToken savedUser = ResetEmailToken.builder().id(1L).userId(USER_ID).token(TOKEN).createdDate(CREATED_DATE).build();
        given(resetEmailTokenRepository.save(Mockito.any(ResetEmailToken.class))).willReturn(savedUser);

        ResetEmailToken response = userAuthenticationService.generateEmailToken(USER_ID);

        verify(resetEmailTokenRepository, times(1)).delete(previousTokens);

        assertThat(response, is(sameInstance(savedUser)));
        assertThat(response.getUserId(), is(USER_ID));
        assertThat(response.getToken(), is(TOKEN));
        assertThat(response.getCreatedDate(), is(CREATED_DATE));
    }

    @Test
    public void EmailTokenIsNotSaved() {

        Set<ResetEmailToken> previousTokens = null;
        given(resetEmailTokenRepository.findByUserId(1L)).willReturn(previousTokens);

        final ResetEmailToken resetEmailToken = ResetEmailToken.builder().token(TOKEN).userId(USER_ID).createdDate(CREATED_DATE).build();
        given(resetEmailTokenRepository.save(resetEmailToken)).willReturn(NO_TOKEN);

        final ResetEmailToken response = userAuthenticationService.generateEmailToken(USER_ID);

        assertThat(response, is(nullValue()));
    }

    @Test
    public void EmailTokenIsValidated() throws Exception {

        ResetEmailToken savedToken = ResetEmailToken.builder().id(1L).userId(USER_ID).token(TOKEN).createdDate(CREATED_DATE).build();

        given(resetEmailTokenRepository.findByToken(TOKEN)).willReturn(savedToken);

        final User savedUser = User.builder().uuid(UUID).build();

        given(userRepository.findById(1L)).willReturn(savedUser);

        String response = userAuthenticationService.validateEmailToken(TOKEN);

        assertThat(response, is(equalTo(savedUser.getUuid())));
    }

    @Test
    public void EmailTokenIsNotVerified() {

        given(resetEmailTokenRepository.findByToken(TOKEN)).willReturn(null);

        final ResetEmailToken response = userAuthenticationService.generateEmailToken(USER_ID);

        assertThat(response, is(nullValue()));
    }

    @Test
    public void EmailTokenIsDeleted() throws Exception {

        ResetEmailToken savedToken = ResetEmailToken.builder().id(1L).userId(USER_ID).token(TOKEN).createdDate(CREATED_DATE).build();

        given(resetEmailTokenRepository.findByToken(TOKEN)).willReturn(savedToken);

        doNothing().when(resetEmailTokenRepository).delete(Mockito.any(ResetEmailToken.class));

        boolean response = userAuthenticationService.deleteEmailToken(TOKEN);

        assertThat(response, is(equalTo(true)));
    }

    @Test
    public void EmailTokenIsNotDeleted() throws Exception {

        given(resetEmailTokenRepository.findByToken(TOKEN)).willReturn(null);

        doNothing().when(resetEmailTokenRepository).delete(Mockito.any(ResetEmailToken.class));

        boolean response = userAuthenticationService.deleteEmailToken(TOKEN);

        assertThat(response, is(equalTo(false)));
    }

    @Test
    public void expiredResetEmailTokenDeleted1Row() {

        final int numberRowsDeleted = 1;

        doReturn(numberRowsDeleted).when(resetEmailTokenRepository).deleteByCreatedDateBefore(Mockito.any(LocalDateTime.class));

        int response = userAuthenticationService.deleteExpiredResetEmailTokens();

        assertThat(response, is(equalTo(numberRowsDeleted)));
    }

    @Test
    public void expiredResetEmailTokenDeleted0Row() {

        final int numberRowsDeleted = 0;

        doReturn(numberRowsDeleted).when(resetEmailTokenRepository).deleteByCreatedDateBefore(Mockito.any(LocalDateTime.class));

        int response = userAuthenticationService.deleteExpiredResetEmailTokens();

        assertThat(response, is(equalTo(numberRowsDeleted)));
    }

}