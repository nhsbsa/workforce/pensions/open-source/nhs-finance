package uk.nhs.nhsbsa.service;

import com.nhsbsa.model.AuthorizedApplication;
import uk.nhs.nhsbsa.repos.ApplicationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

/**
 * Created by Mark Lishman on 05/01/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorizedApplicationServiceTest {

    public static final String APP_NAME = "app-name";
    public static final String HASH = "hash";

    private AuthorizedApplicationService authorizedApplicationService;

    @Mock private ApplicationRepository applicationRepository;

    @Before
    public void before() {
        authorizedApplicationService = new AuthorizedApplicationService(applicationRepository);
    }

    @Test
    public void applicationHashIsReturned() {
        final AuthorizedApplication authorizedApplication = AuthorizedApplication
                .builder()
                .hash(HASH)
                .build();
        given(applicationRepository.findByName(APP_NAME)).willReturn(authorizedApplication);

        final String actualHash = authorizedApplicationService.getApplicationHash(APP_NAME);

        assertThat(actualHash, is(equalTo(HASH)));
    }

    @Test
    public void nullIsReturnedIfApplicationDoesNotExist() {
        given(applicationRepository.findByName(APP_NAME)).willReturn(null);

        final String actualHash = authorizedApplicationService.getApplicationHash(APP_NAME);

        assertThat(actualHash, is(nullValue()));
    }
}