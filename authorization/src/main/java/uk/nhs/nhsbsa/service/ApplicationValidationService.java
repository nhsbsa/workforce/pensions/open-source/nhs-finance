package uk.nhs.nhsbsa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.nhs.nhsbsa.rest.RestAuthentication;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by jeffreya on 23/09/2016.
 * ApplicationValidationService
 */

@Service
public class ApplicationValidationService {

    private final ApplicationService applicationService;

    @Autowired
    public ApplicationValidationService(final ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    public boolean validateApplication(final HttpServletRequest httpServletRequest) {
        final String applicationId = httpServletRequest.getHeader(RestAuthentication.AUTH_APP_ID);
        if (applicationId == null) {
            return false;
        }
        final String applicationToken = httpServletRequest.getHeader(RestAuthentication.AUTH_APP_TOKEN);

        if (applicationToken == null) {
            return false;
        }

        return applicationService.isValidApplication(applicationId, applicationToken);
    }
}
