package uk.nhs.nhsbsa.service;

import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.nhsbsa.model.User;
import uk.nhs.nhsbsa.model.UserPasswordHistory;
import uk.nhs.nhsbsa.repos.UserPasswordHistoryRepository;
import uk.nhs.nhsbsa.repos.UserRepository;
import uk.nhs.nhsbsa.security.SetPasswordRequest;


/**
 * Created by nataliehulse on 05/10/2017.
 */

@Log4j
@Service
public class UserUpdateDetailsService {

    private final PasswordService passwordService;
    private final UserRepository userRepository;
    private final UserPasswordHistoryRepository userPasswordHistoryRepository;

    @Autowired
    public UserUpdateDetailsService(
            final PasswordService passwordService,
            final UserRepository userRepository,
            final UserPasswordHistoryRepository userPasswordHistoryRepository) {
        this.passwordService = passwordService;
        this.userRepository = userRepository;
        this.userPasswordHistoryRepository = userPasswordHistoryRepository;
    }

    public User updatePassword(final SetPasswordRequest setPasswordRequest) {

        final String hash = passwordService.generatePasswordHash(setPasswordRequest.getNewPassword());

        final User user = userRepository.findByUuid(setPasswordRequest.getUuid());

        if (user == null) {
            return null;
        }

        // Copy Password over to UserPasswordHistory table before overwriting with new password
        final UserPasswordHistory userPasswordHistory = saveUserPasswordHistory(user);

        if(userPasswordHistory == null) {
            return null;
        }

        // Set account to be unlocked regardless of whether it's currently locked
        user.setLocked(false);
        user.setFirstLogin(setPasswordRequest.isFirstLogin());
        user.setHash(hash);


        return userRepository.save(user);
    }

    public UserPasswordHistory saveUserPasswordHistory(final User user) {

        final UserPasswordHistory userPasswordHistory = new UserPasswordHistory();
        userPasswordHistory.setUserId(user.getId());
        userPasswordHistory.setName(user.getName());
        userPasswordHistory.setHash(user.getHash());

        return userPasswordHistoryRepository.save(userPasswordHistory);
    }

    public boolean isNewPasswordSameAsPrevious(final SetPasswordRequest setPasswordRequest) {

        final User user = userRepository.findByUuid(setPasswordRequest.getUuid());

        if (user == null) {
            return false;
        }

        return passwordService.isValidPassword(setPasswordRequest.getNewPassword(), user.getHash());
    }
}