package uk.nhs.nhsbsa.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.nhsbsa.model.AuthorizedApplication;
import uk.nhs.nhsbsa.repos.ApplicationRepository;

/**
 * Created by jeffreya on 23/09/2016. AuthorizedApplicationService
 */

@Service
@Transactional
@Slf4j
public class AuthorizedApplicationService {

  private final ApplicationRepository applicationRepository;

  @Autowired
  public AuthorizedApplicationService(final ApplicationRepository applicationRepository) {
    this.applicationRepository = applicationRepository;
  }

  public String getApplicationHash(final String applicationName) {
    final AuthorizedApplication app = applicationRepository.findByName(applicationName);
    return app == null ? null : app.getHash();
  }


  public static void mainGen(String[] args) {

    if (args.length < 1) {
      log.debug("requires arg{0} service-name");
      return;
    }

    final ApplicationTokenGeneratorService applicationTokenGeneratorService = new ApplicationTokenGeneratorService();
    final PasswordService passwordService = new PasswordService();

    final String applicationName = args[0];
    final String token = applicationTokenGeneratorService.nextSessionId();
    final String combined = applicationName + token;
    final String passwordHash = passwordService.generatePasswordHash(combined);

    log.debug("app  : " + applicationName);
    log.debug("token: " + token);
    log.debug("hash : " + passwordHash);
  }
}
