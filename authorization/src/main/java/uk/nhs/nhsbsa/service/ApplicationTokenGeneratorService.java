package uk.nhs.nhsbsa.service;

import java.math.BigInteger;
import java.security.SecureRandom;
import org.springframework.stereotype.Service;

/**
 * Created by jeffreya on 23/09/2016. ApplicationValidationService
 */

@Service
public class ApplicationTokenGeneratorService {

  private SecureRandom random = new SecureRandom();

  public String nextSessionId() {
    return new BigInteger(130, random).toString(32);
  }

}
