package uk.nhs.nhsbsa.service;

import com.nhsbsa.model.User;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.AuthenticationResponse.AuthenticationStatus;
import java.time.LocalDateTime;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import uk.nhs.nhsbsa.model.ResetEmailToken;
import uk.nhs.nhsbsa.repos.ResetEmailTokenRepository;
import uk.nhs.nhsbsa.repos.UserRepository;

/**
 * Created by jeffreya on 26/09/2016.
 * UserAuthenticationService
 */

@Slf4j
@Service
public class UserAuthenticationService {

    private final UserRepository userRepository;
    private final PasswordService passwordService;
    private final ResetEmailTokenRepository resetEmailTokenRepository;

    private Integer maxRetries;

    @Autowired
    public UserAuthenticationService(
        final UserRepository userRepository,
        final PasswordService passwordService,
        final ResetEmailTokenRepository resetEmailTokenRepository,
        @Value("${failedLogin.maxRetries}")
        final Integer maxRetries
    ) {
        this.userRepository = userRepository;
        this.passwordService = passwordService;
        this.resetEmailTokenRepository = resetEmailTokenRepository;
        this.maxRetries = maxRetries;
    }

    public AuthenticationResponse authenticateUser(final String appName, final String userName, final String password) {

        final User user = userRepository.findByNameAndApplicationsName(userName, appName);

        if ( user == null || user.userIsDeleted()) {

            return authFailure();
        }

        if ( user.isLocked() ) {

            return authLocked();

        } else if( passwordService.notAValidPassword(password, user.getHash()) ){

            user.increaseRetries();
            if (user.getRetries() >= maxRetries) {
                user.setRetries(0);
                user.setLocked(true);
            }
            userRepository.save(user);

            return authFailure();

        }  else {

            if (user.getRetries() != 0) {

                user.setRetries(0);
                userRepository.save(user);
            }

            return AuthenticationResponse
                .builder()
                .uuid(user.getUuid())
                .firstLogin(user.isFirstLogin())
                .role(user.getRole())
                .token("OAUTH2-TOKEN")
                .authenticationStatus(AuthenticationStatus.AUTH_SUCCESS)
                .build();
        }

    }

    private AuthenticationResponse authFailure(){
        return AuthenticationResponse
            .builder()
            .authenticationStatus(AuthenticationStatus.AUTH_FAILURE)
            .build();
    }

    private AuthenticationResponse authLocked(){
        return AuthenticationResponse
            .builder()
            .authenticationStatus(AuthenticationStatus.ACCOUNT_LOCKED)
            .build();
    }

    public User retrieveUserByValidEmail(final String appName, final String email) {
        return userRepository.findByNameAndApplicationsName(email, appName);
    }

    public ResetEmailToken generateEmailToken(final Long userId) {

        // Delete any outstanding tokens for previous requests before creating a new one
        Set<ResetEmailToken> previousTokens = resetEmailTokenRepository.findByUserId(userId);
        resetEmailTokenRepository.delete(previousTokens);

        final ResetEmailToken resetEmailToken = new ResetEmailToken();
        final String token = java.util.UUID.randomUUID().toString();
        resetEmailToken.setUserId(userId);
        resetEmailToken.setToken(token);
        resetEmailToken.setCreatedDate(LocalDateTime.now());

        return resetEmailTokenRepository.save(resetEmailToken);
    }

    public String validateEmailToken(final String emailTokenRequest) {
        final ResetEmailToken resetEmailToken = resetEmailTokenRepository.findByToken(emailTokenRequest);

        if (resetEmailToken == null) {
            return null;
        }

        final User user = userRepository.findById(resetEmailToken.getUserId());


        if (user == null) {
            return null;
        }

        return user.getUuid();
    }

    public boolean deleteEmailToken(final String emailToken) {

        final ResetEmailToken resetEmailToken = resetEmailTokenRepository.findByToken(emailToken);

        if (resetEmailToken != null) {
            resetEmailTokenRepository.delete(resetEmailToken);
            return true;
        }
        return false;

    }

    /**
     * Scheduled job
     * Deletes any rows from reset_email_token table where createdDate > 24 hours old.
     */
    @Scheduled(cron = "${email.token.deletion.cron.schedule}")
    public int deleteExpiredResetEmailTokens() {

        LocalDateTime deletionDate = LocalDateTime.now().minusHours(24);

        int noRowsDeleted = resetEmailTokenRepository.deleteByCreatedDateBefore(deletionDate);

        log.info("Number Email Tokens Deleted: " + noRowsDeleted);

        return noRowsDeleted;

    }
}
