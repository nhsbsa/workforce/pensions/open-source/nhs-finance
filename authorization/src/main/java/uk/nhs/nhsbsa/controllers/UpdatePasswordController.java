package uk.nhs.nhsbsa.controllers;

import com.nhsbsa.security.SetPasswordResponse;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.nhsbsa.model.User;
import uk.nhs.nhsbsa.security.SetPasswordRequest;
import uk.nhs.nhsbsa.service.UserUpdateDetailsService;
import javax.validation.Valid;

/**
 * Created by nataliehulse on 05/10/2017.
 */

@Slf4j
@RequestMapping
@RestController
public class UpdatePasswordController {

    private final UserUpdateDetailsService userUpdateDetailsService;

    @Autowired
    public UpdatePasswordController(final UserUpdateDetailsService userUpdateDetailsService) {
        this.userUpdateDetailsService = userUpdateDetailsService;
    }

    @PostMapping("/authenticate/update-password")
    @ApiOperation(value = "default", notes = "default")
    public ResponseEntity<SetPasswordResponse> updatePassword(@RequestBody @Valid final SetPasswordRequest setPasswordRequest,
                                                              final BindingResult bindingResult) {
        try {
            log.info("ENTER - {}", "/update-password");

            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            final User user = userUpdateDetailsService.updatePassword(setPasswordRequest);

            if (user == null) {
                log.error("ERROR - Password not updated in db");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            SetPasswordResponse setPasswordResponse =
                    SetPasswordResponse
                            .builder()
                            .uuid(user.getUuid())
                            .firstLogin(user.isFirstLogin())
                            .build();


            return ResponseEntity.ok(setPasswordResponse);
        } finally {
            log.info("EXIT - {}", "/update-password");
        }
    }

    @PostMapping("/authenticate/validate-password")
    @ApiOperation(value = "default", notes = "default")
    public ResponseEntity<SetPasswordResponse> validateNewPassword(@RequestBody @Valid final SetPasswordRequest setPasswordRequest,
                                                                   final BindingResult bindingResult) {
        try {
            log.info("ENTER - {}", "/validate-password");

            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            final boolean isNewPasswordSameAsPrevious = userUpdateDetailsService.isNewPasswordSameAsPrevious(setPasswordRequest);


            SetPasswordResponse setPasswordResponse =
                    SetPasswordResponse
                            .builder()
                            .passwordSameAsPrevious(isNewPasswordSameAsPrevious)
                            .build();


            return ResponseEntity.ok(setPasswordResponse);
        } finally {
            log.info("EXIT - {}", "/validate-password");
        }
    }


}