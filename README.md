# NHS Financial Information Capture

FIC is a web application that lets employers submit their pension contributions without the need to connect trough POL.

It consists of several services:
1. Authorization
2. Employer contributions service 
3. Employer details service
4. Employer service
5. Employer website

All of which are REST services except for Employer website. 
Once you have all up and running you will be able to access it through `http://localhost:8080/employer-website/login` 

## Requirements

Install [Java8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) or later

Install [Maven 3](https://maven.apache.org/download.cgi)

Setup a [PostgreSQL](https://www.postgresql.org/download/) database version 9.X (If using docker, ignore this and look on the installation instructions how to create a database docker image)

Setup your Maven settings.xml as per the standard BSA settings for development environments

If you don't have access to the BSA settings.xml or to the BSA network you will need to request access to download the library `uk.nhs.nhsbsa.bsa-notify:bsa-notify-client` otherwise your build will fail. You will be provided a JAR file and you will need to install it following [these instructions](https://maven.apache.org/guides/mini/guide-3rd-party-jars-local.html)

## Installation and Running

Our local environments have all been setup using a PostgreSQL instance using docker. You can find it on the subfolder `nhs-finance/database`

If you use docker you can create a database image using the `database` Maven project:

    mvn clean install docker:build

Otherwise you can use the SQL on `init-user-db.sh` to create the databases yourself on any PostgreSQL instance.

Create a new entry in your hosts file pointing to the database with the name `database`

    127.0.0.1 database

Now you can use liquibase to create all the DDL and a small bit of DML to get your local environment up and running:

      mvn -f "./employer-contribs-service/liquibase/pom.xml" -Dliquibase.url="jdbc:postgresql://database:5432/finance" liquibase:update
      mvn -f "./authorization/authorization-liquibase/pom.xml" -Dliquibase.url="jdbc:postgresql://database:5432/authentication" liquibase:update
      mvn -f "./employer-details-service/liquibase/pom.xml" -Dliquibase.url="jdbc:postgresql://database:5432/employer" liquibase:update@employer-database

All services use Spring Boot, so you can run them independently by using:

    mvn spring-boot:run
    
Or you can run a packaged jar like so:

    mvn install
    java -jar target/myapplication-0.0.1-SNAPSHOT.jar
    
You can also import the project in your favourite IDE thanks to Maven (We use IntelliJ and Eclipse) and run the services from the IDE using SpringBoot plugins

For our convenience building and running the project we created `up.sh` for unix/linux users, run `./up.sh -h` for instructions on how to use it.

Once all 5 services are up and running, check for the logs to verify they could connect to their own databases, then you can access employer-website through `http://localhost:8080/employer-website/login`

## Testing

When the project is packaged using maven it will be automatically tested, however you can run the tests independently by executing:
 
    mvn test

## Contributing

Please see [CONTRIBUTING.md](/CONTRIBUTING.md).




